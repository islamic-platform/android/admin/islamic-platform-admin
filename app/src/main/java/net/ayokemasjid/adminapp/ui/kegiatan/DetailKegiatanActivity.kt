package net.ayokemasjid.adminapp.ui.kegiatan

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import com.squareup.picasso.Picasso
import jp.wasabeef.richeditor.RichEditor
import kotlinx.android.synthetic.main.activity_detail_kegiatan.*
import kotlinx.android.synthetic.main.activity_list_kegiatan.swipeRefreshLayout
import net.ayokemasjid.adminapp.BuildConfig
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Kegiatan
import net.ayokemasjid.adminapp.data.preferences.PreferenceProvider
import net.ayokemasjid.adminapp.databinding.ActivityDetailKegiatanBinding
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class DetailKegiatanActivity : AppCompatActivity(), KodeinAware {
    private lateinit var binding: ActivityDetailKegiatanBinding

    private var mEditor: RichEditor? = null
    private var data: Kegiatan? = Kegiatan()

    override val kodein by kodein()
    private val preference by instance<PreferenceProvider>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_kegiatan)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail_kegiatan)
        setSupportActionBar(binding.toolbar)
        setData()

        swipeRefreshLayout.setOnRefreshListener {
            setData()
        }
    }

    private fun setData(){
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = true
        }
        data = intent.getSerializableExtra("data")  as? Kegiatan ?: Kegiatan()
        binding.kegiatan = data
        mEditor = binding.editor
        mEditor!!.html = data!!.information
        mEditor!!.setEditorFontSize(14)
        mEditor!!.setEditorFontColor(R.color.dark)
        mEditor!!.setInputEnabled(false)

        val url = preference.getImageURL() ?: "http://staging.ayokemasjid.net/assets/uploads/"
        Picasso.get()
            .load(url+data!!.main_image)
            .placeholder(R.drawable.progress_animation)
            .error(R.color.white)
            .into(ivMainImage)

        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}