package net.ayokemasjid.adminapp.data.model

import com.google.gson.annotations.SerializedName

data class JamaahData(
    @SerializedName("email")
    var email: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("telephone")
    var telephone: String,
    @SerializedName("gender")
    var gender: String,
    @SerializedName("address")
    var address: String,
    @SerializedName("city")
    var city: String,
    @SerializedName("province")
    var province: String,
    @SerializedName("country")
    var country: String,
    @SerializedName("postcode")
    var postcode: String,
    @SerializedName("information")
    var information: String,
    @SerializedName("lat")
    var lat: String,
    @SerializedName("lon")
    var lon: String
)