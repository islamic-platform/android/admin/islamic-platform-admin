package net.ayokemasjid.adminapp.ui.home

interface HomeListener {
    fun onStarted()
    fun getMessage(message: String)
}