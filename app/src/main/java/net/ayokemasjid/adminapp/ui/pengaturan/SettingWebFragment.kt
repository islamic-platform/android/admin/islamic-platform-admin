package net.ayokemasjid.adminapp.ui.pengaturan

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.core.content.ContextCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.geocoding.v5.MapboxGeocoding
import com.mapbox.api.geocoding.v5.models.CarmenFeature
import com.mapbox.api.geocoding.v5.models.GeocodingResponse
import com.mapbox.core.exceptions.ServicesException
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.*
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions
import com.mapbox.mapboxsdk.style.layers.Layer
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import kotlinx.android.synthetic.main.activity_pengaturan.progress_bar
import kotlinx.android.synthetic.main.activity_pengaturan.root_layout
import kotlinx.android.synthetic.main.fragment_setting_web.swipeRefreshLayout
import kotlinx.android.synthetic.main.fragment_setting_web.*
import kotlinx.android.synthetic.main.fragment_setting_web.back_to_camera_tracking_mode
import kotlinx.android.synthetic.main.fragment_setting_web.fab_location_search
import kotlinx.android.synthetic.main.fragment_setting_web.map_view
import kotlinx.android.synthetic.main.fragment_setting_web.select_location_button
import kotlinx.android.synthetic.main.fragment_setting_web.tvButtonShowMaps
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.preferences.PreferenceProvider
import net.ayokemasjid.adminapp.databinding.FragmentSettingWebBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

class SettingWebFragment : DialogFragment(), KodeinAware, MainListener, OnMapReadyCallback,
    PermissionsListener, OnLocationClickListener, OnCameraTrackingChangedListener {

    override val kodein by kodein()
    private lateinit var viewModel: PengaturanViewModel
    private val factory by instance<PengaturanViewModelFactory>()

    private var _binding: FragmentSettingWebBinding? = null
    private val binding get() = _binding!!

    //maps
    private var isMapHidden = true
    private lateinit var permissionsManager: PermissionsManager
    private lateinit var mapboxMap: MapboxMap
    private var locationComponent: LocationComponent? = null
    private var isInTrackingMode = false
    private val REQUEST_CODE_AUTOCOMPLETE = 1
    private val geojsonSourceLayerId = "geojsonSourceLayerId"
    private val symbolIconId = "symbolIconId"
    private val DROPPED_MARKER_LAYER_ID = "DROPPED_MARKER_LAYER_ID"
    private var hoveringMarker: ImageView? = null
    private var droppedMarkerLayer: Layer? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        //maps
        Mapbox.getInstance(requireContext(), getString(R.string.map_box_access_token))
        _binding = FragmentSettingWebBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this, factory).get(PengaturanViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel.pengaturanListener = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        setData()
        setButtonListener()
        //maps
        initMapView(savedInstanceState)
        initPermissions()
        syncMapbox()
    }

    private fun setData() = Coroutines.main{
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = true
        }
        viewModel.masjidItems.await().observe(this, {
            viewModel.dataMasjid = it
            etEmailMasjid.setText(it.email)
            etTeleponMasjid.setText(it.phone)
            etAlamatMasjid.setText(it.address)
            etKotaMasjid.setText(it.city)
            etProvinsiMasjid.setText(it.province)
            etNegaraMasjid.setText(it.country)
            etPostcodeMasjid.setText(it.postcode)
            etLatMasjid.text = it.lat
            etLonMasjid.text = it.lon
            select_location_button.text = getString(R.string.location_picker_select_location_button_cancel)
        })
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun setButtonListener(){
        swipeRefreshLayout.setOnRefreshListener {
            refresh()
        }
        buttonShowMaps.setOnClickListener {
            showMaps()
        }
        buttonPengaturanMasjid.setOnClickListener {
            onStarted()
            if(isNoFormError()){
                viewModel.onPengaturanButtonClick()
                refresh()
            } else{
                (activity as PengaturanActivity?)?.toast("Ada form yang kosong!")
            }
            (activity as PengaturanActivity?)?.progress_bar?.hide()
        }
    }

    private fun isNoFormError(): Boolean{
        var status = true
        if(viewModel.dataMasjid?.email.isNullOrEmpty()){
            etEmailMasjid.error = "isi email masjid dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.phone.isNullOrEmpty()){
            etTeleponMasjid.error = "isi telepon masjid dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.address.isNullOrEmpty()){
            etAlamatMasjid.error = "isi alamat masjid dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.city.isNullOrEmpty()){
            etKotaMasjid.error = "isi nama kota dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.province.isNullOrEmpty()){
            etProvinsiMasjid.error = "isi nama provinsi dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.country.isNullOrEmpty()){
            etNegaraMasjid.error = "isi nama negara dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.postcode.isNullOrEmpty()){
            etPostcodeMasjid.error = "isi kode pos dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.lat.isNullOrEmpty()||viewModel.dataMasjid?.lon.isNullOrEmpty()){
            tvButtonShowMaps.error = "Pilih lokasi di maps dengan benar!"
            if(status){
                status = false
            }
        }
        return status
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStarted() {
        hideSoftKeyboard(requireActivity())
        (activity as PengaturanActivity?)?.progress_bar?.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
        (activity as PengaturanActivity?)?.progress_bar?.hide()
        (activity as PengaturanActivity?)?.toast(message)
    }

    fun refresh(){
        parentFragmentManager
            .beginTransaction()
            .detach(this)
            .attach(this)
            .commit()
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if(activity.currentFocus != null){
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }

    override fun onActivityCreated(@Nullable savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideSoftKeyboard(requireActivity())
    }

    //maps
    private fun showMaps(){
        if(isMapHidden){
            layout_maps.visibility = View.VISIBLE
            tvButtonShowMaps.text = "Tutup Maps"
            isMapHidden = false
        } else{
            layout_maps.visibility = View.GONE
            tvButtonShowMaps.text = "Tampilkan Maps"
            isMapHidden = true
        }
    }

    //kebawah ini maps semua
    private fun initMapView(savedInstanceState: Bundle?) {
        map_view.onCreate(savedInstanceState)
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        mapboxMap.setStyle(Style.MAPBOX_STREETS){ style ->
            initSearchFab()
            enableLocationComponent(style)
            style.addImage(symbolIconId, BitmapFactory.decodeResource(
                resources, R.drawable.red_marker
            ),false)

            // Create an empty GeoJSON source using the empty feature collection
            setUpSource(style)

            // Set up a new symbol layer for displaying the searched location's feature coordinates
            setupLayer(style)

            hoveringMarker = ImageView(requireContext())
            hoveringMarker!!.setImageResource(R.drawable.red_marker)
            val params: FrameLayout.LayoutParams = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER
            )
            hoveringMarker!!.layoutParams = params
            map_view.addView(hoveringMarker)
            initDroppedMarker(style)

            select_location_button.setOnClickListener {
                if (hoveringMarker!!.visibility == View.VISIBLE && select_location_button.text == "Pilih Lokasi") {

                    // Use the map target's coordinates to make a reverse geocoding search
                    val mapTargetLatLng = mapboxMap.cameraPosition.target

                    // Hide the hovering red hovering ImageView marker
                    hoveringMarker!!.visibility = View.INVISIBLE

                    // Transform the appearance of the button to become the cancel button
                    select_location_button.setBackgroundColor(
                        ContextCompat.getColor(requireContext(), R.color.colorAccent)
                    )
                    select_location_button.text = getString(R.string.location_picker_select_location_button_cancel)

                    // Show the SymbolLayer icon to represent the selected map location
                    if (style.getLayer(DROPPED_MARKER_LAYER_ID) != null) {
                        val source: GeoJsonSource? =
                            style.getSourceAs("dropped-marker-source-id")
                        source?.setGeoJson(
                            Point.fromLngLat(
                                mapTargetLatLng.longitude,
                                mapTargetLatLng.latitude
                            )
                        )
                        droppedMarkerLayer = style.getLayer(DROPPED_MARKER_LAYER_ID)
                        droppedMarkerLayer?.setProperties(PropertyFactory.visibility(Property.VISIBLE))
                    }

                    // Use the map camera target's coordinates to make a reverse geocoding search
                    reverseGeocode(
                        Point.fromLngLat(
                            mapTargetLatLng.longitude,
                            mapTargetLatLng.latitude
                        )
                    )
                } else {
                    viewModel.dataMasjid?.lat = null
                    etLatMasjid.text = ""
                    viewModel.dataMasjid?.lon = null
                    etLonMasjid.text = ""
                    tvButtonShowMaps.error = "Pilih lokasi di maps!"
                    // Switch the button appearance back to select a location.
                    select_location_button.setBackgroundColor(
                        ContextCompat.getColor(
                            requireContext(),
                            R.color.colorPrimary
                        )
                    )
                    select_location_button.text = getString(R.string.location_picker_select_location_button_select)

                    // Show the red hovering ImageView marker
                    hoveringMarker!!.visibility = View.VISIBLE

                    // Hide the selected location SymbolLayer
                    droppedMarkerLayer = style.getLayer(DROPPED_MARKER_LAYER_ID)
                    droppedMarkerLayer?.setProperties(PropertyFactory.visibility(Property.NONE))
                }
            }
        }
        this.mapboxMap = mapboxMap
    }

    private fun initDroppedMarker(loadedMapStyle: Style) {
        // Add the marker image to map
        loadedMapStyle.addImage(
            "dropped-icon-image", BitmapFactory.decodeResource(
                resources, R.drawable.blue_marker
            )
        )
        loadedMapStyle.addSource(GeoJsonSource("dropped-marker-source-id"))
        loadedMapStyle.addLayer(
            SymbolLayer(
                DROPPED_MARKER_LAYER_ID,
                "dropped-marker-source-id"
            ).withProperties(
                PropertyFactory.iconImage("dropped-icon-image"),
                PropertyFactory.visibility(Property.NONE),
                PropertyFactory.iconAllowOverlap(true),
                PropertyFactory.iconIgnorePlacement(true)
            )
        )
    }

    private fun initSearchFab() {
        fab_location_search.setOnClickListener {
            val intent: Intent = PlaceAutocomplete.IntentBuilder()
                .accessToken(
                    if (Mapbox.getAccessToken() != null) Mapbox.getAccessToken().toString() else getString(
                        R.string.map_box_access_token
                    )
                )
                .placeOptions(
                    PlaceOptions.builder()
                        .backgroundColor(Color.parseColor("#EEEEEE"))
                        .limit(10)
                        .build(PlaceOptions.MODE_CARDS)
                )
                .build(requireActivity())
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE)
        }
    }

    private fun setUpSource(@NonNull loadedMapStyle: Style) {
        loadedMapStyle.addSource(GeoJsonSource(geojsonSourceLayerId))
    }

    private fun setupLayer(loadedMapStyle: Style) {
        loadedMapStyle.addLayer(
            SymbolLayer("SYMBOL_LAYER_ID", geojsonSourceLayerId).withProperties(
                PropertyFactory.iconOffset(arrayOf(0f, -8f))
            )
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            // Retrieve selected location's CarmenFeature
            val selectedCarmenFeature = PlaceAutocomplete.getPlace(data)

            // Create a new FeatureCollection and add a new Feature to it using selectedCarmenFeature above.
            // Then retrieve and update the source designated for showing a selected location's symbol layer icon
            if (mapboxMap != null) {
                val style = mapboxMap.style
                if (style != null) {
                    val source =
                        style.getSourceAs<GeoJsonSource>(geojsonSourceLayerId)
                    source?.setGeoJson(
                        FeatureCollection.fromFeatures(
                            arrayOf(
                                Feature.fromJson(
                                    selectedCarmenFeature.toJson()
                                )
                            )
                        )
                    )

                    // Move map camera to the selected location
                    mapboxMap.animateCamera(
                        CameraUpdateFactory.newCameraPosition(
                            CameraPosition.Builder()
                                .target(
                                    LatLng(
                                        (selectedCarmenFeature.geometry() as Point?)!!.latitude(),
                                        (selectedCarmenFeature.geometry() as Point?)!!.longitude()
                                    )
                                )
                                .zoom(14.0)
                                .build()
                        ), 4000
                    )
                }
            }
        }
    }

    private fun initPermissions() {
        val permissionListener = object : PermissionsListener {
            override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
                /* Nothing to do in here */
            }

            override fun onPermissionResult(granted: Boolean) {
                if (granted) {
                    syncMapbox()
                } else {
                    val alertDialogInfo = AlertDialog.Builder(requireContext())
                        .setTitle(getString(R.string.info))
                        .setCancelable(false)
                        .setMessage(getString(R.string.permissions_denied))
                        .setPositiveButton(getString(R.string.dismiss)) { _, _ ->
                            requireActivity().finish()
                        }
                        .create()
                    alertDialogInfo.show()
                }
            }
        }
        if (PermissionsManager.areLocationPermissionsGranted(requireContext())) {
            syncMapbox()
        } else {
            permissionsManager = PermissionsManager(permissionListener)
            permissionsManager.requestLocationPermissions(requireActivity())
        }
    }

    private fun syncMapbox() {
        map_view.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(requireContext())) {

            // Create and customize the LocationComponent's options
            val customLocationComponentOptions = LocationComponentOptions.builder(requireContext())
                .trackingGesturesManagement(true)
                .elevation(5F)
                .accuracyAlpha(.6f)
                .accuracyColor(ContextCompat.getColor(requireContext(), R.color.mapboxGreen))
                .foregroundDrawable(R.drawable.ic_location_on)
                .build()

            // Get an instance of the component
            locationComponent = mapboxMap.locationComponent

            val locationComponentActivationOptions =
                LocationComponentActivationOptions.builder(requireContext(), loadedMapStyle)
                    .locationComponentOptions(customLocationComponentOptions)
                    .build()

            // Activate with options
            locationComponent!!.activateLocationComponent(locationComponentActivationOptions)

            // Enable to make component visible
            locationComponent!!.isLocationComponentEnabled = true

            // Set the component's camera mode
            locationComponent!!.cameraMode = CameraMode.TRACKING

            // Set the component's render mode
            locationComponent!!.renderMode = RenderMode.COMPASS

            // Add the location icon click listener
            locationComponent!!.addOnLocationClickListener(this)

            // Add the camera tracking listener. Fires if the map camera is manually moved.
            locationComponent!!.addOnCameraTrackingChangedListener(this)

            back_to_camera_tracking_mode.setOnClickListener {
                if (!isInTrackingMode) {
                    isInTrackingMode = true
                    locationComponent!!.cameraMode = CameraMode.TRACKING
                    locationComponent!!.zoomWhileTracking(16.0)
                } else {
                    requireActivity().toast(getString(R.string.tracking_already_enabled))
                }
            }

        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(requireActivity())
        }
    }

    override fun onLocationComponentClick() {
        if (locationComponent!!.lastKnownLocation != null) {
            requireActivity().toast("You Are Here!")
        }
    }

    override fun onCameraTrackingDismissed() {
        isInTrackingMode = false
    }

    override fun onCameraTrackingChanged(currentMode: Int) {
        // Empty on purpose
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String>) {
        requireActivity().toast(R.string.user_location_permission_explanation.toString())
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            enableLocationComponent(mapboxMap.style!!)
        } else {
            requireActivity().toast(R.string.user_location_permission_not_granted.toString())
            requireActivity().finish()
        }
    }

    override fun onResume() {
        super.onResume()
        map_view.onResume()
    }

    @SuppressWarnings("MissingPermission")
    override fun onStart() {
        super.onStart()
        map_view.onStart()
    }

    override fun onStop() {
        super.onStop()
        map_view.onStop()
    }

    override fun onPause() {
        super.onPause()
        map_view.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map_view.onLowMemory()
    }

    private fun reverseGeocode(point: Point) {
        try {
            val client: MapboxGeocoding = MapboxGeocoding.builder()
                .accessToken(getString(R.string.map_box_access_token))
                .query(Point.fromLngLat(point.longitude(), point.latitude()))
                .build()
            client.enqueueCall(object : Callback<GeocodingResponse?> {
                override fun onResponse(
                    call: Call<GeocodingResponse?>?,
                    response: Response<GeocodingResponse?>
                ) {
                    if (response.body() != null) {
                        val results: List<CarmenFeature> = response.body()!!.features()
                        if (results.isNotEmpty()) {
                            // If the geocoder returns a result, we take the first in the list and show a Toast with the place name.
                            mapboxMap.getStyle { style ->
                                if (style.getLayer(DROPPED_MARKER_LAYER_ID) != null) {
                                    val r = results[0].geometry() as Point
                                    viewModel.dataMasjid?.lat = r.latitude().toString()
                                    etLatMasjid.text = r.latitude().toString()
                                    viewModel.dataMasjid?.lon = r.longitude().toString()
                                    etLonMasjid.text = r.longitude().toString()
                                    tvButtonShowMaps.error = null
                                }
                            }
                        } else {
                            root_layout.snackbar(getString(R.string.location_picker_dropped_marker_snippet_no_results))
                        }
                    }
                }

                override fun onFailure(
                    call: Call<GeocodingResponse?>?,
                    throwable: Throwable
                ) {
                    Timber.e("Geocoding Failure: %s", throwable.message)
                }
            })
        } catch (servicesException: ServicesException) {
            Timber.e("Error geocoding: %s", servicesException.toString())
            servicesException.printStackTrace()
        }
    }
}
