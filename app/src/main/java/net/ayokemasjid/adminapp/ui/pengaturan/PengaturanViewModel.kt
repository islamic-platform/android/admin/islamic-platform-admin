package net.ayokemasjid.adminapp.ui.pengaturan

import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.model.PengaturanMasjid
import net.ayokemasjid.adminapp.data.model.PengaturanMasjidData
import net.ayokemasjid.adminapp.data.repository.PengaturanRepository
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred
import okhttp3.MultipartBody
import okhttp3.RequestBody

class PengaturanViewModel(
    private val repository: PengaturanRepository
): ViewModel() {

    var dataMasjid: PengaturanMasjid? = null
    var pengaturanListener: MainListener? = null

    var sliderFile: RequestBody? = null
    var sambutanFile: RequestBody? = null
    var logoFile: RequestBody? = null

    val masjidItems by lazyDeferred{
        repository.getAllMasjid()
    }

    fun setSlider(data: RequestBody?, name: String?){
        sliderFile = data
        dataMasjid?.slider_pic = name
    }

    fun setSambutan(data: RequestBody?, name: String?){
        sambutanFile = data
        dataMasjid?.greeting_pic = name
    }

    fun setLogo(data: RequestBody?, name: String?){
        logoFile = data
        dataMasjid?.logo = name
    }

    fun onPengaturanButtonClick(){
        Coroutines.main {
            try {
                val response = repository.pengaturanMasjid(
                    PengaturanMasjidData(
                        dataMasjid?.id_mosque!!,
                        dataMasjid?.email!!,
                        dataMasjid?.phone!!,
                        dataMasjid?.address!!,
                        dataMasjid?.city!!,
                        dataMasjid?.province!!,
                        dataMasjid?.country!!,
                        dataMasjid?.postcode!!,
                        dataMasjid?.lat!!,
                        dataMasjid?.lon!!
                    )
                )
                pengaturanListener?.getMessage(response.message!!, response.status)
            } catch (e: ApiException){
                pengaturanListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                pengaturanListener?.getMessage(e.message!!, false)
            }
        }
    }

    fun onProfileButtonClick(){
        Coroutines.main {
            try {
                val id_mosque = RequestBody.create(MultipartBody.FORM, dataMasjid?.id_mosque.toString())
                val name = RequestBody.create(MultipartBody.FORM, dataMasjid?.name.toString())
                val motto = RequestBody.create(MultipartBody.FORM, dataMasjid?.motto.toString())
                val width = RequestBody.create(MultipartBody.FORM, dataMasjid?.width.toString())
                val volume = RequestBody.create(MultipartBody.FORM, dataMasjid?.volume.toString())
                val about_sort = RequestBody.create(MultipartBody.FORM, dataMasjid?.about_short.toString())
                val about_long = RequestBody.create(MultipartBody.FORM, dataMasjid?.about_long.toString())
                val greeting = RequestBody.create(MultipartBody.FORM, dataMasjid?.greeting.toString())
                val slider_pic = MultipartBody.Part.createFormData("slider_pic", dataMasjid?.slider_pic, sliderFile!!)
                val greeting_pic = MultipartBody.Part.createFormData("greeting_pic", dataMasjid?.greeting_pic, sambutanFile!!)
                val logo = MultipartBody.Part.createFormData("logo", dataMasjid?.logo, logoFile!!)

                val response = repository.profileMasjid(id_mosque, name, motto, width, volume, about_sort, about_long, greeting, slider_pic, greeting_pic, logo)
                pengaturanListener?.getMessage(response.message!!, response.status)
            } catch (e: ApiException){
                pengaturanListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                pengaturanListener?.getMessage(e.message!!, false)
            }
        }
    }
}