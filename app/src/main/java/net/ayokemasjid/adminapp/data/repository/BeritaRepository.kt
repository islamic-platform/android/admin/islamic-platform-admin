package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.model.*
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.BeritaResponse
import net.ayokemasjid.adminapp.data.network.response.InfaqResponse
import net.ayokemasjid.adminapp.data.network.response.KategoriBeritaResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody

data class BeritaRepository(
    private val api: MyApi
): SafeApiRequest() {

    private val beritaItems = MutableLiveData<List<Berita>>()

    suspend fun addBerita(title: RequestBody, category: RequestBody, content: RequestBody, file: MultipartBody.Part): BeritaResponse {
        return apiRequest { api.addBerita(title, category, content, file) }
    }

    suspend fun editBerita(beritaData: BeritaData): BeritaResponse {
        return apiRequest { api.editBerita(beritaData) }
    }

    suspend fun deleteBerita(idBerita : Int): BeritaResponse {
        return apiRequest { api.deleteBerita(idBerita) }
    }

    suspend fun getAllBerita(): LiveData<List<Berita>> {
        fetchAllBerita()
        return withContext(Dispatchers.IO) {
            beritaItems
        }
    }

    private suspend fun fetchAllBerita() {
        try {
            val response = apiRequest { api.getBerita() }
            beritaItems.postValue(response.data)
        } catch (e: Exception) {
            val empty: List<Berita> = ArrayList()
            beritaItems.postValue(empty)
            e.printStackTrace()
        }
    }

    private val kategoriBeritaItems = MutableLiveData<List<KategoriBerita>>()

    suspend fun addKategoriBerita(kategoriBeritaData: KategoriBeritaData): KategoriBeritaResponse {
        return apiRequest { api.addKategoriBerita(kategoriBeritaData) }
    }

    suspend fun editKategoriBerita(kategoriBerita: KategoriBerita): KategoriBeritaResponse {
        return apiRequest { api.editKategoriBerita(kategoriBerita) }
    }

    suspend fun deleteKategoriBerita(id_category : Int): KategoriBeritaResponse {
        return apiRequest { api.deleteKategoriBerita(id_category) }
    }

    suspend fun getAllKategoriBerita(): LiveData<List<KategoriBerita>> {
        fetchAllKategoriBerita()
        return withContext(Dispatchers.IO) {
            kategoriBeritaItems
        }
    }

    private suspend fun fetchAllKategoriBerita() {
        try {
            val response = apiRequest { api.getKategoriBerita() }
            kategoriBeritaItems.postValue(response.data)
        } catch (e: Exception) {
            val empty: List<KategoriBerita> = ArrayList()
            kategoriBeritaItems.postValue(empty)
            e.printStackTrace()
        }
    }
}