package net.ayokemasjid.adminapp.ui.jamaah

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Resources
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.JsonObject
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.geocoding.v5.models.CarmenFeature
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.*
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconOffset
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.mapboxsdk.utils.BitmapUtils
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_list_jamaah.*
import kotlinx.android.synthetic.main.activity_list_jamaah.btnExcel
import kotlinx.android.synthetic.main.activity_list_jamaah.btnSaveAs
import kotlinx.android.synthetic.main.activity_list_jamaah.layout_btnSave
import kotlinx.android.synthetic.main.activity_list_jamaah.recyclerview
import kotlinx.android.synthetic.main.activity_list_jamaah.root_layout
import kotlinx.android.synthetic.main.activity_list_jamaah.toolbar
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Jamaah
import net.ayokemasjid.adminapp.databinding.ActivityListJamaahBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.ui.home.Location
import net.ayokemasjid.adminapp.util.*
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.io.FileOutputStream
import java.util.*
import kotlin.collections.ArrayList

//maps: OnMapReadyCallback, PermissionsListener, OnLocationClickListener, OnCameraTrackingChangedListener
class ListJamaahActivity : AppCompatActivity(), MainListener, KodeinAware, OnMapReadyCallback, PermissionsListener,
    OnLocationClickListener, OnCameraTrackingChangedListener {

    override val kodein by kodein()
    private val factory by instance<JamaahViewModelFactory>()

    private lateinit var binding: ActivityListJamaahBinding
    private lateinit var viewModel: JamaahViewModel

    //maps
    private lateinit var permissionsManager: PermissionsManager
    private lateinit var mapboxMap: MapboxMap
    private var locationComponent: LocationComponent? = null
    private var isInTrackingMode = false
    val REQUEST_CODE_AUTOCOMPLETE = 1
    private val geojsonSourceLayerId = "geojsonSourceLayerId"
    private val symbolIconId = "symbolIconId"
    private var locationList: MutableList<Location> = ArrayList()

    private var isLayoutSaveVisible = false

    private var dataExcel: StringBuilder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //maps
        Mapbox.getInstance(this, getString(R.string.map_box_access_token))

        setContentView(R.layout.activity_list_jamaah)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_jamaah)
        viewModel = ViewModelProvider(this, factory).get(JamaahViewModel::class.java)

        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.jamaahListener = this

        progress_bar2.show()
        progress_bar.show()
        val behavior = BottomSheetBehavior.from(bottomSheet)
        behavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED
        //recycle view
        bindUI()
        setListener()

        var displayHeight: Int
        var mapsHeight: Int
        lifecycleScope.launch{
            delay(2000)
            //maps
            initMapView(savedInstanceState)
            initPermissions()
            syncMapbox()

            displayHeight = Resources.getSystem().displayMetrics.heightPixels
            mapsHeight = layout_maps.height + toolbar.height + 50
            changePeek(displayHeight, mapsHeight, behavior)
            progress_bar2.hide()
        }

        lifecycleScope.launch {
            delay(3000)
            progress_bar.hide()
        }
    }

    private fun changePeek(
        displayHeight: Int,
        mapsHeight: Int,
        behavior: BottomSheetBehavior<CoordinatorLayout>
    ) {
        behavior.state = BottomSheetBehavior.STATE_EXPANDED
        behavior.peekHeight = displayHeight - mapsHeight
        behavior.state = BottomSheetBehavior.STATE_COLLAPSED
    }

    //maps
    fun refresh(view: View) {
        val intent: Intent = intent
        finish()
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    //recycle view
    private fun bindUI() = Coroutines.main {
        //maps
        back_to_camera_tracking_mode.isClickable = false
        fab_location_search.isClickable = false

        //recycle view
        viewModel.jamaahItems.await().observe(this, Observer {
            //maps
            if (!it.isNullOrEmpty()) {
                for (x in it.indices) {
                    if (!it[x].lat.isNullOrEmpty() && !it[x].lon.isNullOrEmpty()) {
                        val item = Location(
                            it[x].lat!!.toDouble(),
                            it[x].lon!!.toDouble(),
                            it[x].name!!,
                            it[x].telephone!!
                        )
                        locationList.add(item)
                    }
                }
            }

            //recycle view
            initRecyclerView(it.toJamaahItem())

            //maps
            back_to_camera_tracking_mode.isClickable = true
            fab_location_search.isClickable = true

            //add data to excel
            val currentDate = getCurrentDate()
            dataExcel = StringBuilder()
            dataExcel!!.append("Data Jamaah $currentDate")
            dataExcel!!.append("No,Name,Gender,Email,Phone Number,Address,Information,Latitude/Longitude,Date Joined")
            if (!it.isNullOrEmpty()) {
                for ((x, data) in it.toJamaahItem().withIndex()) {
                    val number = x + 1
                    val address = data.jamaah.address!!
                    val city = data.jamaah.city!!
                    val province = data.jamaah.province!!
                    val country = data.jamaah.country!!
                    val postcode = data.jamaah.postcode!!
                    val fullAddress = "$address, $city, $province, $country ($postcode)"

                    val lat = data.jamaah.lat!!
                    val lon = data.jamaah.lon!!
                    val coordinate = "$lat/$lon"

                    val name = data.jamaah.name!!
                    val gender = data.jamaah.gender!!
                    val email = data.jamaah.email!!
                    val telp = data.jamaah.telephone!!
                    val info = data.jamaah.information!!
                    val dateJoined = data.jamaah.date_insert!!

                    dataExcel!!.append(
                        "\n\"$number\",\"$name\",\"$gender\",\"$email\",\"$telp\",\"$fullAddress\",\"$info\",\"$coordinate\",\"$dateJoined\""
                    )
                }
            }
        })
    }

    private fun exportExcel(data: StringBuilder?){
        try {
            val currentDate = getCurrentDate()
            //saving the file into device
            val out: FileOutputStream = openFileOutput("dataJamaah[$currentDate].csv", Context.MODE_PRIVATE)
            out.write(data.toString().toByteArray())
            out.close()

            //exporting
            val context: Context = applicationContext
            val fileLocation = File(filesDir, "dataJamaah[$currentDate].csv")
            val path: Uri = FileProvider.getUriForFile(
                context,
                "net.ayokemasjid.adminapp.fileprovider",
                fileLocation
            )
            val fileIntent = Intent(Intent.ACTION_SEND)
            val resInfoList =
                packageManager.queryIntentActivities(fileIntent, PackageManager.MATCH_DEFAULT_ONLY)
            for (resolveInfo in resInfoList) {
                val packageName = resolveInfo.activityInfo.packageName
                applicationContext.grantUriPermission(
                    packageName,
                    path,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
            }
            fileIntent.type = "text/csv"
            fileIntent.putExtra(Intent.EXTRA_SUBJECT, "dataJamaah[$currentDate]")
            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            fileIntent.putExtra(Intent.EXTRA_STREAM, path)
            startActivity(Intent.createChooser(fileIntent, "Send via"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getCurrentDate(): String{
        val current = Calendar.getInstance()
        val dayOfMonth = current.get(Calendar.DAY_OF_MONTH)
        val bulan = current.get(Calendar.MONTH)
        val year = current.get(Calendar.YEAR)

        return "$year-$bulan-$dayOfMonth"
    }

    private fun setListener(){
        btnAddJamaah.setOnClickListener {
            startActivity(Intent(this, AddJamaahActivity::class.java))
        }

        btnSaveAs.setOnClickListener {
            if(isLayoutSaveVisible){
                layout_btnSave.visibility = View.GONE
                isLayoutSaveVisible = false
            } else{
                layout_btnSave.visibility = View.VISIBLE
                isLayoutSaveVisible = true
            }
        }

        btnExcel.setOnClickListener {
            exportExcel(dataExcel)
        }
    }

    //recycle view
    private fun List<Jamaah>.toJamaahItem() : List<JamaahItem>{
        return this.map {
            JamaahItem(it, viewModel, this@ListJamaahActivity)
        }
    }

    //recycle view
    private fun initRecyclerView(jamaahItem: List<JamaahItem>){
        val mAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(jamaahItem)
        }

        recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = mAdapter
        }
    }

    //button back di topbar
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        progress_bar2.show()
        progress_bar.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        progress_bar2.hide()
        progress_bar.hide()
        root_layout.snackbar(message)
    }

    //maps
    private fun initMapView(savedInstanceState: Bundle?) {
        map_view.onCreate(savedInstanceState)
    }

    //maps
    override fun onMapReady(mapboxMap: MapboxMap) {
        mapboxMap.setStyle(Style.MAPBOX_STREETS){
            initSearchFab()
            addMarkerIconsToMap(it)
            enableLocationComponent(it)

            // Create an empty GeoJSON source using the empty feature collection
            setUpSource(it)

            // Set up a new symbol layer for displaying the searched location's feature coordinates
            setupLayer(it)
        }
        this.mapboxMap = mapboxMap
    }

    //maps search bar
    private fun initSearchFab() {
        if (PermissionsManager.areLocationPermissionsGranted(this)){
            val list: List<CarmenFeature> = addUserLocations()
            fab_location_search.setOnClickListener {
                var builder : PlaceOptions.Builder? = PlaceOptions.builder()
                if(!list.isNullOrEmpty()){
                    val item = CarmenFeature.builder().text("Search For More Jamaah")
                        .placeName("Ketik sesuatu di search bar")
                        .geometry(
                            Point.fromLngLat(
                                locationComponent!!.lastKnownLocation!!.longitude,
                                locationComponent!!.lastKnownLocation!!.latitude
                            )
                        )
                        .id("first")
                        .properties(JsonObject())
                        .build()
                    builder = PlaceOptions.builder().addInjectedFeature(item)

                    for(element in list){
                        builder!!.addInjectedFeature(element)
                    }

                }
                val intent: Intent = PlaceAutocomplete.IntentBuilder()
                    .accessToken(
                        if (Mapbox.getAccessToken() != null) Mapbox.getAccessToken()
                            .toString() else getString(
                            R.string.map_box_access_token
                        )
                    )
                    .placeOptions(
                        builder!!
                            .backgroundColor(Color.parseColor("#EEEEEE"))
                            .limit(list.size)
                            .build(PlaceOptions.MODE_CARDS)
                    )
                    .build(this@ListJamaahActivity)
                startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE)
            }
        }
    }

    //maps add location(s) pin
    private fun addUserLocations(): ArrayList<CarmenFeature> {
        val list : ArrayList<CarmenFeature> = ArrayList()
        if(!locationList.isNullOrEmpty()){
            for(x in 0 until locationList.size){
                val item = CarmenFeature.builder().text(locationList[x].nama)
                    .geometry(Point.fromLngLat(locationList[x].long, locationList[x].lat))
                    .placeName(locationList[x].telp)
                    .id("jamaah-$x")
                    .properties(JsonObject())
                    .build()
                list.add(item)
            }
        }
        return list
    }

    //maps go to current location
    private fun setUpSource(@NonNull loadedMapStyle: Style) {
        loadedMapStyle.addSource(GeoJsonSource(geojsonSourceLayerId))
    }

    //maps go to current location
    private fun setupLayer(loadedMapStyle: Style) {
        loadedMapStyle.addLayer(
            SymbolLayer("SYMBOL_LAYER_ID", geojsonSourceLayerId).withProperties(
                iconImage(symbolIconId),
                iconOffset(arrayOf(0f, -8f))
            )
        )
    }

    //maps
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {

            // Retrieve selected location's CarmenFeature
            val selectedCarmenFeature = PlaceAutocomplete.getPlace(data)

            // Create a new FeatureCollection and add a new Feature to it using selectedCarmenFeature above.
            // Then retrieve and update the source designated for showing a selected location's symbol layer icon
            if (mapboxMap != null) {
                val style = mapboxMap.style
                if (style != null) {
                    val source =
                        style.getSourceAs<GeoJsonSource>(geojsonSourceLayerId)
                    source?.setGeoJson(
                        FeatureCollection.fromFeatures(
                            arrayOf(
                                Feature.fromJson(
                                    selectedCarmenFeature.toJson()
                                )
                            )
                        )
                    )

                    // Move map camera to the selected location
                    mapboxMap.animateCamera(
                        CameraUpdateFactory.newCameraPosition(
                            CameraPosition.Builder()
                                .target(
                                    LatLng(
                                        (selectedCarmenFeature.geometry() as Point?)!!.latitude(),
                                        (selectedCarmenFeature.geometry() as Point?)!!.longitude()
                                    )
                                )
                                .zoom(14.0)
                                .build()
                        ), 4000
                    )
                }
            }
        }
    }

    //maps add location(s) pin
    private fun addMarkerIconsToMap(loadedMapStyle: Style) {
        BitmapUtils.getBitmapFromDrawable(getDrawable(R.drawable.red_marker))?.let {
            loadedMapStyle.addImage("icon-id", it)
        }
        if(!locationList.isNullOrEmpty()){
            val feature: ArrayList<Feature> = ArrayList()
            for (x in 0 until locationList.size){
                feature.add(
                    Feature.fromGeometry(
                        Point.fromLngLat(
                            locationList[x].long,
                            locationList[x].lat
                        )
                    )
                )
            }

            loadedMapStyle.addSource(
                GeoJsonSource(
                    "source-id", FeatureCollection.fromFeatures(
                        feature
                    )
                )
            )
            loadedMapStyle.addLayer(
                SymbolLayer("layer-id", "source-id").withProperties(
                    iconImage("icon-id"), iconOffset(
                        arrayOf(
                            0f,
                            -8f
                        )
                    )
                )
            )
        }
    }

    //maps
    private fun initPermissions() {
        val permissionListener = object : PermissionsListener {
            override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
                /* Nothing to do in here */
            }

            override fun onPermissionResult(granted: Boolean) {
                if (granted) {
                    syncMapbox()
                } else {
                    val alertDialogInfo = AlertDialog.Builder(this@ListJamaahActivity)
                        .setTitle(getString(R.string.info))
                        .setCancelable(false)
                        .setMessage(getString(R.string.permissions_denied))
                        .setPositiveButton(getString(R.string.dismiss)) { _, _ ->
                            finish()
                        }
                        .create()
                    alertDialogInfo.show()
                }
            }
        }
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            syncMapbox()
        } else {
            permissionsManager = PermissionsManager(permissionListener)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    //maps
    private fun syncMapbox() {
        map_view.getMapAsync(this)
    }

    //maps current location
    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            // Create and customize the LocationComponent's options
            val customLocationComponentOptions = LocationComponentOptions.builder(this)
                .trackingGesturesManagement(true)
                .elevation(5F)
                .accuracyAlpha(.6f)
                .accuracyColor(ContextCompat.getColor(this, R.color.mapboxGreen))
                .foregroundDrawable(R.drawable.ic_location_on)
                .build()

            // Get an instance of the component
            locationComponent = mapboxMap.locationComponent

            val locationComponentActivationOptions =
                LocationComponentActivationOptions.builder(this, loadedMapStyle)
                    .locationComponentOptions(customLocationComponentOptions)
                    .build()

            // Activate with options
            locationComponent!!.activateLocationComponent(locationComponentActivationOptions)

            // Enable to make component visible
            locationComponent!!.isLocationComponentEnabled = true

            // Set the component's camera mode
            locationComponent!!.cameraMode = CameraMode.TRACKING

            // Set the component's render mode
            locationComponent!!.renderMode = RenderMode.COMPASS

            // Add the location icon click listener
            locationComponent!!.addOnLocationClickListener(this)

            // Add the camera tracking listener. Fires if the map camera is manually moved.
            locationComponent!!.addOnCameraTrackingChangedListener(this)

            back_to_camera_tracking_mode.setOnClickListener {
                if (!isInTrackingMode) {
                    isInTrackingMode = true
                    locationComponent!!.cameraMode = CameraMode.TRACKING
                    locationComponent!!.zoomWhileTracking(16.0)
                } else {
                    toast(getString(R.string.tracking_already_enabled))
                }
            }

        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    //maps current location
    override fun onLocationComponentClick() {
        if (locationComponent!!.lastKnownLocation != null) {
            toast("You Are Here!")
        }
    }

    //maps
    override fun onCameraTrackingDismissed() {
        isInTrackingMode = false
    }

    //maps
    override fun onCameraTrackingChanged(currentMode: Int) {
        // Empty on purpose
    }

    //maps
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    //maps
    override fun onExplanationNeeded(permissionsToExplain: List<String>) {
        toast(R.string.user_location_permission_explanation.toString())
    }

    //maps
    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            enableLocationComponent(mapboxMap.style!!)
        } else {
            toast(R.string.user_location_permission_not_granted.toString())
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        map_view.onResume()
    }

    @SuppressWarnings("MissingPermission")
    override fun onStart() {
        super.onStart()
        map_view.onStart()
    }

    override fun onStop() {
        super.onStop()
        map_view.onStop()
    }

    override fun onPause() {
        super.onPause()
        map_view.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map_view.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        map_view.onDestroy()
    }

    override fun onRestart() {
        super.onRestart()
        refresh(root_layout)
    }
}