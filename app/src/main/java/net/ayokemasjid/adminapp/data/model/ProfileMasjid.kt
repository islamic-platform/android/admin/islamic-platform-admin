package net.ayokemasjid.adminapp.data.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

//POST Mosque (Edit Profile)
data class ProfileMasjid (
    var name: String? = null,
    var motto: String? = null,
    var slider_pic: String? = null,
    var width: String? = null,
    var volume: String? = null,
    var about_short: String? = null,
    var about_long: String? = null,
    var greeting: String? = null,
    var greeting_pic: String? = null,
    var logo: String? = null
): Serializable