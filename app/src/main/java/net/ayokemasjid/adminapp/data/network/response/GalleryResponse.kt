package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.Gallery

class GalleryResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<Gallery>?
)