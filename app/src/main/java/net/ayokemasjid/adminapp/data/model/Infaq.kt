package net.ayokemasjid.adminapp.data.model

import java.io.Serializable

data class Infaq (
    var id_infaq: Int? = null,
    var category: String? = null,
    var total: String? = null,
    var info: String? = null,
    var date: String? = null
): Serializable