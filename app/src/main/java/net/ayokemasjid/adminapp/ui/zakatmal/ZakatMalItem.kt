package net.ayokemasjid.adminapp.ui.zakatmal

import android.os.Bundle
import com.xwray.groupie.databinding.BindableItem
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.ZakatMal
import net.ayokemasjid.adminapp.databinding.ItemZakatMalBinding

class ZakatMalItem(
    val zakatMal: ZakatMal,
    private val viewModel: ZakatMalViewModel,
    val activity: ListZakatMalActivity
): BindableItem<ItemZakatMalBinding>() {

    override fun getLayout() = R.layout.item_zakat_mal

    override fun bind(viewBinding: ItemZakatMalBinding, position: Int) {
        viewBinding.zakatmal = zakatMal
        viewBinding.viewmodel = viewModel

        viewBinding.btnDeleteZakatMal.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("ID", zakatMal.id_zakat_mal!!)

            val fm = activity.supportFragmentManager
            val fragment = DeleteZakatMalDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Delete Dialog")
        }
    }
}