package net.ayokemasjid.adminapp.data.model

import java.io.Serializable

data class Kegiatan (
    var id_task: Int? = null,
    var title: String? = null,
    var people: String? = null,
    var information: String? = null,
    var date: String? = null,
    var main_image: String? = null
): Serializable