package net.ayokemasjid.adminapp.data.model

import java.io.Serializable
//GET Mosque
data class PengaturanMasjid (
    var id_mosque: Int? = null,
    var name: String? = null,
    var email: String? = null,
    var phone: String? = null,
    var address: String? = null,
    var city: String? = null,
    var province: String? = null,
    var country: String? = null,
    var postcode: String? = null,
    var lat: String? = null,
    var lon: String? = null,
    var motto: String? = null,
    var slider_pic: String? = null,
    var width: String? = null,
    var volume: String? = null,
    var about_short: String? = null,
    var about_long: String? = null,
    var greeting: String? = null,
    var greeting_pic: String? = null,
    var logo: String? = null,
    var date_installed: String? = null,
): Serializable