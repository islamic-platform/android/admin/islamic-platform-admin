package net.ayokemasjid.adminapp.ui.user

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_add_user.*
import kotlinx.android.synthetic.main.activity_list_user.*
import kotlinx.android.synthetic.main.activity_list_user.root_layout
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.User
import net.ayokemasjid.adminapp.databinding.ActivityListUserBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.hide
import net.ayokemasjid.adminapp.util.snackbar
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class ListUserActivity : AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private val factory by instance<UserViewModelFactory>()

    private lateinit var binding: ActivityListUserBinding
    private lateinit var viewModel: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_list_user)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_user)
        viewModel = ViewModelProvider(this, factory).get(UserViewModel::class.java)

        swipeRefreshLayout.isRefreshing = true
        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel

        //recycle view
        bindUI()
        setListener()
    }

    //recycle view
    private fun bindUI() = Coroutines.main {
        //recycle view
        viewModel.userItems.await().observe(this, {
            //recycle view
            initRecyclerView(it.toUserItem())
        })

        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun setListener(){
        btnAdd.setOnClickListener {
            AddDialogFragment().show(supportFragmentManager, "AddDialogFragment")
        }

        swipeRefreshLayout.setOnRefreshListener {
            refresh(root_layout)
        }

    }

    //recycle view
    private fun List<User>.toUserItem() : List<UserItem>{
        return this.map {
            UserItem(it, viewModel, this@ListUserActivity)
        }
    }

    //recycle view
    private fun initRecyclerView(jamaahItem: List<UserItem>){
        val mAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(jamaahItem)
        }

        recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = mAdapter
        }
    }

    //button back di topbar
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun refresh(view: View) {
        val intent: Intent = intent
        finish()
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    override fun onRestart() {
        super.onRestart()
        refresh(root_layout)
    }
}