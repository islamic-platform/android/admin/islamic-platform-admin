package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.ZakatFitrah

data class ZakatFitrahResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<ZakatFitrah>?
)
