package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.model.Infaq
import net.ayokemasjid.adminapp.data.model.InfaqData
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.InfaqResponse

data class InfaqRepository(
    private val api: MyApi
): SafeApiRequest() {

    private val infaqItems = MutableLiveData<List<Infaq>>()

    suspend fun addInfaq(infaqData: InfaqData): InfaqResponse {
        return apiRequest { api.addInfaq(infaqData) }
    }

    suspend fun editInfaq(infaq: Infaq): InfaqResponse {
        return apiRequest { api.editInfaq(infaq) }
    }

    suspend fun deleteInfaq(idInfaq : Int): InfaqResponse {
        return apiRequest { api.deleteInfaq(idInfaq) }
    }

    suspend fun getAllInfaq(): LiveData<List<Infaq>> {
        fetchAllInfaq()
        return withContext(Dispatchers.IO) {
            infaqItems
        }
    }

    private suspend fun fetchAllInfaq() {
        try {
            val response = apiRequest { api.getInfaq() }
            infaqItems.postValue(response.data)
        } catch (e: Exception) {
            val empty: List<Infaq> = ArrayList()
            infaqItems.postValue(empty)
            e.printStackTrace()
        }
    }
}