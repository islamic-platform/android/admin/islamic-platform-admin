package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.KategoriBerita

class KategoriBeritaResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<KategoriBerita>?
)