package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.model.EditPengurus
import net.ayokemasjid.adminapp.data.model.Pengurus
import net.ayokemasjid.adminapp.data.model.PengurusData
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.PengurusResponse

data class PengurusRepository(
    private val api: MyApi
): SafeApiRequest() {

    private val pengurusItems = MutableLiveData<List<Pengurus>>()

    suspend fun addPengurus(pengurusData: PengurusData): PengurusResponse {
        return apiRequest { api.addPengurus(pengurusData) }
    }

    suspend fun editPengurus(pengurus: EditPengurus): PengurusResponse {
        return apiRequest { api.editPengurus(pengurus) }
    }

    suspend fun deletePengurus(idPengurus : Int): PengurusResponse {
        return apiRequest { api.deletePengurus(idPengurus) }
    }

    suspend fun getAllPengurus(): LiveData<List<Pengurus>> {
        fetchAllPengurus()
        return withContext(Dispatchers.IO) {
            pengurusItems
        }
    }

    private suspend fun fetchAllPengurus() {
        try {
            val response = apiRequest { api.getPengurus() }
            pengurusItems.postValue(response.data)
        } catch (e: Exception) {
            val empty: List<Pengurus> = ArrayList()
            pengurusItems.postValue(empty)
            e.printStackTrace()
        }
    }
}