package net.ayokemasjid.adminapp.ui.kegiatan

import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.model.KegiatanData
import net.ayokemasjid.adminapp.data.repository.KegiatanRepository
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred
import okhttp3.MultipartBody
import okhttp3.RequestBody

class KegiatanViewModel(
    private val repository: KegiatanRepository
): ViewModel()   {

    var idKegiatan: Int? = null
    var title: String? = null
    var people: String? = null
    var information: String? = null
    var date: String? = null
    var filePart: RequestBody? = null
    var image_name: String? = null

    fun setImage(data: RequestBody?, name: String?){
        filePart = data
        image_name = name
    }

    var kegiatanListener: MainListener? = null

    val kegiatanItems by lazyDeferred{
        repository.getAllKegiatan()
    }

    fun onAddButtonClick(){
        kegiatanListener?.onStarted()
        Coroutines.main {
            try {
                val titleBody = RequestBody.create(MultipartBody.FORM, title.toString())
                val peopleBody = RequestBody.create(MultipartBody.FORM, people.toString())
                val informationBody = RequestBody.create(MultipartBody.FORM, information.toString())
                val dateBody = RequestBody.create(MultipartBody.FORM, date.toString())
                val file = MultipartBody.Part.createFormData("main_image", image_name, filePart!!)

                val kegiatanResponse = repository.addKegiatan(titleBody,peopleBody,informationBody,dateBody,file)
                kegiatanListener?.getMessage(kegiatanResponse.message!!, kegiatanResponse.status)
            } catch (e: ApiException){
                kegiatanListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                kegiatanListener?.getMessage(e.message!!, false)
            }
        }
    }

    fun onEditButtonClick(){
        kegiatanListener?.onStarted()
        Coroutines.main{
            try {
                val data = KegiatanData(idKegiatan!!, title!!, people!!, information!!, date!!)
                val kegiatanResponse = repository.editKegiatan(data)
                kegiatanListener?.getMessage(kegiatanResponse.message!!, kegiatanResponse.status)
            } catch (e: ApiException){
                kegiatanListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                kegiatanListener?.getMessage(e.message!!, false)
            }
        }
    }

    fun onDeleteButton(idKegiatan: Int){
        kegiatanListener?.onStarted()
        Coroutines.main {
            try{
                val respon = repository.deleteKegiatan(idKegiatan)
                kegiatanListener?.getMessage(respon.message!!, false)
            } catch (e: ApiException){
                kegiatanListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                kegiatanListener?.getMessage(e.message!!, false)
            }
        }
    }
}