package net.ayokemasjid.adminapp.ui.zakatmal

import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.model.ZakatMalData
import net.ayokemasjid.adminapp.data.repository.ZakatMalRepository
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred

class ZakatMalViewModel(
    private val repository: ZakatMalRepository
): ViewModel()  {

    var date: String? = null
    var name: String? = null
    var total: String? = null
    var info: String? = null

    var zakatMalListener: MainListener? = null

    val zakatMalItems by lazyDeferred{
        repository.getAllZakatMal()
    }

    fun onAddButtonClick(){
        zakatMalListener?.onStarted()
        Coroutines.main {
            try {
                val data = ZakatMalData(date!!,name!!,total!!,info!!)
                val zakatMalResponse = repository.addZakatMal(data)
                zakatMalListener?.getMessage(zakatMalResponse.message!!,zakatMalResponse.status)
            } catch (e: ApiException){
                zakatMalListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                zakatMalListener?.getMessage(e.message!!,false)
            }
        }
    }

    fun onDeleteButton(idZakatMal : Int){
        zakatMalListener?.onStarted()
        Coroutines.main {
            try{
                val respon = repository.deleteZakatMal(idZakatMal)
                zakatMalListener?.getMessage(respon.message!!,false)
            } catch (e: ApiException){
                zakatMalListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                zakatMalListener?.getMessage(e.message!!,false)
            }
        }
    }
}