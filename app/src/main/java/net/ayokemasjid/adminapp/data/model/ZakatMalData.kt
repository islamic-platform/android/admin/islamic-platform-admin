package net.ayokemasjid.adminapp.data.model

import com.google.gson.annotations.SerializedName

data class ZakatMalData(
    @SerializedName("date")
    var date: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("total")
    var total: String,
    @SerializedName("info")
    var info: String
)