package net.ayokemasjid.adminapp.ui.pengurus

import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.model.EditPengurus
import net.ayokemasjid.adminapp.data.model.Pengurus
import net.ayokemasjid.adminapp.data.model.PengurusData
import net.ayokemasjid.adminapp.data.repository.PengurusRepository
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred

class PengurusViewModel(
    private val repository: PengurusRepository
): ViewModel()   {

    var idPengurus: Int? = null
    var name: String? = null
    var position: String? = null

    var pengurusListener: MainListener? = null

    val pengurusItems by lazyDeferred{
        repository.getAllPengurus()
    }

    fun setJenis(data: String){
        position = data
    }

    fun onAddButtonClick(){
        pengurusListener?.onStarted()
        Coroutines.main {
            try {
                val data = PengurusData(name!!,position!!)
                val pengurusResponse = repository.addPengurus(data)
                pengurusListener?.getMessage(pengurusResponse.message!!,pengurusResponse.status)
            } catch (e: ApiException){
                pengurusListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                pengurusListener?.getMessage(e.message!!,false)
            }
        }
    }

    fun onEditButtonClick(){
        pengurusListener?.onStarted()
        Coroutines.main{
            try {
                val data = EditPengurus(idPengurus!!,name!!,position!!)
                val pengurusResponse = repository.editPengurus(data)
                pengurusListener?.getMessage(pengurusResponse.message!!,pengurusResponse.status)
            } catch (e: ApiException){
                pengurusListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                pengurusListener?.getMessage(e.message!!,false)
            }
        }
    }

    fun onDeleteButton(idPengurus : Int){
        pengurusListener?.onStarted()
        Coroutines.main {
            try{
                val respon = repository.deletePengurus(idPengurus)
                pengurusListener?.getMessage(respon.message!!,false)
            } catch (e: ApiException){
                pengurusListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                pengurusListener?.getMessage(e.message!!,false)
            }
        }
    }
}