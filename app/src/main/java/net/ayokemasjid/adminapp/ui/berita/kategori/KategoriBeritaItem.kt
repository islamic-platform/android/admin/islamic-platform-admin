package net.ayokemasjid.adminapp.ui.berita.kategori

import android.content.Intent
import android.os.Bundle
import com.xwray.groupie.databinding.BindableItem
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.KategoriBerita
import net.ayokemasjid.adminapp.databinding.FragmentDeleteBeritaDialogBinding
import net.ayokemasjid.adminapp.databinding.ItemBeritaBinding
import net.ayokemasjid.adminapp.databinding.ItemKategoriBeritaBinding
import net.ayokemasjid.adminapp.ui.berita.*

class KategoriBeritaItem (
    val category: KategoriBerita,
    private val viewModel: BeritaViewModel,
    val activity: ListKategoriBeritaActivity
): BindableItem<ItemKategoriBeritaBinding>() {

    override fun getLayout() = R.layout.item_kategori_berita

    override fun bind(viewBinding: ItemKategoriBeritaBinding, position: Int) {
        viewBinding.category = category
        viewBinding.viewmodel = viewModel

        viewBinding.btnEditBerita.setOnClickListener {
            val intent = Intent(activity.baseContext, AddKategoriBeritaActivity::class.java)
            intent.putExtra("data", category)
            intent.putExtra("status","edit")
            activity.startActivity(intent)
        }
        viewBinding.btnDeleteBerita.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("ID", category.id_category!!)

            val fm = activity.supportFragmentManager
            val fragment = DeleteKategoriBeritaDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Delete Dialog")
        }
    }
}