package net.ayokemasjid.adminapp.ui.home

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.app.ActivityCompat.requestPermissions
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.fragment_manajemen.*
import net.ayokemasjid.adminapp.data.preferences.PreferenceProvider
import net.ayokemasjid.adminapp.databinding.FragmentManajemenBinding
import net.ayokemasjid.adminapp.ui.berita.AddBeritaActivity
import net.ayokemasjid.adminapp.ui.berita.ListBeritaActivity
import net.ayokemasjid.adminapp.ui.infaq.AddInfaqActivity
import net.ayokemasjid.adminapp.ui.infaq.ListInfaqActivity
import net.ayokemasjid.adminapp.ui.jamaah.AddJamaahActivity
import net.ayokemasjid.adminapp.ui.jamaah.ListJamaahActivity
import net.ayokemasjid.adminapp.ui.kegiatan.AddKegiatanActivity
import net.ayokemasjid.adminapp.ui.kegiatan.ListKegiatanActivity
import net.ayokemasjid.adminapp.ui.pengaturan.PengaturanActivity
import net.ayokemasjid.adminapp.ui.pengurus.AddPengurusActivity
import net.ayokemasjid.adminapp.ui.pengurus.ListPengurusActivity
import net.ayokemasjid.adminapp.ui.zakatfitrah.AddZakatFitrahActivity
import net.ayokemasjid.adminapp.ui.zakatfitrah.ListZakatFitrahActivity
import net.ayokemasjid.adminapp.ui.zakatmal.AddZakatMalActivity
import net.ayokemasjid.adminapp.ui.zakatmal.ListZakatMalActivity
import net.ayokemasjid.adminapp.util.Coroutines
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*


class ManajemenFragment : DialogFragment(), KodeinAware {

    override val kodein by kodein()
    private lateinit var viewModel: HomeViewModel
    private val factory by instance<HomeViewModelFactory>()
    private val preference by instance<PreferenceProvider>()

    private var _binding: FragmentManajemenBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentManajemenBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (activity as AppCompatActivity).supportActionBar?.title = "Laporan"
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        setButtonListener()
        setData()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setData() = Coroutines.main{
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = true
        }

        viewModel.totalInfaqMingguan.await().observe(this, {
            val dc = DecimalFormat("#,##0.00")
            val format = DecimalFormatSymbols.getInstance(Locale.ENGLISH)
            format.decimalSeparator = ','
            format.groupingSeparator = '.'
            dc.decimalFormatSymbols = format
            etTotalInfaq.text = dc.format(it)
        })
        viewModel.totalZakatFitrah.await().observe(this, {
            val dc = DecimalFormat("#,##0.00")
            val format = DecimalFormatSymbols.getInstance(Locale.ENGLISH)
            format.decimalSeparator = ','
            format.groupingSeparator = '.'
            dc.decimalFormatSymbols = format
            etTotalZakatFitrah.text = dc.format(it)
        })
        viewModel.totalZakatMal.await().observe(this, {
            val dc = DecimalFormat("#,##0.00")
            val format = DecimalFormatSymbols.getInstance(Locale.ENGLISH)
            format.decimalSeparator = ','
            format.groupingSeparator = '.'
            dc.decimalFormatSymbols = format
            etTotalZakatMal.text = dc.format(it)
        })
        viewModel.totalJamaah.await().observe(this, {
            etTotalJamaah.text = it.toString()
        })
        viewModel.totalBerita.await().observe(this, {
            etJumlahBerita.text = it.toString()
            etInfoUrl.text = preference.getDomainURL()
        })
        viewModel.totalKegiatan.await().observe(this, {
            etTotalKegiatan.text = it.toString()
        })
        viewModel.getMasjid.await().observe(this, {
            etSambutan.text = it.greeting.toString()
            etTanggalSambutan.text = getCurrentDate()
        })
        viewModel.totalPengurus.await().observe(this, {
            etTotalPengurus.text = it.toString()
        })

        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun getCurrentDate(): String{
        val current = Calendar.getInstance()
        val dayOfMonth = current.get(Calendar.DAY_OF_MONTH)
        val bulan = current.get(Calendar.MONTH)
        val year = current.get(Calendar.YEAR)

        return "$dayOfMonth/$bulan/$year"
    }

    private fun setButtonListener(){
        swipeRefreshLayout.setOnRefreshListener {
            refresh()
        }

        cardSambutan.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    requireActivity(), arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ),
                    1
                )
            } else {
                startActivity(Intent(requireContext(), PengaturanActivity::class.java))
            }
        }

        cardInfaq.setOnClickListener {
            startActivity(Intent(requireContext(), ListInfaqActivity::class.java))
        }

        iconAddInfaq.setOnClickListener {
            startActivity(Intent(requireContext(), AddInfaqActivity::class.java))
        }

        cardZakatFitrah.setOnClickListener {
            startActivity(Intent(requireContext(), ListZakatFitrahActivity::class.java))
        }

        iconAddZakatFitrah.setOnClickListener {
            startActivity(Intent(requireContext(), AddZakatFitrahActivity::class.java))
        }

        cardZakatMal.setOnClickListener {
            startActivity(Intent(requireContext(), ListZakatMalActivity::class.java))
        }

        iconAddZakatMal.setOnClickListener {
            startActivity(Intent(requireContext(), AddZakatMalActivity::class.java))
        }

        cardJamaah.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    requireActivity(), arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ),
                    1
                )
            } else {
                startActivity(Intent(requireContext(), ListJamaahActivity::class.java))
            }
        }

        iconAddJamaah.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                requestPermissions(
                    requireActivity(), arrayOf(
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ),
                    1
                )
            } else {
                startActivity(Intent(requireContext(), AddJamaahActivity::class.java))
            }
        }

        cardPengurus.setOnClickListener {
            startActivity(Intent(requireContext(), ListPengurusActivity::class.java))
        }

        iconAddPengurus.setOnClickListener {
            startActivity(Intent(requireContext(), AddPengurusActivity::class.java))
        }

        cardBerita.setOnClickListener {
            startActivity(Intent(requireContext(), ListBeritaActivity::class.java))
        }

        iconAddBerita.setOnClickListener {
            startActivity(Intent(requireContext(), AddBeritaActivity::class.java))
        }

        cardKegiatan.setOnClickListener {
            startActivity(Intent(requireContext(), ListKegiatanActivity::class.java))
        }

        iconAddKegiatan.setOnClickListener {
            startActivity(Intent(requireContext(), AddKegiatanActivity::class.java))
        }
    }

    fun refresh(){
        parentFragmentManager
            .beginTransaction()
            .detach(this)
            .attach(this)
            .commit()
    }
}