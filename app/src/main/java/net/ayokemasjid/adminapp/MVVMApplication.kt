package net.ayokemasjid.adminapp

import net.ayokemasjid.adminapp.data.database.AppDatabase
import android.app.Application
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.NetworkConnectionInterceptor
import net.ayokemasjid.adminapp.data.preferences.PreferenceProvider
import net.ayokemasjid.adminapp.data.repository.*
import net.ayokemasjid.adminapp.ui.auth.AuthViewModelFactory
import net.ayokemasjid.adminapp.ui.berita.BeritaViewModelFactory
import net.ayokemasjid.adminapp.ui.gallery.GalleryViewModelFactory
import net.ayokemasjid.adminapp.ui.home.HomeViewModelFactory
import net.ayokemasjid.adminapp.ui.infaq.InfaqViewModelFactory
import net.ayokemasjid.adminapp.ui.jamaah.JamaahViewModelFactory
import net.ayokemasjid.adminapp.ui.kegiatan.KegiatanViewModelFactory
import net.ayokemasjid.adminapp.ui.mustahik.MustahikViewModelFactory
import net.ayokemasjid.adminapp.ui.pengaturan.PengaturanViewModelFactory
import net.ayokemasjid.adminapp.ui.pengurus.PengurusViewModelFactory
import net.ayokemasjid.adminapp.ui.user.UserViewModelFactory
import net.ayokemasjid.adminapp.ui.zakatfitrah.ZakatFitrahViewModelFactory
import net.ayokemasjid.adminapp.ui.zakatmal.ZakatMalViewModelFactory
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.provider
import org.kodein.di.generic.singleton

class MVVMApplication : Application(), KodeinAware {

    override val kodein = Kodein.lazy {
        import(androidXModule(this@MVVMApplication))

        bind() from singleton { NetworkConnectionInterceptor(instance()) }
        bind() from singleton { MyApi(instance(),instance()) }
        bind() from singleton { AppDatabase(instance()) }
        bind() from singleton { PreferenceProvider(instance()) }
        bind() from singleton { MainRepository(instance(), instance()) }
        bind() from singleton { JamaahRepository(instance()) }
        bind() from singleton { ZakatFitrahRepository(instance()) }
        bind() from singleton { ZakatMalRepository(instance()) }
        bind() from singleton { InfaqRepository(instance()) }
        bind() from singleton { MustahikRepository(instance()) }
        bind() from singleton { BeritaRepository(instance()) }
        bind() from singleton { KegiatanRepository(instance()) }
        bind() from singleton { PengaturanRepository(instance()) }
        bind() from singleton { PengurusRepository(instance()) }
        bind() from singleton { GalleryRepository(instance()) }
        bind() from singleton { UserRepository(instance()) }

        bind() from provider { AuthViewModelFactory(instance()) }
        bind() from provider { JamaahViewModelFactory(instance()) }
        bind() from provider { HomeViewModelFactory(instance()) }
        bind() from provider { ZakatFitrahViewModelFactory(instance()) }
        bind() from provider { ZakatMalViewModelFactory(instance()) }
        bind() from provider { InfaqViewModelFactory(instance()) }
        bind() from provider { MustahikViewModelFactory(instance()) }
        bind() from provider { BeritaViewModelFactory(instance()) }
        bind() from provider { KegiatanViewModelFactory(instance()) }
        bind() from provider { PengaturanViewModelFactory(instance()) }
        bind() from provider { PengurusViewModelFactory(instance()) }
        bind() from provider { GalleryViewModelFactory(instance()) }
        bind() from provider { UserViewModelFactory(instance()) }
    }

}