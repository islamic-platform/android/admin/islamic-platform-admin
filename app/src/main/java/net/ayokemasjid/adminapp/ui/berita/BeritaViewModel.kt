package net.ayokemasjid.adminapp.ui.berita

import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.model.BeritaData
import net.ayokemasjid.adminapp.data.model.KategoriBerita
import net.ayokemasjid.adminapp.data.model.KategoriBeritaData
import net.ayokemasjid.adminapp.data.repository.BeritaRepository
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred
import okhttp3.MultipartBody
import okhttp3.RequestBody


class BeritaViewModel(
    private val repository: BeritaRepository
): ViewModel()   {

    var idBerita: Int? = null
    var title: String? = null
    var category: String? = null
    var konten: String? = null
    var filePart: RequestBody? = null
    var image_name: String? = null

    fun setJenis(data: String){
        category = data
    }

    fun setImage(data: RequestBody?, name: String?){
        filePart = data
        image_name = name
    }

    var beritaListener: MainListener? = null

    val beritaItems by lazyDeferred{
        repository.getAllBerita()
    }

    fun onAddButtonClick(){
        beritaListener?.onStarted()
        Coroutines.main {
            try {
                val titleBody = RequestBody.create(MultipartBody.FORM, title.toString())
                val categoryBody = RequestBody.create(MultipartBody.FORM, category.toString())
                val contentBody = RequestBody.create(MultipartBody.FORM, konten.toString())
                val file = MultipartBody.Part.createFormData("main_image", image_name, filePart!!)

                val beritaResponse = repository.addBerita(titleBody,categoryBody,contentBody,file)
                beritaListener?.getMessage(beritaResponse.message!!, beritaResponse.status)
            } catch (e: ApiException){
                beritaListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                beritaListener?.getMessage(e.message!!, false)
            }
        }
    }

    fun onEditButtonClick(){
        beritaListener?.onStarted()
        Coroutines.main{
            try {
                val data = BeritaData(idBerita!!, title!!, category!!, konten!!)
                val beritaResponse = repository.editBerita(data)
                beritaListener?.getMessage(beritaResponse.message!!, beritaResponse.status)
            } catch (e: ApiException){
                beritaListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                beritaListener?.getMessage(e.message!!, false)
            }
        }
    }

    fun onDeleteButton(idBerita: Int){
        beritaListener?.onStarted()
        Coroutines.main {
            try{
                val respon = repository.deleteBerita(idBerita)
                beritaListener?.getMessage(respon.message!!, false)
            } catch (e: ApiException){
                beritaListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                beritaListener?.getMessage(e.message!!, false)
            }
        }
    }

    var idKategori: Int? = null
    var namaKategori: String? = null

    val categoryItems by lazyDeferred{
        repository.getAllKategoriBerita()
    }

    fun onAddCategoryButtonClick(){
        beritaListener?.onStarted()
        Coroutines.main {
            try {
                val data = KategoriBeritaData(namaKategori!!)
                val response = repository.addKategoriBerita(data)
                beritaListener?.getMessage(response.message!!, response.status)
            } catch (e: ApiException){
                beritaListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                beritaListener?.getMessage(e.message!!, false)
            }
        }
    }

    fun onEditCategoryButtonClick(){
        beritaListener?.onStarted()
        Coroutines.main{
            try {
                val data = KategoriBerita(idKategori!!, namaKategori!!)
                val kategoriResponse = repository.editKategoriBerita(data)
                beritaListener?.getMessage(kategoriResponse.message!!, kategoriResponse.status)
            } catch (e: ApiException){
                beritaListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                beritaListener?.getMessage(e.message!!, false)
            }
        }
    }

    fun onDeleteCategoryButton(idKategori: Int){
        beritaListener?.onStarted()
        Coroutines.main {
            try{
                val respon = repository.deleteKategoriBerita(idKategori)
                beritaListener?.getMessage(respon.message!!, false)
            } catch (e: ApiException){
                beritaListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                beritaListener?.getMessage(e.message!!, false)
            }
        }
    }
}