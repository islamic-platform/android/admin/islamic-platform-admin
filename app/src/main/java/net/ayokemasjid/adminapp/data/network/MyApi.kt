package net.ayokemasjid.adminapp.data.network

import net.ayokemasjid.adminapp.data.model.*
import net.ayokemasjid.adminapp.data.network.response.*
import net.ayokemasjid.adminapp.data.preferences.PreferenceProvider
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit


interface MyApi {
    companion object{
        operator fun invoke(
            networkConnectionInterceptor: NetworkConnectionInterceptor, preferenceProvider: PreferenceProvider
        ) : MyApi{
            val url = preferenceProvider.getApiURL() ?: "http://staging.ayokemasjid.net/api/"

            val okkHttpClient = OkHttpClient.Builder()
                .addInterceptor(networkConnectionInterceptor)
                .connectTimeout(30, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build()

            return Retrofit.Builder()
                .client(okkHttpClient)
                .baseUrl(url)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(MyApi::class.java)
        }
    }

    @FormUrlEncoded
    @POST("login")
    suspend fun userLogin(
        @Field("email") email: String,
        @Field("password") password: String
    ): Response<AuthResponse>

    @GET("users")
    suspend fun getUser() : Response<AuthResponse>

    @PUT("users")
    suspend fun editUser(@Body user: User): Response<AuthResponse>

    @POST("jamaah")
    suspend fun addJamaah(@Body jamaahData: JamaahData): Response<JamaahResponse>

    @GET("jamaah")
    suspend fun getJamaah() : Response<JamaahResponse>

    @PUT("jamaah")
    suspend fun editJamaah(@Body jamaah: Jamaah): Response<JamaahResponse>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "jamaah", hasBody = true)
    suspend fun deleteJamaah(@Field("id_jamaah") id_jamaah: Int): Response<JamaahResponse>

    @POST("zakatfitrah")
    suspend fun addZakatFitrah(@Body zakatFitrahData: ZakatFitrahData): Response<ZakatFitrahResponse>

    @GET("zakatfitrah")
    suspend fun getZakatFitrah() : Response<ZakatFitrahResponse>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "zakatfitrah", hasBody = true)
    suspend fun deleteZakatFitrah(@Field("id_zakat_fitrah") id_zakat_fitrah: Int): Response<ZakatFitrahResponse>

    @POST("zakatmal")
    suspend fun addZakatMal(@Body zakatMalData: ZakatMalData): Response<ZakatMalResponse>

    @GET("zakatmal")
    suspend fun getZakatMal() : Response<ZakatMalResponse>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "zakatmal", hasBody = true)
    suspend fun deleteZakatMal(@Field("id_zakat_mal") id_zakat_mal: Int): Response<ZakatMalResponse>

    @POST("infaq")
    suspend fun addInfaq(@Body infaqData: InfaqData): Response<InfaqResponse>

    @GET("infaq")
    suspend fun getInfaq() : Response<InfaqResponse>

    @PUT("infaq")
    suspend fun editInfaq(@Body infaq: Infaq): Response<InfaqResponse>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "infaq", hasBody = true)
    suspend fun deleteInfaq(@Field("id_infaq") id_infaq: Int): Response<InfaqResponse>

    @POST("mustahik")
    suspend fun addMustahik(@Body mustahikData: MustahikData): Response<MustahikResponse>

    @GET("mustahik")
    suspend fun getMustahik() : Response<MustahikResponse>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "mustahik", hasBody = true)
    suspend fun deleteMustahik(@Field("id_mustahik") id_mustahik: Int): Response<MustahikResponse>

    @Multipart
    @POST("news")
    suspend fun addBerita(
        @Part("title") title: RequestBody,
        @Part("category") category: RequestBody,
        @Part("content") content: RequestBody,
        @Part main_image: MultipartBody.Part
    ): Response<BeritaResponse>

    @GET("news")
    suspend fun getBerita() : Response<BeritaResponse>

    @PUT("news")
    suspend fun editBerita(@Body beritaData: BeritaData): Response<BeritaResponse>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "news", hasBody = true)
    suspend fun deleteBerita(@Field("id_news") id_news: Int): Response<BeritaResponse>

    @POST("newscategory")
    suspend fun addKategoriBerita(@Body kategoriBeritaData: KategoriBeritaData): Response<KategoriBeritaResponse>

    @GET("newscategory")
    suspend fun getKategoriBerita() : Response<KategoriBeritaResponse>

    @PUT("newscategory")
    suspend fun editKategoriBerita(@Body kategoriBerita: KategoriBerita): Response<KategoriBeritaResponse>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "newscategory", hasBody = true)
    suspend fun deleteKategoriBerita(@Field("id_category") id_category: Int): Response<KategoriBeritaResponse>

    @Multipart
    @POST("task")
    suspend fun addKegiatan(
        @Part("title") title: RequestBody,
        @Part("people") people: RequestBody,
        @Part("information") information: RequestBody,
        @Part("date") date: RequestBody,
        @Part main_image: MultipartBody.Part
    ): Response<KegiatanResponse>

    @GET("task")
    suspend fun getKegiatan() : Response<KegiatanResponse>

    @PUT("task")
    suspend fun editKegiatan(@Body kegiatanData: KegiatanData): Response<KegiatanResponse>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "task", hasBody = true)
    suspend fun deleteKegiatan(@Field("id_task") id_news: Int): Response<KegiatanResponse>

    @GET("mosque")
    suspend fun getMasjid(): Response<PengaturanResponse>

    @PUT("mosque")
    suspend fun pengaturanMasjid(@Body pengaturanMasjidData: PengaturanMasjidData): Response<PengaturanResponse>

    @Multipart
    @POST("setting")
    suspend fun profileMasjid(
        @Part("id_mosque") id_mosque: RequestBody,
        @Part("name") name: RequestBody,
        @Part("motto") motto: RequestBody,
        @Part("width") width: RequestBody,
        @Part("volume") volume: RequestBody,
        @Part("about_short") about_short: RequestBody,
        @Part("about_long") about_long: RequestBody,
        @Part("greeting") greeting: RequestBody,
        @Part slider_pic: MultipartBody.Part,
        @Part greeting_pic: MultipartBody.Part,
        @Part logo: MultipartBody.Part
    ): Response<ProfileMasjidResponse>

    @POST("takmir")
    suspend fun addPengurus(@Body pengurusData: PengurusData): Response<PengurusResponse>

    @GET("takmir")
    suspend fun getPengurus() : Response<PengurusResponse>

    @PUT("takmir")
    suspend fun editPengurus(@Body pengurus: EditPengurus): Response<PengurusResponse>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "takmir", hasBody = true)
    suspend fun deletePengurus(@Field("id_takmir") id_takmir: Int): Response<PengurusResponse>

    @Multipart
    @POST("gallery")
    suspend fun addGallery(
        @Part("caption") caption: RequestBody,
        @Part file_name: MultipartBody.Part
    ): Response<GalleryResponse>

    @GET("gallery")
    suspend fun getGallery() : Response<GalleryResponse>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "gallery", hasBody = true)
    suspend fun deleteGallery(@Field("id_gallery") id_gallery: Int): Response<GalleryResponse>
}