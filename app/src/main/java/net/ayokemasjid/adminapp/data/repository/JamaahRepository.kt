package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.database.AppDatabase
import net.ayokemasjid.adminapp.data.model.Jamaah
import net.ayokemasjid.adminapp.data.model.JamaahData
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.JamaahResponse
import net.ayokemasjid.adminapp.util.Coroutines

class JamaahRepository(
    private val api: MyApi
): SafeApiRequest() {

    private val jamaahItems = MutableLiveData<List<Jamaah>>()

    suspend fun addJamaah(jamaahData: JamaahData): JamaahResponse {
        return apiRequest { api.addJamaah(jamaahData) }
    }

    suspend fun editJamaah(jamaah: Jamaah): JamaahResponse{
        return apiRequest { api.editJamaah(jamaah) }
    }

    suspend fun deleteJamaah(idJamaah : Int): JamaahResponse {
        return apiRequest { api.deleteJamaah(idJamaah) }
    }

    suspend fun getAllJamaah(): LiveData<List<Jamaah>> {
        fetchAllJamaah()
        return withContext(Dispatchers.IO) {
            jamaahItems
        }
    }

    private suspend fun fetchAllJamaah() {
        try {
            val response = apiRequest { api.getJamaah() }
            jamaahItems.postValue(response.data)
        } catch (e: Exception) {
            val empty: List<Jamaah> = ArrayList()
            jamaahItems.postValue(empty)
            e.printStackTrace()
        }
    }
}