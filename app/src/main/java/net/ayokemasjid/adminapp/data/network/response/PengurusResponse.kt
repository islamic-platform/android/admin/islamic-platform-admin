package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.Pengurus

data class PengurusResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<Pengurus>?
)