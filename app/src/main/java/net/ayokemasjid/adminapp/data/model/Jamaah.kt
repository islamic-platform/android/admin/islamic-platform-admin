package net.ayokemasjid.adminapp.data.model

import java.io.Serializable

data class Jamaah (
    var id_jamaah: Int? = null,
    var name: String? = null,
    var email: String? = null,
    var telephone: String? = null,
    var gender: String? = null,
    var address: String? = null,
    var city: String? = null,
    var province: String? = null,
    var country: String? = null,
    var postcode: String? = null,
    var information: String? = null,
    var lat: String? = null,
    var lon: String? = null,
    var date_insert: String? = null
) : Serializable