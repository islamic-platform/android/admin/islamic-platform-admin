package net.ayokemasjid.adminapp.data.model

import com.google.gson.annotations.SerializedName

//ga ke baca di MyApi, di simpen buat contoh

data class LoginData(
    @SerializedName("email")
    var email: String,
    @SerializedName("password")
    var password: String
)