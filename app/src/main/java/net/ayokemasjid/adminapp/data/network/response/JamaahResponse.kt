package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.Jamaah

data class JamaahResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<Jamaah>?
)