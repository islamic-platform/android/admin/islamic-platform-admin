package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.Berita

class BeritaResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<Berita>?
)