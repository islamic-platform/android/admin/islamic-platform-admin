package net.ayokemasjid.adminapp.ui.berita.kategori

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_list_kategori_berita.*
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.KategoriBerita
import net.ayokemasjid.adminapp.databinding.ActivityListKategoriBeritaBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.ui.berita.BeritaViewModel
import net.ayokemasjid.adminapp.ui.berita.BeritaViewModelFactory
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.snackbar
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.io.FileOutputStream
import java.util.*

class ListKategoriBeritaActivity : AppCompatActivity(), MainListener, KodeinAware {

    override val kodein by kodein()
    private val factory by instance<BeritaViewModelFactory>()

    private lateinit var binding: ActivityListKategoriBeritaBinding
    private lateinit var viewModel: BeritaViewModel

    private var isLayoutSaveVisible = false

    private var dataExcel: StringBuilder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_list_kategori_berita)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_kategori_berita)
        viewModel = ViewModelProvider(this, factory).get(BeritaViewModel::class.java)

        swipeRefreshLayout.isRefreshing = true
        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.beritaListener = this

        //recycle view
        bindUI()
        setListener()
    }

    //recycle view
    private fun bindUI() = Coroutines.main {
        //recycle view
        viewModel.categoryItems.await().observe(this, {
            //recycle view
            initRecyclerView(it.toKategoriItem())

            //add data to excel
            val currentDate = getCurrentDate()
            dataExcel = StringBuilder()
            dataExcel!!.append("Data Kategori Berita $currentDate")
            dataExcel!!.append("No,Nama Kategori Berita")
            if (!it.isNullOrEmpty()) {
                for ((x, data) in it.toKategoriItem().withIndex()) {
                    val number = x + 1
                    val judul = data.category.name!!

                    dataExcel!!.append(
                        "\n\"$number\",\"$judul\""
                    )
                }
            }
        })

        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun exportExcel(data: StringBuilder?){
        try {
            val currentDate = getCurrentDate()
            //saving the file into device
            val out: FileOutputStream = openFileOutput(
                "dataKategori[$currentDate].csv",
                Context.MODE_PRIVATE
            )
            out.write(data.toString().toByteArray())
            out.close()

            //exporting
            val context: Context = applicationContext
            val fileLocation = File(filesDir, "dataKategori[$currentDate].csv")
            val path: Uri = FileProvider.getUriForFile(
                context,
                "net.ayokemasjid.adminapp.fileprovider",
                fileLocation
            )
            val fileIntent = Intent(Intent.ACTION_SEND)
            val resInfoList =
                packageManager.queryIntentActivities(fileIntent, PackageManager.MATCH_DEFAULT_ONLY)
            for (resolveInfo in resInfoList) {
                val packageName = resolveInfo.activityInfo.packageName
                applicationContext.grantUriPermission(
                    packageName,
                    path,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
            }
            fileIntent.type = "text/csv"
            fileIntent.putExtra(Intent.EXTRA_SUBJECT, "dataKategori[$currentDate]")
            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            fileIntent.putExtra(Intent.EXTRA_STREAM, path)
            startActivity(Intent.createChooser(fileIntent, "Send via"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getCurrentDate(): String{
        val current = Calendar.getInstance()
        val dayOfMonth = current.get(Calendar.DAY_OF_MONTH)
        val bulan = current.get(Calendar.MONTH)
        val year = current.get(Calendar.YEAR)

        return "$year-$bulan-$dayOfMonth"
    }

    private fun setListener(){
        btnAdd.setOnClickListener {
            startActivity(Intent(this, AddKategoriBeritaActivity::class.java))
        }

        btnSaveAs.setOnClickListener {
            if(isLayoutSaveVisible){
                layout_btnSave.visibility = View.GONE
                isLayoutSaveVisible = false
            } else{
                layout_btnSave.visibility = View.VISIBLE
                isLayoutSaveVisible = true
            }
        }

        btnExcel.setOnClickListener {
            exportExcel(dataExcel)
        }

        swipeRefreshLayout.setOnRefreshListener {
            refresh(root_layout)
        }

    }

    //recycle view
    private fun List<KategoriBerita>.toKategoriItem() : List<KategoriBeritaItem>{
        return this.map {
            KategoriBeritaItem(it, viewModel, this@ListKategoriBeritaActivity)
        }
    }

    //recycle view
    private fun initRecyclerView(item: List<KategoriBeritaItem>){
        val mAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(item)
        }

        recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = mAdapter
        }
    }

    //button back di topbar
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = true
        }
    }

    override fun getMessage(message: String, status: Boolean?) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
        root_layout.snackbar(message)
    }

    fun refresh(view: View) {
        val intent: Intent = intent
        finish()
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    override fun onRestart() {
        super.onRestart()
        refresh(root_layout)
    }
}