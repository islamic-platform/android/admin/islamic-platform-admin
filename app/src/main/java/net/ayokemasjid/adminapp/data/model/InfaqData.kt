package net.ayokemasjid.adminapp.data.model

import com.google.gson.annotations.SerializedName

data class InfaqData (
    @SerializedName("category")
    var category: String,
    @SerializedName("total")
    var total: String,
    @SerializedName("info")
    var info: String,
    @SerializedName("date")
    var date: String
)