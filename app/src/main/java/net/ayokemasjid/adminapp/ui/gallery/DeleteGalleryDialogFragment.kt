package net.ayokemasjid.adminapp.ui.gallery

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_list_gallery.*
import kotlinx.android.synthetic.main.fragment_delete_gallery_dialog.*
import net.ayokemasjid.adminapp.databinding.FragmentDeleteGalleryDialogBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class DeleteGalleryDialogFragment : DialogFragment(), KodeinAware, MainListener {

    override val kodein by kodein()
    private lateinit var viewModel: GalleryViewModel
    private val factory by instance<GalleryViewModelFactory>()

    private var _binding: FragmentDeleteGalleryDialogBinding? = null
    private val binding get() = _binding!!

    private var listActivity: ListGalleryActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDeleteGalleryDialogBinding.inflate(inflater, container, false)

        viewModel = ViewModelProvider(this,factory).get(GalleryViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel.galleryListener = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        buttonBatal.setOnClickListener {
            dismiss()
        }

        buttonDelete.setOnClickListener {
            val idGallery = arguments?.getSerializable("ID") as Int
            viewModel.onDeleteButton(idGallery)
            listActivity!!.refresh(view)
            dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            val a: Activity
            if (context is Activity) {
                a = context
                listActivity = a as ListGalleryActivity
            }
        } catch (e: ClassCastException) {
            throw ClassCastException("$activity must implement FeedbackListener")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStarted() {
        if(listActivity!!.swipeRefreshLayout != null){
            listActivity!!.swipeRefreshLayout.isRefreshing = true
        }
    }

    override fun getMessage(message: String, status: Boolean?) {
        if(listActivity!!.swipeRefreshLayout != null){
            listActivity!!.swipeRefreshLayout.isRefreshing = false
        }
        listActivity!!.toast(message)
    }

}