package net.ayokemasjid.adminapp.ui.user

import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.model.User
import net.ayokemasjid.adminapp.data.repository.UserRepository
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred
import java.nio.charset.StandardCharsets
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import kotlin.experimental.and

class UserViewModel(
    private val repository: UserRepository
): ViewModel() {

    var userListener: UserListener? = null
    var user: User? = null
    /*var oldPassword: String? = null
    var newPassword: String? = null
    var confirmPassword: String? = null*/

    val userItems by lazyDeferred{
        repository.getAllUser()
    }

    fun onEditButtonClick(){
        userListener?.onStarted()
        Coroutines.main{
            try {
                val response = repository.editUser(user!!)
                userListener?.onSuccess(response.message!!,user)
            } catch (e: ApiException){
                userListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                userListener?.getMessage(e.message!!,false)
            }
        }
    }

    /*fun onChangePassword(){
        userListener?.onStarted()
        Coroutines.main{
            try {
                val authResponse = repository.userLogin(user!!.email!!,oldPassword!!)
                authResponse.data?.let {
                   if(it.isNullOrEmpty()){
                       userListener?.getMessage(authResponse.message!!,authResponse.status)
                   }else{
                       val new = it[0]
                       new.password = get_SHA_512_SecurePassword(newPassword,new.salt)
                       val response = repository.editUser(new)
                       userListener?.onSuccess(response.message!!,new)
                   }
                }
            } catch (e: ApiException){
                userListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                userListener?.getMessage(e.message!!,false)
            }
        }
    }

    private fun get_SHA_512_SecurePassword(passwordToHash: String?, salt: String?): String? {
        var generatedPassword: String? = null
        try {
            val md: MessageDigest = MessageDigest.getInstance("SHA-512")
            md.update(salt!!.toByteArray(StandardCharsets.UTF_8))
            val bytes: ByteArray = md.digest(passwordToHash!!.toByteArray(StandardCharsets.UTF_8))
            val sb = StringBuilder()
            for (i in bytes.indices) {
                sb.append(
                    ((bytes[i] and 0xff.toByte()) + 0x100).toString(16).substring(1)
                )
            }
            generatedPassword = sb.toString()
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        }
        return generatedPassword
    }*/
}