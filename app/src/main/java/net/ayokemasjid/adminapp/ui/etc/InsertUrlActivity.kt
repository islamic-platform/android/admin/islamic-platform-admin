package net.ayokemasjid.adminapp.ui.etc

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import kotlinx.android.synthetic.main.activity_insert_url.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.preferences.PreferenceProvider
import net.ayokemasjid.adminapp.databinding.ActivityInsertUrlBinding
import net.ayokemasjid.adminapp.ui.auth.AuthViewModel
import net.ayokemasjid.adminapp.ui.auth.AuthViewModelFactory
import net.ayokemasjid.adminapp.util.hide
import net.ayokemasjid.adminapp.util.show
import net.ayokemasjid.adminapp.util.snackbar
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class InsertUrlActivity : AppCompatActivity(), MainListener, KodeinAware {

    override val kodein by kodein()
    private val factory by instance<AuthViewModelFactory>()
    private val preference by instance<PreferenceProvider>()

    private lateinit var binding: ActivityInsertUrlBinding
    private lateinit var viewModel: AuthViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_insert_url)
        viewModel = ViewModelProvider(this, factory).get(AuthViewModel::class.java)

        buttonBantuanInsertUrl.setOnClickListener {
            BantuanDialogFragment().show(supportFragmentManager, "BantuanDialogFragment")
        }

        btnInsertUrl.setOnClickListener {
            onStarted()
            if(etInsertUrl.text.isNullOrEmpty()){
                getMessage("Isi URL dengan benar!", true)
                etInsertUrl.error = "Isi URL dengan benar!"
            } else{
                val url = "http://" + etInsertUrl.text.toString()
                if(Patterns.WEB_URL.matcher(url).matches()){
                    if(url == "http://staging.ayokemasjid.net"){
                        preference.setDomainURL(url)
                        viewModel.clearUser()
                        root_layout.snackbar("Berhasil")

                        lifecycleScope.launch{
                            delay(2000)
                            progress_bar.hide()
                            val intent = baseContext.packageManager
                                .getLaunchIntentForPackage(baseContext.packageName)
                            intent!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            startActivity(intent)
                        }
                    } else{
                        getMessage("URL belum terdaftar!", true)
                        infoGagalInsertUrl.visibility = View.VISIBLE
                    }
                } else{
                    getMessage("URL tidak valid!", true)
                }
            }
        }

        etInsertUrl.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (!binding.etInsertUrl.text.isNullOrEmpty()) {
                    binding.etInsertUrl.error = null
                    infoGagalInsertUrl.visibility = View.GONE
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    override fun onStarted() {
        hideSoftKeyboard(this)
        progress_bar.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        progress_bar.hide()
        root_layout.snackbar(message)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if(activity.currentFocus != null){
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }
}