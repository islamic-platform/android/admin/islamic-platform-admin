package net.ayokemasjid.adminapp.ui.user

import net.ayokemasjid.adminapp.data.model.User

interface UserListener {
    fun onStarted()
    fun onSuccess(message: String, user: User?)
    fun getMessage(message: String, status: Boolean?)
}