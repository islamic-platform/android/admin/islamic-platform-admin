package net.ayokemasjid.adminapp.ui.mustahik

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_list_mustahik.*
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Mustahik
import net.ayokemasjid.adminapp.databinding.ActivityListMustahikBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.snackbar
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.io.FileOutputStream
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

class ListMustahikActivity : AppCompatActivity(), MainListener, KodeinAware {

    override val kodein by kodein()
    private val factory by instance<MustahikViewModelFactory>()

    private lateinit var binding: ActivityListMustahikBinding
    private lateinit var viewModel: MustahikViewModel

    private var isLayoutSaveVisible = false

    private var dataExcel: StringBuilder? = null

    private var isDateFormatted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_list_mustahik)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_mustahik)
        viewModel = ViewModelProvider(this, factory).get(MustahikViewModel::class.java)

        swipeRefreshLayout.isRefreshing = true
        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.mustahikListener = this

        //recycle view
        bindUI()
        setListener()
    }

    private fun changeFormat(list: List<Mustahik>){
        for(mustahik in list){
            mustahik.date = getDate(mustahik.date!!)
            if(isDateFormatted){
                if(mustahik.category == "Uang"){
                    mustahik.total = getRupiah(mustahik.total!!)
                } else if(mustahik.category == "Beras"){
                    mustahik.total = mustahik.total + " Kg"
                }
            }
        }
    }

    private fun getDate(string: String): String{
        return try{
            val localeID = Locale("in", "ID")
            val date: Date? = SimpleDateFormat("yyyy-MM-dd", localeID).parse(string)
            val formatter = SimpleDateFormat("EEEE, dd MMMM yyyy", localeID)
            isDateFormatted = true
            formatter.format(date)
        } catch (e: Exception){
            isDateFormatted = false
            string
        }
    }

    private fun getRupiah(string: String): String{
        return try{
            val localeID = Locale("in", "ID")
            val otherSymbols = DecimalFormatSymbols(localeID)
            otherSymbols.decimalSeparator = ','
            otherSymbols.groupingSeparator = '.'
            val format = NumberFormat.getInstance() as DecimalFormat
            val symbol = Currency.getInstance(localeID).getSymbol(localeID)
            format.isGroupingUsed = true
            format.positivePrefix = "$symbol. "
            format.negativePrefix = "-$symbol. "
            format.minimumFractionDigits = 2
            format.maximumFractionDigits = 2
            format.decimalFormatSymbols = otherSymbols

            return format.format(string.toDouble())
        } catch (e: Exception){
            string
        }
    }

    //recycle view
    private fun bindUI() = Coroutines.main {
        //recycle view
        viewModel.mustahikItems.await().observe(this, Observer {
            var mutableList : MutableList<Mustahik> = ArrayList()
            mutableList = it.toMutableList()
            changeFormat(mutableList)
            //recycle view
            initRecyclerView(mutableList.toMustahikItem())

            //add data to excel
            val currentDate = getCurrentDate()
            dataExcel = StringBuilder()
            dataExcel!!.append("Data Mustahik $currentDate")
            dataExcel!!.append("No,Date,Name,Jenis Zakat,Jumlah Zakat,Information")
            if (!mutableList.isNullOrEmpty()) {
                for ((x, data) in mutableList.toMustahikItem().withIndex()) {
                    val number = x + 1
                    val date = data.mustahik.date!!
                    val name = data.mustahik.name!!
                    val jenis = data.mustahik.category!!
                    val total = data.mustahik.total!!
                    val information = data.mustahik.info!!

                    dataExcel!!.append(
                        "\n\"$number\",\"$date\",\"$name\",\"$jenis\",\"$total\",\"$information\""
                    )
                }
            }
        })

        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun exportExcel(data: StringBuilder?){
        try {
            val currentDate = getCurrentDate()
            //saving the file into device
            val out: FileOutputStream = openFileOutput(
                "dataMustahik[$currentDate].csv",
                Context.MODE_PRIVATE
            )
            out.write(data.toString().toByteArray())
            out.close()

            //exporting
            val context: Context = applicationContext
            val fileLocation = File(filesDir, "dataMustahik[$currentDate].csv")
            val path: Uri = FileProvider.getUriForFile(
                context,
                "net.ayokemasjid.adminapp.fileprovider",
                fileLocation
            )
            val fileIntent = Intent(Intent.ACTION_SEND)
            val resInfoList =
                packageManager.queryIntentActivities(fileIntent, PackageManager.MATCH_DEFAULT_ONLY)
            for (resolveInfo in resInfoList) {
                val packageName = resolveInfo.activityInfo.packageName
                applicationContext.grantUriPermission(
                    packageName,
                    path,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
            }
            fileIntent.type = "text/csv"
            fileIntent.putExtra(Intent.EXTRA_SUBJECT, "dataMustahik[$currentDate]")
            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            fileIntent.putExtra(Intent.EXTRA_STREAM, path)
            startActivity(Intent.createChooser(fileIntent, "Send via"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getCurrentDate(): String{
        val current = Calendar.getInstance()
        val dayOfMonth = current.get(Calendar.DAY_OF_MONTH)
        val bulan = current.get(Calendar.MONTH)
        val year = current.get(Calendar.YEAR)

        return "$year-$bulan-$dayOfMonth"
    }

    private fun setListener(){
        btnAdd.setOnClickListener {
            startActivity(Intent(this, AddMustahikActivity::class.java))
        }

        btnSaveAs.setOnClickListener {
            if(isLayoutSaveVisible){
                layout_btnSave.visibility = View.GONE
                isLayoutSaveVisible = false
            } else{
                layout_btnSave.visibility = View.VISIBLE
                isLayoutSaveVisible = true
            }
        }

        btnExcel.setOnClickListener {
            exportExcel(dataExcel)
        }

        swipeRefreshLayout.setOnRefreshListener {
            refresh(root_layout)
        }

    }

    //recycle view
    private fun List<Mustahik>.toMustahikItem() : List<MustahikItem>{
        return this.map {
            MustahikItem(it, viewModel, this@ListMustahikActivity)
        }
    }

    //recycle view
    private fun initRecyclerView(jamaahItem: List<MustahikItem>){
        val mAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(jamaahItem)
        }

        recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = mAdapter
        }
    }

    //button back di topbar
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = true
        }
    }

    override fun getMessage(message: String, status: Boolean?) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
        root_layout.snackbar(message)
    }

    fun refresh(view: View) {
        val intent: Intent = intent
        finish()
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    override fun onRestart() {
        super.onRestart()
        refresh(root_layout)
    }
}