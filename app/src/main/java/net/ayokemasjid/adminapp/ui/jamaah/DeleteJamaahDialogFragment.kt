package net.ayokemasjid.adminapp.ui.jamaah

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_list_jamaah.*
import kotlinx.android.synthetic.main.fragment_delete_jamaah_dialog.*
import kotlinx.android.synthetic.main.fragment_logout_dialog.buttonBatal
import net.ayokemasjid.adminapp.databinding.FragmentDeleteJamaahDialogBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.hide
import net.ayokemasjid.adminapp.util.show
import net.ayokemasjid.adminapp.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class DeleteJamaahDialogFragment : DialogFragment(), KodeinAware, MainListener {

    override val kodein by kodein()
    private lateinit var viewModel: JamaahViewModel
    private val factory by instance<JamaahViewModelFactory>()

    private var _binding: FragmentDeleteJamaahDialogBinding? = null
    private val binding get() = _binding!!

    private var listActivity: ListJamaahActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDeleteJamaahDialogBinding.inflate(inflater, container, false)

        viewModel = ViewModelProvider(this,factory).get(JamaahViewModel::class.java)
        binding.vmJamaah = viewModel
        viewModel.jamaahListener = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        buttonBatal.setOnClickListener {
            dismiss()
        }

        buttonDelete.setOnClickListener {
            val idJamaah = arguments?.getSerializable("ID") as Int
            viewModel.onDeleteButton(idJamaah)
            listActivity!!.refresh(view)
            dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            val a: Activity
            if (context is Activity) {
                a = context
                listActivity = a as ListJamaahActivity
            }
        } catch (e: ClassCastException) {
            throw ClassCastException("$activity must implement FeedbackListener")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStarted() {
        listActivity!!.progress_bar.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        listActivity!!.progress_bar.hide()
        listActivity!!.toast(message)
    }

}