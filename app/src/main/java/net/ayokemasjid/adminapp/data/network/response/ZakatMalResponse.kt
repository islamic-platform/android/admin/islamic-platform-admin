package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.ZakatMal

data class ZakatMalResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<ZakatMal>?
)