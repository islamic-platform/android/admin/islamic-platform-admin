package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.model.Gallery
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.GalleryResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody

data class GalleryRepository (
    private val api: MyApi
): SafeApiRequest() {

    private val galleryItems = MutableLiveData<List<Gallery>>()

    suspend fun addGallery(caption: RequestBody, file: MultipartBody.Part): GalleryResponse {
        return apiRequest { api.addGallery(caption, file) }
    }

    suspend fun deleteGallery(idGallery : Int): GalleryResponse {
        return apiRequest { api.deleteGallery(idGallery) }
    }

    suspend fun getAllGallery(): LiveData<List<Gallery>> {
        fetchAllGallery()
        return withContext(Dispatchers.IO) {
            galleryItems
        }
    }

    private suspend fun fetchAllGallery() {
        try {
            val response = apiRequest { api.getGallery() }
            galleryItems.postValue(response.data)
        } catch (e: Exception) {
            val empty: List<Gallery> = ArrayList()
            galleryItems.postValue(empty)
            e.printStackTrace()
        }
    }
}