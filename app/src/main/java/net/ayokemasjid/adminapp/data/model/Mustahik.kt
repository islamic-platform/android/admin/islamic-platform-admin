package net.ayokemasjid.adminapp.data.model

import java.io.Serializable

data class Mustahik (
    var id_mustahik: Int? = null,
    var date: String? = null,
    var name: String? = null,
    var category: String? = null,
    var total: String? = null,
    var info: String? = null
): Serializable