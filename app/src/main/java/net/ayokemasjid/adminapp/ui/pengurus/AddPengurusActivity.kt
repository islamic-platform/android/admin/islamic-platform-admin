package net.ayokemasjid.adminapp.ui.pengurus

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_add_pengurus.*
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Pengurus
import net.ayokemasjid.adminapp.databinding.ActivityAddPengurusBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.hide
import net.ayokemasjid.adminapp.util.show
import net.ayokemasjid.adminapp.util.snackbar
import net.ayokemasjid.adminapp.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class AddPengurusActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, KodeinAware,
    MainListener {

    override val kodein by kodein()
    private val factory by instance<PengurusViewModelFactory>()
    private lateinit var viewModel: PengurusViewModel

    private lateinit var binding: ActivityAddPengurusBinding

    private var isJenisSelected = false
    private var isAdded = false

    private var status : String = "tambah"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_pengurus)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_pengurus)
        viewModel = ViewModelProvider(this, factory).get(PengurusViewModel::class.java)
        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.pengurusListener= this

        status = intent.getSerializableExtra("status")  as? String ?: "tambah"

        setListener(binding)
        val spinner: Spinner = spinnerJenisPengurus
        setSpinner(spinner)
        setButtonListener()

        //status edit
        setFormStatus(binding)
    }

    private fun setButtonListener(){

        if(status == "edit"){
            buttonAddPengurus.setOnClickListener {
                if(isNoFormError(binding)){
                    viewModel.onEditButtonClick()
                } else{
                    root_layout.snackbar("Ada form yang kosong!")
                }
            }
            supportActionBar?.title = "Edit Pengurus"
            buttonAddPengurus.text = "Edit"
        } else if(status == "tambah"){
            buttonAddPengurus.setOnClickListener {
                if(isNoFormError(binding)){
                    viewModel.onAddButtonClick()
                } else{
                    root_layout.snackbar("Ada form yang kosong!")
                }
            }
            supportActionBar?.title = "Tambah Pengurus"
            buttonAddPengurus.text = "Tambah"
        }

        etNamaPengurus.setOnClickListener {
            if(!isJenisSelected){
                spinnerJenisPengurus.performClick()
            }
        }
    }

    //status edit
    private fun setFormStatus(binding: ActivityAddPengurusBinding){
        if(status == "edit"){
            val data = intent.getSerializableExtra("data") as Pengurus
            viewModel.name = data.name
            when (data.position) {
                "Anggota" -> {
                    binding.spinnerJenisPengurus.setSelection(4)
                }
                "Bendahara" -> {
                    binding.spinnerJenisPengurus.setSelection(3)
                }
                "Sekertaris" -> {
                    binding.spinnerJenisPengurus.setSelection(2)
                }
                else -> {
                    binding.spinnerJenisPengurus.setSelection(1)
                }
            }
            viewModel.position = data.position
            viewModel.idPengurus = data.id_takmir
        }
    }

    private fun setListener(binding: ActivityAddPengurusBinding){

        binding.etNamaPengurus.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etNamaPengurus.text.isNullOrEmpty()){
                    binding.etNamaPengurus.error = "Isi nama pengurus dengan benar!"
                    return
                } else{
                    binding.etNamaPengurus.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun isNoFormError(binding: ActivityAddPengurusBinding): Boolean{
        var status = true
        if(binding.viewmodel!!.name.isNullOrEmpty()) {
            binding.etNamaPengurus.error = "Isi nama pengurus dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.position.isNullOrEmpty()) {
            val view: TextView = spinnerJenisPengurus.selectedView as TextView
            view.error = "Pilih jabatan dengan benar!"
            if(status){
                status = false
            }
        }
        return status
    }

    private fun clearFormError(){
        isAdded = true
        etNamaPengurus.text.clear()
        etNamaPengurus.error = null
        spinnerJenisPengurus.setSelection(0)
    }

    private fun setSpinner(spinner: Spinner){
        ArrayAdapter.createFromResource(
            this,
            R.array.pengurus_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.setSelection(0, true)

            val tf = Typeface.createFromAsset(assets,"fonts/Roboto-Regular.ttf")
            val v = spinner.selectedView
            (v as TextView).setTextColor(Color.parseColor("#808080"))
            v.typeface = tf
            v.textSize = 14f
        }
        spinner.onItemSelectedListener = this
    }

    //Untuk Spinner spinnerJenisPengurus
    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    //Untuk Spinner spinnerJenisPengurus
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val text: String = parent?.getItemAtPosition(position).toString()
        if (parent != null) {
            (parent.getChildAt(0) as TextView).textSize = 14f
            (parent.getChildAt(0) as TextView).typeface =  Typeface.createFromAsset(assets,"fonts/Roboto-Regular.ttf")
            if(text == "Pilih jenis pengurus"){
                (parent.getChildAt(0) as TextView).setTextColor(Color.parseColor("#808080"))
                if(isAdded){
                    (parent.getChildAt(0) as TextView).error = null
                    isAdded = false
                }else{
                    (parent.getChildAt(0) as TextView).error = "Pilih jenis pengurus dengan benar!"
                }
            }else{
                (parent.getChildAt(0) as TextView).error = null
                (parent.getChildAt(0) as TextView).setTextColor(Color.BLACK)
                binding.viewmodel!!.setJenis(text)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if(activity.currentFocus != null){
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }

    override fun onStarted() {
        hideSoftKeyboard(this)
        progress_bar.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        progress_bar.hide()
        if(status!!){
            if(this.status == "tambah"){
                root_layout.snackbar(message)
                clearFormError()
            } else{
                Handler(Looper.getMainLooper()).postDelayed({
                    toast(message)
                    finish()
                }, 1000)
            }
        } else{
            root_layout.snackbar(message)
        }
    }
}