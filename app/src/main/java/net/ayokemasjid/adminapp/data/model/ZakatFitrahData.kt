package net.ayokemasjid.adminapp.data.model

import com.google.gson.annotations.SerializedName

data class ZakatFitrahData(
    @SerializedName("date")
    var date: String,
    @SerializedName("name")
    var name: String,
    @SerializedName("jiwa")
    var jiwa: String,
    @SerializedName("category")
    var category: String,
    @SerializedName("total")
    var total: String,
    @SerializedName("info")
    var info: String
)