package net.ayokemasjid.adminapp.ui.zakatfitrah

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.ayokemasjid.adminapp.data.repository.ZakatFitrahRepository

@Suppress("UNCHECKED_CAST")
class ZakatFitrahViewModelFactory(
    private val repository: ZakatFitrahRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ZakatFitrahViewModel(repository) as T
    }
}