package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.model.ZakatMal
import net.ayokemasjid.adminapp.data.model.ZakatMalData
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.ZakatMalResponse

class ZakatMalRepository(
    private val api: MyApi
): SafeApiRequest() {

    private val zakatMalItems = MutableLiveData<List<ZakatMal>>()

    suspend fun addZakatMal(zakatMalData: ZakatMalData): ZakatMalResponse {
        return apiRequest { api.addZakatMal(zakatMalData) }
    }

    suspend fun deleteZakatMal(idZakatMal : Int): ZakatMalResponse {
        return apiRequest { api.deleteZakatMal(idZakatMal) }
    }

    suspend fun getAllZakatMal(): LiveData<List<ZakatMal>> {
        fetchAllZakatMal()
        return withContext(Dispatchers.IO) {
            zakatMalItems
        }
    }

    private suspend fun fetchAllZakatMal() {
        try {
            val response = apiRequest { api.getZakatMal() }
            zakatMalItems.postValue(response.data)
        } catch (e: Exception) {
            val empty: List<ZakatMal> = ArrayList()
            zakatMalItems.postValue(empty)
            e.printStackTrace()
        }
    }
}