package net.ayokemasjid.adminapp.ui.auth
import net.ayokemasjid.adminapp.data.model.User

interface AuthListener {
    fun onStarted()
    fun onSuccess(user: User)
    fun onFailure(message: String)
}