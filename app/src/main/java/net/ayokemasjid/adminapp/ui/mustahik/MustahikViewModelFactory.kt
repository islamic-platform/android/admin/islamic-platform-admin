package net.ayokemasjid.adminapp.ui.mustahik

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.ayokemasjid.adminapp.data.repository.MustahikRepository

@Suppress("UNCHECKED_CAST")
class MustahikViewModelFactory(
    private val repository: MustahikRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MustahikViewModel(repository) as T
    }
}