package net.ayokemasjid.adminapp.ui.pengurus

import android.content.Intent
import android.os.Bundle
import com.xwray.groupie.databinding.BindableItem
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Pengurus
import net.ayokemasjid.adminapp.databinding.ItemPengurusBinding
import net.ayokemasjid.adminapp.ui.pengurus.DeletePengurusDialogFragment
import net.ayokemasjid.adminapp.ui.pengurus.ListPengurusActivity

class PengurusItem(
    val pengurus: Pengurus,
    private val viewModel: PengurusViewModel,
    val activity: ListPengurusActivity
): BindableItem<ItemPengurusBinding>() {

    override fun getLayout() = R.layout.item_pengurus

    override fun bind(viewBinding: ItemPengurusBinding, position: Int) {
        viewBinding.pengurus = pengurus
        viewBinding.viewmodel = viewModel

        viewBinding.btnEditPengurus.setOnClickListener {
            val intent = Intent(activity.baseContext, AddPengurusActivity::class.java)
            intent.putExtra("data", pengurus)
            intent.putExtra("status","edit")
            activity.startActivity(intent)
        }
        viewBinding.btnDeletePengurus.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("ID", pengurus.id_takmir!!)

            val fm = activity.supportFragmentManager
            val fragment = DeletePengurusDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Delete Dialog")
        }
    }
}