package net.ayokemasjid.adminapp.data.model

import com.google.gson.annotations.SerializedName

data class BeritaData (
    @SerializedName("id_news")
    var id_news: Int,
    @SerializedName("title")
    var title: String,
    @SerializedName("category")
    var category: String,
    @SerializedName("content")
    var content: String
)