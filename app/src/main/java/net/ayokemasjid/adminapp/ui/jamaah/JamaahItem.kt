package net.ayokemasjid.adminapp.ui.jamaah

import android.content.Intent
import android.os.Bundle
import com.xwray.groupie.databinding.BindableItem
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Jamaah
import net.ayokemasjid.adminapp.databinding.ItemJamaahBinding


class JamaahItem(val jamaah: Jamaah, private val viewModel: JamaahViewModel, val activity: ListJamaahActivity): BindableItem<ItemJamaahBinding>() {

    override fun getLayout() = R.layout.item_jamaah

    override fun bind(viewBinding: ItemJamaahBinding, position: Int) {
        viewBinding.jamaah = jamaah
        viewBinding.viewmodel = viewModel

        viewBinding.btnEditJamaah.setOnClickListener {
            val intent = Intent(activity.baseContext,AddJamaahActivity::class.java)
            intent.putExtra("data", jamaah)
            intent.putExtra("status","edit")
            activity.startActivity(intent)
        }
        viewBinding.btnDeleteJamaah.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("ID", jamaah.id_jamaah!!)

            val fm = activity.supportFragmentManager
            val deleteJamaahDialogFragment = DeleteJamaahDialogFragment()
            deleteJamaahDialogFragment.arguments = bundle
            deleteJamaahDialogFragment.show(fm,"Delete Dialog")
        }
    }

}