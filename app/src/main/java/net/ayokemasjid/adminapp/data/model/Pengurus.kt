package net.ayokemasjid.adminapp.data.model

import java.io.Serializable

data class Pengurus (
    var id_takmir: Int? = null,
    var name: String? = null,
    var position: String? = null,
    var date_insert: String? = null
): Serializable

data class EditPengurus (
    var id_takmir: Int? = null,
    var name: String? = null,
    var position: String? = null
): Serializable