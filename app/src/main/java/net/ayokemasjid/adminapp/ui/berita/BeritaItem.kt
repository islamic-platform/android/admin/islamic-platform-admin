package net.ayokemasjid.adminapp.ui.berita

import android.content.Intent
import android.os.Bundle
import com.xwray.groupie.databinding.BindableItem
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Berita
import net.ayokemasjid.adminapp.databinding.ItemBeritaBinding

class BeritaItem (
    val berita: Berita,
    private val viewModel: BeritaViewModel,
    val activity: ListBeritaActivity
): BindableItem<ItemBeritaBinding>() {

    override fun getLayout() = R.layout.item_berita

    override fun bind(viewBinding: ItemBeritaBinding, position: Int) {
        viewBinding.berita = berita
        viewBinding.viewmodel = viewModel

        viewBinding.btnDetailBerita.setOnClickListener {
            val intent = Intent(activity.baseContext, DetailBeritaActivity::class.java)
            intent.putExtra("data", berita)
            activity.startActivity(intent)
        }
        viewBinding.btnEditBerita.setOnClickListener {
            val intent = Intent(activity.baseContext, AddBeritaActivity::class.java)
            intent.putExtra("data", berita)
            intent.putExtra("status","edit")
            activity.startActivity(intent)
        }
        viewBinding.btnDeleteBerita.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("ID", berita.id_news!!)

            val fm = activity.supportFragmentManager
            val fragment = DeleteBeritaDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Delete Dialog")
        }
    }
}