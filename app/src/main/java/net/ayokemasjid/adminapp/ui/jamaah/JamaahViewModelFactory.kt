package net.ayokemasjid.adminapp.ui.jamaah

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.ayokemasjid.adminapp.data.repository.JamaahRepository

@Suppress("UNCHECKED_CAST")
class JamaahViewModelFactory(
    private val repository: JamaahRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return JamaahViewModel(repository) as T
    }
}