package net.ayokemasjid.adminapp.ui.zakatfitrah

import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.model.ZakatFitrahData
import net.ayokemasjid.adminapp.data.repository.ZakatFitrahRepository
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred

class ZakatFitrahViewModel(
    private val repository: ZakatFitrahRepository
): ViewModel()  {

    var date: String? = null
    var name: String? = null
    var jiwa: String? = null
    var category: String? = null
    var total: String? = null
    var info: String? = null

    var zakatFitrahListener: MainListener? = null

    val zakatFitrahItems by lazyDeferred{
        repository.getAllZakatFitrah()
    }

    fun setJenis(data: String){
        category = data
    }

    fun onAddButtonClick(){
        zakatFitrahListener?.onStarted()
        Coroutines.main {
            try {
                val data = ZakatFitrahData(date!!,name!!,jiwa!!,category!!,total!!,info!!)
                val zakatFitrahResponse = repository.addZakatFitrah(data)
                zakatFitrahListener?.getMessage(zakatFitrahResponse.message!!,zakatFitrahResponse.status)
            } catch (e: ApiException){
                zakatFitrahListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                zakatFitrahListener?.getMessage(e.message!!,false)
            }
        }
    }

    fun onDeleteButton(idZakatFitrah : Int){
        zakatFitrahListener?.onStarted()
        Coroutines.main {
            try{
                val respon = repository.deleteZakatFitrah(idZakatFitrah)
                zakatFitrahListener?.getMessage(respon.message!!,false)
            } catch (e: ApiException){
                zakatFitrahListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                zakatFitrahListener?.getMessage(e.message!!,false)
            }
        }
    }
}