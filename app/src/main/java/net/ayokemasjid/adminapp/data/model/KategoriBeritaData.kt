package net.ayokemasjid.adminapp.data.model

import com.google.gson.annotations.SerializedName

data class KategoriBeritaData (
    @SerializedName("name")
    var name: String
)