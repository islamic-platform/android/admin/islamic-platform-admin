package net.ayokemasjid.adminapp.data.database

import androidx.lifecycle.LiveData
import androidx.room.*
import net.ayokemasjid.adminapp.data.model.CURRENT_USER_ID
import net.ayokemasjid.adminapp.data.model.User

@Dao
interface UserDao{

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsert(user: User) : Long

    @Query("SELECT * FROM user WHERE uid = $CURRENT_USER_ID")
    fun getuser() : LiveData<User>

    @Delete
    suspend fun deleteUser(user: User)

    @Query("DELETE FROM user")
    suspend fun deleteAll() : Int

}