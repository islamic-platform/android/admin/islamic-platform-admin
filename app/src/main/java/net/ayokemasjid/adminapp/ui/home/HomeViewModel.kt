package net.ayokemasjid.adminapp.ui.home

import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.repository.MainRepository
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred

class HomeViewModel(private val repository: MainRepository): ViewModel() {

    private var homeListener: HomeListener? = null

    fun onLogoutButton(){
        Coroutines.main {
            try{
                repository.logout()
            } catch (e: ApiException){
                homeListener?.getMessage(e.message!!)
            } catch (e: NoInternetException){
                homeListener?.getMessage(e.message!!)
            }
        }
    }

    val totalInfaqMingguan by lazyDeferred{
        repository.getTotalInfaq()
    }

    val totalBerita by lazyDeferred{
        repository.getTotalBerita()
    }

    val totalJamaah by lazyDeferred{
        repository.getTotalJamaah()
    }

    val totalKegiatan by lazyDeferred{
        repository.getTotalKegiatan()
    }

    val totalZakatFitrah by lazyDeferred{
        repository.getTotalZakatFitrah()
    }

    val totalZakatMal by lazyDeferred{
        repository.getTotalZakatMal()
    }

    val getMasjid by lazyDeferred {
        repository.getAllMasjid()
    }

    val totalPengurus by lazyDeferred{
        repository.getTotalPengurus()
    }
}