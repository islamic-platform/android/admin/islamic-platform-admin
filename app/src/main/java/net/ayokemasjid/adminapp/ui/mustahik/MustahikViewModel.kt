package net.ayokemasjid.adminapp.ui.mustahik

import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.model.MustahikData
import net.ayokemasjid.adminapp.data.repository.MustahikRepository
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred

class MustahikViewModel(
    private val repository: MustahikRepository
): ViewModel()   {

    var date: String? = null
    var name: String? = null
    var category: String? = null
    var total: String? = null
    var info: String? = null

    var mustahikListener: MainListener? = null

    val mustahikItems by lazyDeferred{
        repository.getAllMustahik()
    }

    fun setJenis(data: String){
        category = data
    }

    fun onAddButtonClick(){
        mustahikListener?.onStarted()
        Coroutines.main {
            try {
                val data = MustahikData(date!!,name!!,category!!,total!!,info!!)
                val mustahikResponse = repository.addMustahik(data)
                mustahikListener?.getMessage(mustahikResponse.message!!,mustahikResponse.status)
            } catch (e: ApiException){
                mustahikListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                mustahikListener?.getMessage(e.message!!,false)
            }
        }
    }

    fun onDeleteButton(idMustahik : Int){
        mustahikListener?.onStarted()
        Coroutines.main {
            try{
                val respon = repository.deleteMustahik(idMustahik)
                mustahikListener?.getMessage(respon.message!!,false)
            } catch (e: ApiException){
                mustahikListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                mustahikListener?.getMessage(e.message!!,false)
            }
        }
    }
}