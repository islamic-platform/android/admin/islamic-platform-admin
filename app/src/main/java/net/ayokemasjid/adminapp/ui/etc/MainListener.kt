package net.ayokemasjid.adminapp.ui.etc

interface MainListener {
    fun onStarted()
    fun getMessage(message: String, status: Boolean?)
}