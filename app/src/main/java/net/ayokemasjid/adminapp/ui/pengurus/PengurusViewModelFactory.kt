package net.ayokemasjid.adminapp.ui.pengurus

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.ayokemasjid.adminapp.data.repository.PengurusRepository

@Suppress("UNCHECKED_CAST")
class PengurusViewModelFactory(
    private val repository: PengurusRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PengurusViewModel(repository) as T
    }
}