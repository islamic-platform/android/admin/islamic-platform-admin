package net.ayokemasjid.adminapp.ui.jamaah

import android.content.Intent
import android.view.View
import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.model.Jamaah
import net.ayokemasjid.adminapp.data.model.JamaahData
import net.ayokemasjid.adminapp.data.repository.JamaahRepository
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred

class JamaahViewModel(
    private val repository: JamaahRepository
): ViewModel() {

    var idJamaah: Int? = null
    var email: String? = null
    var name: String? = null
    var telephone: String? = null
    var gender: String? = null
    var address: String? = null
    var city: String? = null
    var province: String? = null
    var country: String? = null
    var postcode: String? = null
    var information: String? = null
    var lat: String? = null
    var lon: String? = null

    var jamaahListener: MainListener? = null

    val jamaahItems by lazyDeferred{
        repository.getAllJamaah()
    }

    fun onAddJamaah(view: View){
        Intent(view.context,AddJamaahActivity::class.java).also{
            view.context.startActivity(it)
        }
    }

    fun onViewJamaah(view: View){
        Intent(view.context,ListJamaahActivity::class.java).also{
            view.context.startActivity(it)
        }
    }

    fun isMale(view: View){
        gender = "Laki-laki"
    }

    fun isFemale(view: View){
        gender = "Perempuan"
    }

    fun onAddButtonClick() {
        jamaahListener?.onStarted()
        Coroutines.main {
            try {
                val data = JamaahData(email!!,name!!,telephone!!,gender!!,address!!,city!!,province!!,country!!,postcode!!,information!!,lat!!,lon!!)
                val jamaahResponse = repository.addJamaah(data)
                jamaahListener?.getMessage(jamaahResponse.message!!,jamaahResponse.status)
            } catch (e: ApiException){
                jamaahListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                jamaahListener?.getMessage(e.message!!,false)
            }
        }
    }

    fun onEditButtonClick(){
        jamaahListener?.onStarted()
        Coroutines.main{
            try {
                val data = Jamaah(
                    idJamaah!!,
                    name!!,
                    email!!,
                    telephone!!,
                    gender!!,
                    address!!,
                    city!!,
                    province!!,
                    country!!,
                    postcode!!,
                    information!!,
                    lat!!,
                    lon!!
                )
                val jamaahResponse = repository.editJamaah(data)
                jamaahListener?.getMessage(jamaahResponse.message!!,jamaahResponse.status)
            } catch (e: ApiException){
                jamaahListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                jamaahListener?.getMessage(e.message!!,false)
            }
        }
    }

    fun onDeleteButton(idJamaah : Int){
        jamaahListener?.onStarted()
        Coroutines.main {
            try{
                val respon = repository.deleteJamaah(idJamaah)
                jamaahListener?.getMessage(respon.message!!,respon.status)
            } catch (e: ApiException){
                jamaahListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                jamaahListener?.getMessage(e.message!!,false)
            }
        }
    }
}