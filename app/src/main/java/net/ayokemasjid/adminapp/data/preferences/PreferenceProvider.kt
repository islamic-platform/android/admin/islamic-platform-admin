package net.ayokemasjid.adminapp.data.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager

/*private const val KEY_SAVED_AT = "key_saved_at"*/
private const val DOMAIN_URL = "domain_url"
private const val API_URL = "api_url"
private const val IMAGE_URL = "image_url"

class PreferenceProvider(
    context: Context
) {

    private val appContext = context.applicationContext

    private val preference: SharedPreferences
        get() = PreferenceManager.getDefaultSharedPreferences(appContext)

    fun clear() {
        preference.edit().clear().apply()
    }

    /*"http://staging.ayokemasjid.net"*/
    fun setDomainURL(string: String?){
        preference.edit().putString(DOMAIN_URL,string).apply()
        setApiURL("$string/api/")
        setImageURL("$string/assets/uploads/")
    }

    fun getDomainURL(): String? {
        return preference.getString(DOMAIN_URL, null)
    }

    /*"http://staging.ayokemasjid.net/api/"*/
    private fun setApiURL(string: String?){
        preference.edit().putString(API_URL,string).apply()
    }

    fun getApiURL(): String? {
        return preference.getString(API_URL, null)
    }

    /*"http://staging.ayokemasjid.net/assets/uploads/"*/
    private fun setImageURL(string: String?){
        preference.edit().putString(IMAGE_URL,string).apply()
    }

    fun getImageURL(): String? {
        return preference.getString(IMAGE_URL, null)
    }

    /*fun savelastSavedAt(savedAt: String) {
        preference.edit().putString(
            KEY_SAVED_AT,
            savedAt
        ).apply()
    }

    fun getLastSavedAt(): String? {
        return preference.getString(KEY_SAVED_AT, null)
    }*/

}