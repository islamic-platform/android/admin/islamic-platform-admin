package net.ayokemasjid.adminapp.ui.user

import android.content.Intent
import com.xwray.groupie.databinding.BindableItem
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.User
import net.ayokemasjid.adminapp.databinding.ItemUserBinding

class UserItem(val user: User, private val viewModel: UserViewModel, val activity: ListUserActivity): BindableItem<ItemUserBinding>() {

    override fun getLayout() = R.layout.item_user

    override fun bind(viewBinding: ItemUserBinding, position: Int) {
        viewBinding.user = user
        viewBinding.viewmodel = viewModel

        viewBinding.btnEditUser.setOnClickListener {
            val intent = Intent(activity.baseContext, AddUserActivity::class.java)
            intent.putExtra("data", user)
            activity.startActivity(intent)
        }
    }

}