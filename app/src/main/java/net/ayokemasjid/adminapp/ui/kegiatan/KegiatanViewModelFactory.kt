package net.ayokemasjid.adminapp.ui.kegiatan

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.ayokemasjid.adminapp.data.repository.KegiatanRepository

@Suppress("UNCHECKED_CAST")
class KegiatanViewModelFactory(
    private val repository: KegiatanRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return KegiatanViewModel(repository) as T
    }
}