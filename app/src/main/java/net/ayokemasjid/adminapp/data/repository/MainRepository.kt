package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.database.AppDatabase
import net.ayokemasjid.adminapp.data.model.*
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.*
import net.ayokemasjid.adminapp.util.Coroutines
import okhttp3.MultipartBody
import okhttp3.RequestBody

class MainRepository(
    private val api: MyApi,
    private val db: AppDatabase
): SafeApiRequest() {

    private val beritaItems = MutableLiveData<Int>()
    private val infaqItems = MutableLiveData<Int>()
    private val jamaahItems = MutableLiveData<Int>()
    private val kegiatanItems = MutableLiveData<Int>()
    private val zakatFitrahItems = MutableLiveData<Int>()
    private val zakatMalItems = MutableLiveData<Int>()
    private val masjidItems = MutableLiveData<PengaturanMasjid>()
    private val pengurusItems = MutableLiveData<Int>()

    suspend fun userLogin(email: String, password: String): AuthResponse {
        return apiRequest { api.userLogin(email,password) }
    }

    suspend fun saveUser(user: User) = db.getUserDao().upsert(user)

    fun getUser() = db.getUserDao().getuser()

    fun logout(){
        Coroutines.io {
            db.getUserDao().deleteAll()
        }
    }

    suspend fun getTotalBerita(): LiveData<Int> {
        fetchAllBerita()
        return withContext(Dispatchers.IO) {
            beritaItems
        }
    }

    private suspend fun fetchAllBerita() {
        try {
            val response = apiRequest { api.getBerita() }
            var total = 0
            for(data in response.data!!){
                total += 1
            }
            beritaItems.postValue(total)
        } catch (e: Exception) {
            beritaItems.postValue(0)
            e.printStackTrace()
        }
    }

    suspend fun getTotalInfaq(): LiveData<Int> {
        fetchAllInfaq()
        return withContext(Dispatchers.IO) {
            infaqItems
        }
    }

    private suspend fun fetchAllInfaq() {
        try {
            val response = apiRequest { api.getInfaq() }
            var total = 0
            for(data in response.data!!){
                if(data.category == "Infaq Mingguan"){
                    if(data.total!!.toIntOrNull() != null){
                        total += data.total!!.toInt()
                    }
                }
            }
            infaqItems.postValue(total)
        } catch (e: Exception) {
            infaqItems.postValue(0)
            e.printStackTrace()
        }
    }

    suspend fun getTotalJamaah(): LiveData<Int> {
        fetchAllJamaah()
        return withContext(Dispatchers.IO) {
            jamaahItems
        }
    }

    private suspend fun fetchAllJamaah() {
        try {
            val response = apiRequest { api.getJamaah() }
            var total = 0
            for(data in response.data!!){
                total += 1
            }
            jamaahItems.postValue(total)
        } catch (e: Exception) {
            jamaahItems.postValue(0)
            e.printStackTrace()
        }
    }

    suspend fun getTotalKegiatan(): LiveData<Int> {
        fetchAllKegiatan()
        return withContext(Dispatchers.IO) {
            kegiatanItems
        }
    }

    private suspend fun fetchAllKegiatan() {
        try {
            val response = apiRequest { api.getKegiatan() }
            var total = 0
            for(data in response.data!!){
                total += 1
            }
            kegiatanItems.postValue(total)
        } catch (e: Exception) {
            kegiatanItems.postValue(0)
            e.printStackTrace()
        }
    }

    suspend fun getTotalZakatFitrah(): LiveData<Int> {
        fetchAllZakatFitrah()
        return withContext(Dispatchers.IO) {
            zakatFitrahItems
        }
    }

    private suspend fun fetchAllZakatFitrah() {
        try {
            val response = apiRequest { api.getZakatFitrah() }
            var total = 0
            for(data in response.data!!){
                if(data.category == "Uang"){
                    if(data.total!!.toIntOrNull() != null){
                        total += data.total!!.toInt()
                    }
                }
            }
            zakatFitrahItems.postValue(total)
        } catch (e: Exception) {
            zakatFitrahItems.postValue(0)
            e.printStackTrace()
        }
    }

    suspend fun getTotalZakatMal(): LiveData<Int> {
        fetchAllZakatMal()
        return withContext(Dispatchers.IO) {
            zakatMalItems
        }
    }

    private suspend fun fetchAllZakatMal() {
        try {
            val response = apiRequest { api.getZakatMal() }
            var total = 0
            for(data in response.data!!){
                if(data.total!!.toIntOrNull() != null){
                    total += data.total!!.toInt()
                }
            }
            zakatMalItems.postValue(total)
        } catch (e: Exception) {
            zakatMalItems.postValue(0)
            e.printStackTrace()
        }
    }

    suspend fun getAllMasjid(): LiveData<PengaturanMasjid> {
        fetchAllMasjid()
        return withContext(Dispatchers.IO) {
            masjidItems
        }
    }

    private suspend fun fetchAllMasjid() {
        try {
            val response = apiRequest { api.getMasjid() }
            masjidItems.postValue(response.data?.get(0))
        } catch (e: Exception) {
            val empty: PengaturanMasjid = PengaturanMasjid()
            masjidItems.postValue(empty)
            e.printStackTrace()
        }
    }

    suspend fun getTotalPengurus(): LiveData<Int> {
        fetchAllPengurus()
        return withContext(Dispatchers.IO) {
            pengurusItems
        }
    }

    private suspend fun fetchAllPengurus() {
        try {
            val response = apiRequest { api.getPengurus() }
            var total = 0
            for(data in response.data!!){
                total += 1
            }
            pengurusItems.postValue(total)
        } catch (e: Exception) {
            pengurusItems.postValue(0)
            e.printStackTrace()
        }
    }
}