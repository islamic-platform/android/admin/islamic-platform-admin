package net.ayokemasjid.adminapp.ui.gallery

import android.content.Intent
import android.os.Bundle
import com.squareup.picasso.Picasso
import com.xwray.groupie.databinding.BindableItem
import kotlinx.android.synthetic.main.activity_detail_kegiatan.*
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Gallery
import net.ayokemasjid.adminapp.data.preferences.PreferenceProvider
import net.ayokemasjid.adminapp.databinding.ItemGalleryBinding
import net.ayokemasjid.adminapp.ui.gallery.AddGalleryActivity
import net.ayokemasjid.adminapp.ui.gallery.DeleteGalleryDialogFragment
import net.ayokemasjid.adminapp.ui.gallery.ListGalleryActivity
import net.ayokemasjid.adminapp.ui.gallery.GalleryViewModel

class GalleryItem(
    val gallery: Gallery,
    private val viewModel: GalleryViewModel,
    private val preference: PreferenceProvider,
    val activity: ListGalleryActivity
): BindableItem<ItemGalleryBinding>() {

    override fun getLayout() = R.layout.item_gallery

    override fun bind(viewBinding: ItemGalleryBinding, position: Int) {
        viewBinding.gallery = gallery
        viewBinding.viewmodel = viewModel
        val url = preference.getImageURL() ?: "http://staging.ayokemasjid.net/assets/uploads/"
        Picasso.get()
            .load(url+gallery.file_name)
            .placeholder(R.drawable.progress_animation)
            .error(R.color.white)
            .into(viewBinding.ivMainImage)

        viewBinding.btnDeleteGallery.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("ID", gallery.id_gallery!!)

            val fm = activity.supportFragmentManager
            val fragment = DeleteGalleryDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Delete Dialog")
        }
    }
}