package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.Kegiatan

class KegiatanResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<Kegiatan>?
)