package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.model.ZakatFitrah
import net.ayokemasjid.adminapp.data.model.ZakatFitrahData
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.ZakatFitrahResponse

class ZakatFitrahRepository(
    private val api: MyApi
): SafeApiRequest() {

    private val zakatFitrahItems = MutableLiveData<List<ZakatFitrah>>()

    suspend fun addZakatFitrah(zakatFitrahData: ZakatFitrahData): ZakatFitrahResponse {
        return apiRequest { api.addZakatFitrah(zakatFitrahData) }
    }

    suspend fun deleteZakatFitrah(idZakatFitrah : Int): ZakatFitrahResponse {
        return apiRequest { api.deleteZakatFitrah(idZakatFitrah) }
    }

    suspend fun getAllZakatFitrah(): LiveData<List<ZakatFitrah>> {
        fetchAllZakatFitrah()
        return withContext(Dispatchers.IO) {
            zakatFitrahItems
        }
    }

    private suspend fun fetchAllZakatFitrah() {
        try {
            val response = apiRequest { api.getZakatFitrah() }
            zakatFitrahItems.postValue(response.data)
        } catch (e: Exception) {
            val empty: List<ZakatFitrah> = ArrayList()
            zakatFitrahItems.postValue(empty)
            e.printStackTrace()
        }
    }
}