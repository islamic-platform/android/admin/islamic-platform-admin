package net.ayokemasjid.adminapp.ui.jamaah

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.RadioGroup
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.geocoding.v5.MapboxGeocoding
import com.mapbox.api.geocoding.v5.models.CarmenFeature
import com.mapbox.api.geocoding.v5.models.GeocodingResponse
import com.mapbox.core.exceptions.ServicesException
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.*
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.places.autocomplete.PlaceAutocomplete
import com.mapbox.mapboxsdk.plugins.places.autocomplete.model.PlaceOptions
import com.mapbox.mapboxsdk.style.layers.Layer
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.*
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import kotlinx.android.synthetic.main.activity_add_jamaah.*
import kotlinx.android.synthetic.main.activity_add_jamaah.etNama
import kotlinx.android.synthetic.main.activity_add_jamaah.progress_bar
import kotlinx.android.synthetic.main.activity_add_jamaah.root_layout
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Jamaah
import net.ayokemasjid.adminapp.databinding.ActivityAddJamaahBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.hide
import net.ayokemasjid.adminapp.util.show
import net.ayokemasjid.adminapp.util.snackbar
import net.ayokemasjid.adminapp.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

//maps: OnMapReadyCallback, PermissionsListener, OnLocationClickListener, OnCameraTrackingChangedListener
class AddJamaahActivity : AppCompatActivity(), MainListener, KodeinAware, OnMapReadyCallback,
    PermissionsListener, OnLocationClickListener, OnCameraTrackingChangedListener {

    override val kodein by kodein()
    private val factory by instance<JamaahViewModelFactory>()

    private lateinit var binding: ActivityAddJamaahBinding
    private lateinit var viewModel: JamaahViewModel

    //maps
    private var isMapHidden = true
    private lateinit var permissionsManager: PermissionsManager
    private lateinit var mapboxMap: MapboxMap
    private var locationComponent: LocationComponent? = null
    private var isInTrackingMode = false
    private val REQUEST_CODE_AUTOCOMPLETE = 1
    private val geojsonSourceLayerId = "geojsonSourceLayerId"
    private val symbolIconId = "symbolIconId"
    private val DROPPED_MARKER_LAYER_ID = "DROPPED_MARKER_LAYER_ID"
    private var hoveringMarker: ImageView? = null
    private var droppedMarkerLayer: Layer? = null

    //ganti kegunaan screen
    private var status : String = "tambah"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //maps
        Mapbox.getInstance(this, getString(R.string.map_box_access_token))


        setContentView(R.layout.activity_add_jamaah)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_jamaah)
        viewModel = ViewModelProvider(this, factory).get(JamaahViewModel::class.java)

        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.jamaahListener = this

        status = intent.getSerializableExtra("status")  as? String ?: "tambah"

        setListener(binding)
        setButtonListener()

        //status edit
        setFormStatus(binding)

        //maps
        initMapView(savedInstanceState)
        initPermissions()
        syncMapbox()
    }

    private fun setButtonListener(){
        if(status == "edit"){
            buttonAddJamaah.setOnClickListener {
                if(isNoFormError(binding)){
                    viewModel.onEditButtonClick()
                } else{
                    root_layout.snackbar("Ada form yang kosong!")
                }
            }
            supportActionBar?.title = "Edit Jamaah"
            buttonAddJamaah.text = "Edit"
        } else if(status == "tambah"){
            buttonAddJamaah.setOnClickListener {
                if(isNoFormError(binding)){
                    viewModel.onAddButtonClick()
                } else{
                    root_layout.snackbar("Ada form yang kosong!")
                }
            }
            supportActionBar?.title = "Tambah Jamaah"
            buttonAddJamaah.text = "Tambah"
        }

        buttonShowMaps.setOnClickListener {
            showMaps()
        }
    }

    //status edit
    private fun setFormStatus(binding: ActivityAddJamaahBinding){
        if(status == "edit"){
            val data = intent.getSerializableExtra("data") as Jamaah
            viewModel.name = data.name
            viewModel.email = data.email
            viewModel.telephone = data.telephone
            if(data.gender == "Perempuan"){
                binding.genderInput.check(R.id.gender_wanita)
            } else{
                binding.genderInput.check(R.id.gender_pria)
            }
            viewModel.gender = data.gender
            viewModel.address = data.address
            viewModel.city = data.city
            viewModel.province = data.province
            viewModel.country = data.country
            viewModel.postcode = data.postcode

            //maps
            viewModel.lat = data.lat
            viewModel.lon = data.lon
            select_location_button.text = getString(R.string.location_picker_select_location_button_cancel)

            viewModel.information = data.information
            viewModel.idJamaah = data.id_jamaah
        }
    }

    //maps
    private fun showMaps(){
        if(isMapHidden){
            layout_maps.visibility = View.VISIBLE
            tvButtonShowMaps.text = "Tutup Maps"
            isMapHidden = false
        } else{
            layout_maps.visibility = View.GONE
            tvButtonShowMaps.text = "Tampilkan Maps"
            isMapHidden = true
        }
    }

    //back button topbar
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        hideSoftKeyboard(this)
        progress_bar.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        progress_bar.hide()
        if(status!!){
            if(this.status == "tambah"){
                root_layout.snackbar(message)
                clearFormError()
            } else{
                Handler(Looper.getMainLooper()).postDelayed({
                    toast(message)
                    finish()
                }, 1000)
            }
        } else{
            root_layout.snackbar(message)
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if(activity.currentFocus != null){
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }

    private fun setListener(binding: ActivityAddJamaahBinding){
        binding.etEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etEmail.text.isNullOrEmpty()){
                    binding.etEmail.error = "Isi email jamaah dengan benar!"
                    return
                } else{
                    val emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+".toRegex()
                    if(!binding.etEmail.text.matches(emailPattern)){
                        binding.etEmail.error = "Isi email jamaah yang valid!"
                    } else{
                        binding.etEmail.error = null
                    }
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etNama.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etNama.text.isNullOrEmpty()){
                    binding.etNama.error = "Isi nama jamaah dengan benar!"
                    return
                } else{
                    binding.etNama.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etTelephone.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etTelephone.text.isNullOrEmpty()){
                    binding.etTelephone.error = "Isi no. telepon jamaah dengan benar!"
                    return
                } else{
                    binding.etTelephone.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etAlamat.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etAlamat.text.isNullOrEmpty()){
                    binding.etAlamat.error = "Isi alamat jamaah dengan benar!"
                    return
                } else{
                    binding.etAlamat.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etKota.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etKota.text.isNullOrEmpty()){
                    binding.etKota.error = "Isi nama kota dengan benar!"
                    return
                } else{
                    binding.etKota.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etProvinsi.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etProvinsi.text.isNullOrEmpty()){
                    binding.etProvinsi.error = "Isi nama provinsi dengan benar!"
                    return
                } else{
                    binding.etProvinsi.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etNegara.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etNegara.text.isNullOrEmpty()){
                    binding.etNegara.error = "Isi nama negara dengan benar!"
                    return
                } else{
                    binding.etNegara.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etPostcode.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etPostcode.text.isNullOrEmpty()){
                    binding.etPostcode.error = "Isi kode pos dengan benar!"
                    return
                } else{
                    binding.etPostcode.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.genderInput.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup?, checkedId: Int) {
                if(checkedId != -1){
                    binding.genderWanita.error = null
                } else{
                    binding.genderWanita.error = "Pilih jenis kelamin dengan benar!"
                }
            }
        })
    }

    private fun isNoFormError(binding: ActivityAddJamaahBinding): Boolean{
        var status = true
        if(binding.viewmodel!!.name.isNullOrEmpty()) {
            binding.etNama.error = "Isi nama jamaah dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.email.isNullOrEmpty()) {
            binding.etEmail.error = "Isi email jamaah dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.telephone.isNullOrEmpty()) {
            binding.etTelephone.error = "Isi no. telepon jamaah dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.gender.isNullOrEmpty()) {
            binding.genderWanita.error = "Isi jenis kelamin dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.address.isNullOrEmpty()) {
            binding.etAlamat.error = "Isi alamat jamaah dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.city.isNullOrEmpty()) {
            binding.etKota.error = "Isi nama kota dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.province.isNullOrEmpty()) {
            binding.etProvinsi.error = "Isi nama provinsi dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.country.isNullOrEmpty()) {
            binding.etNegara.error = "Isi nama negara dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.postcode.isNullOrEmpty()) {
            binding.etPostcode.error = "Isi kode pos dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.information.isNullOrEmpty()) {
            binding.viewmodel!!.information = "-"
        }
        if(binding.viewmodel!!.lat.isNullOrEmpty()||binding.viewmodel!!.lon.isNullOrEmpty()) {
            binding.tvButtonShowMaps.error = "Pilih lokasi di maps dengan benar!"
            if(status){
                status = false
            }
        }
        return status
    }

    private fun clearFormError(){
        etNama.text.clear()
        etNama.error = null
        etEmail.text.clear()
        etEmail.error = null
        gender_input.clearCheck()
        gender_wanita.error = null
        etTelephone.text.clear()
        etTelephone.error = null
        etAlamat.text.clear()
        etAlamat.error = null
        etKota.text.clear()
        etKota.error = null
        etProvinsi.text.clear()
        etProvinsi.error = null
        etNegara.text.clear()
        etNegara.error = null
        etPostcode.text.clear()
        etPostcode.error = null
        etInformasi.text.clear()
        etInformasi.error = null
        tvLat.text = ""
        tvLong.text = ""
        tvButtonShowMaps.error = null
        isMapHidden = true
        layout_maps.visibility = View.GONE
    }

    //kebawah ini maps semua
    private fun initMapView(savedInstanceState: Bundle?) {
        map_view.onCreate(savedInstanceState)
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        mapboxMap.setStyle(Style.MAPBOX_STREETS){style ->
            initSearchFab()
            enableLocationComponent(style)
            style.addImage(symbolIconId, BitmapFactory.decodeResource(
                resources, R.drawable.red_marker
            ),false)

            // Create an empty GeoJSON source using the empty feature collection
            setUpSource(style)

            // Set up a new symbol layer for displaying the searched location's feature coordinates
            setupLayer(style)

            hoveringMarker = ImageView(this@AddJamaahActivity)
            hoveringMarker!!.setImageResource(R.drawable.red_marker)
            val params: FrameLayout.LayoutParams = FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER
            )
            hoveringMarker!!.layoutParams = params
            map_view.addView(hoveringMarker)
            initDroppedMarker(style)

            select_location_button.setOnClickListener {
                if (hoveringMarker!!.visibility == View.VISIBLE && select_location_button.text == "Pilih Lokasi") {

                    // Use the map target's coordinates to make a reverse geocoding search
                    val mapTargetLatLng = mapboxMap.cameraPosition.target

                    // Hide the hovering red hovering ImageView marker
                    hoveringMarker!!.visibility = View.INVISIBLE

                    // Transform the appearance of the button to become the cancel button
                    select_location_button.setBackgroundColor(
                        ContextCompat.getColor(this@AddJamaahActivity, R.color.colorAccent)
                    )
                    select_location_button.text = getString(R.string.location_picker_select_location_button_cancel)

                    // Show the SymbolLayer icon to represent the selected map location
                    if (style.getLayer(DROPPED_MARKER_LAYER_ID) != null) {
                        val source: GeoJsonSource? =
                            style.getSourceAs("dropped-marker-source-id")
                        source?.setGeoJson(
                            Point.fromLngLat(
                                mapTargetLatLng.longitude,
                                mapTargetLatLng.latitude
                            )
                        )
                        droppedMarkerLayer = style.getLayer(DROPPED_MARKER_LAYER_ID)
                        droppedMarkerLayer?.setProperties(visibility(Property.VISIBLE))
                    }

                    // Use the map camera target's coordinates to make a reverse geocoding search
                    reverseGeocode(
                        Point.fromLngLat(
                            mapTargetLatLng.longitude,
                            mapTargetLatLng.latitude
                        )
                    )
                } else {
                    binding.viewmodel!!.lat = null
                    binding.tvLat.text = ""
                    binding.viewmodel!!.lon = null
                    binding.tvLong.text = ""
                    binding.tvButtonShowMaps.error = "Pilih lokasi di maps!"
                    // Switch the button appearance back to select a location.
                    select_location_button.setBackgroundColor(
                        ContextCompat.getColor(
                            this@AddJamaahActivity,
                            R.color.colorPrimary
                        )
                    )
                    select_location_button.text = getString(R.string.location_picker_select_location_button_select)

                    // Show the red hovering ImageView marker
                    hoveringMarker!!.visibility = View.VISIBLE

                    // Hide the selected location SymbolLayer
                    droppedMarkerLayer = style.getLayer(DROPPED_MARKER_LAYER_ID)
                    droppedMarkerLayer?.setProperties(visibility(Property.NONE))
                }
            }
        }
        this.mapboxMap = mapboxMap
    }

    private fun initDroppedMarker(loadedMapStyle: Style) {
        // Add the marker image to map
        loadedMapStyle.addImage(
            "dropped-icon-image", BitmapFactory.decodeResource(
                resources, R.drawable.blue_marker
            )
        )
        loadedMapStyle.addSource(GeoJsonSource("dropped-marker-source-id"))
        loadedMapStyle.addLayer(
            SymbolLayer(
                DROPPED_MARKER_LAYER_ID,
                "dropped-marker-source-id"
            ).withProperties(
                iconImage("dropped-icon-image"),
                visibility(Property.NONE),
                iconAllowOverlap(true),
                iconIgnorePlacement(true)
            )
        )
    }

    private fun initSearchFab() {
        fab_location_search.setOnClickListener {
            val intent: Intent = PlaceAutocomplete.IntentBuilder()
                .accessToken(
                    if (Mapbox.getAccessToken() != null) Mapbox.getAccessToken().toString() else getString(
                        R.string.map_box_access_token
                    )
                )
                .placeOptions(
                    PlaceOptions.builder()
                        .backgroundColor(Color.parseColor("#EEEEEE"))
                        .limit(10)
                        .build(PlaceOptions.MODE_CARDS)
                )
                .build(this@AddJamaahActivity)
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE)
        }
    }

    private fun setUpSource(@NonNull loadedMapStyle: Style) {
        loadedMapStyle.addSource(GeoJsonSource(geojsonSourceLayerId))
    }

    private fun setupLayer(loadedMapStyle: Style) {
        loadedMapStyle.addLayer(
            SymbolLayer("SYMBOL_LAYER_ID", geojsonSourceLayerId).withProperties(
                PropertyFactory.iconOffset(arrayOf(0f, -8f))
            )
        )
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            // Retrieve selected location's CarmenFeature
            val selectedCarmenFeature = PlaceAutocomplete.getPlace(data)

            // Create a new FeatureCollection and add a new Feature to it using selectedCarmenFeature above.
            // Then retrieve and update the source designated for showing a selected location's symbol layer icon
            if (mapboxMap != null) {
                val style = mapboxMap.style
                if (style != null) {
                    val source =
                        style.getSourceAs<GeoJsonSource>(geojsonSourceLayerId)
                    source?.setGeoJson(
                        FeatureCollection.fromFeatures(
                            arrayOf(
                                Feature.fromJson(
                                    selectedCarmenFeature.toJson()
                                )
                            )
                        )
                    )

                    // Move map camera to the selected location
                    mapboxMap.animateCamera(
                        CameraUpdateFactory.newCameraPosition(
                            CameraPosition.Builder()
                                .target(
                                    LatLng(
                                        (selectedCarmenFeature.geometry() as Point?)!!.latitude(),
                                        (selectedCarmenFeature.geometry() as Point?)!!.longitude()
                                    )
                                )
                                .zoom(14.0)
                                .build()
                        ), 4000
                    )
                }
            }
        }
    }

    private fun initPermissions() {
        val permissionListener = object : PermissionsListener {
            override fun onExplanationNeeded(permissionsToExplain: MutableList<String>?) {
                /* Nothing to do in here */
            }

            override fun onPermissionResult(granted: Boolean) {
                if (granted) {
                    syncMapbox()
                } else {
                    val alertDialogInfo = AlertDialog.Builder(this@AddJamaahActivity)
                        .setTitle(getString(R.string.info))
                        .setCancelable(false)
                        .setMessage(getString(R.string.permissions_denied))
                        .setPositiveButton(getString(R.string.dismiss)) { _, _ ->
                            finish()
                        }
                        .create()
                    alertDialogInfo.show()
                }
            }
        }
        if (PermissionsManager.areLocationPermissionsGranted(this)) {
            syncMapbox()
        } else {
            permissionsManager = PermissionsManager(permissionListener)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    private fun syncMapbox() {
        map_view.getMapAsync(this)
    }

    @SuppressLint("MissingPermission")
    private fun enableLocationComponent(loadedMapStyle: Style) {
        // Check if permissions are enabled and if not request
        if (PermissionsManager.areLocationPermissionsGranted(this)) {

            // Create and customize the LocationComponent's options
            val customLocationComponentOptions = LocationComponentOptions.builder(this)
                .trackingGesturesManagement(true)
                .elevation(5F)
                .accuracyAlpha(.6f)
                .accuracyColor(ContextCompat.getColor(this, R.color.mapboxGreen))
                .foregroundDrawable(R.drawable.ic_location_on)
                .build()

            // Get an instance of the component
            locationComponent = mapboxMap.locationComponent

            val locationComponentActivationOptions =
                LocationComponentActivationOptions.builder(this, loadedMapStyle)
                    .locationComponentOptions(customLocationComponentOptions)
                    .build()

            // Activate with options
            locationComponent!!.activateLocationComponent(locationComponentActivationOptions)

            // Enable to make component visible
            locationComponent!!.isLocationComponentEnabled = true

            // Set the component's camera mode
            locationComponent!!.cameraMode = CameraMode.TRACKING

            // Set the component's render mode
            locationComponent!!.renderMode = RenderMode.COMPASS

            // Add the location icon click listener
            locationComponent!!.addOnLocationClickListener(this)

            // Add the camera tracking listener. Fires if the map camera is manually moved.
            locationComponent!!.addOnCameraTrackingChangedListener(this)

            back_to_camera_tracking_mode.setOnClickListener {
                if (!isInTrackingMode) {
                    isInTrackingMode = true
                    locationComponent!!.cameraMode = CameraMode.TRACKING
                    locationComponent!!.zoomWhileTracking(16.0)
                } else {
                    toast(getString(R.string.tracking_already_enabled))
                }
            }

        } else {
            permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(this)
        }
    }

    override fun onLocationComponentClick() {
        if (locationComponent!!.lastKnownLocation != null) {
            toast("You Are Here!")
        }
    }

    override fun onCameraTrackingDismissed() {
        isInTrackingMode = false
    }

    override fun onCameraTrackingChanged(currentMode: Int) {
        // Empty on purpose
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String>) {
        root_layout.snackbar(R.string.user_location_permission_explanation.toString())
    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            enableLocationComponent(mapboxMap.style!!)
        } else {
            root_layout.snackbar(R.string.user_location_permission_not_granted.toString())
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        map_view.onResume()
    }

    @SuppressWarnings("MissingPermission")
    override fun onStart() {
        super.onStart()
        map_view.onStart()
    }

    override fun onStop() {
        super.onStop()
        map_view.onStop()
    }

    override fun onPause() {
        super.onPause()
        map_view.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        map_view.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        map_view.onDestroy()
    }

    private fun reverseGeocode(point: Point) {
        try {
            val client: MapboxGeocoding = MapboxGeocoding.builder()
                .accessToken(getString(R.string.map_box_access_token))
                .query(Point.fromLngLat(point.longitude(), point.latitude()))
                .build()
            client.enqueueCall(object : Callback<GeocodingResponse?> {
                override fun onResponse(
                    call: Call<GeocodingResponse?>?,
                    response: Response<GeocodingResponse?>
                ) {
                    if (response.body() != null) {
                        val results: List<CarmenFeature> = response.body()!!.features()
                        if (results.isNotEmpty()) {
                            // If the geocoder returns a result, we take the first in the list and show a Toast with the place name.
                            mapboxMap.getStyle { style ->
                                if (style.getLayer(DROPPED_MARKER_LAYER_ID) != null) {
                                    val r = results[0].geometry() as Point
                                    binding.viewmodel!!.lat = r.latitude().toString()
                                    binding.tvLat.text = r.latitude().toString()
                                    binding.viewmodel!!.lon = r.longitude().toString()
                                    binding.tvLong.text = r.longitude().toString()
                                    binding.tvButtonShowMaps.error = null
                                }
                            }
                        } else {
                            root_layout.snackbar(getString(R.string.location_picker_dropped_marker_snippet_no_results))
                        }
                    }
                }

                override fun onFailure(
                    call: Call<GeocodingResponse?>?,
                    throwable: Throwable
                ) {
                    Timber.e("Geocoding Failure: %s", throwable.message)
                }
            })
        } catch (servicesException: ServicesException) {
            Timber.e("Error geocoding: %s", servicesException.toString())
            servicesException.printStackTrace()
        }
    }

}