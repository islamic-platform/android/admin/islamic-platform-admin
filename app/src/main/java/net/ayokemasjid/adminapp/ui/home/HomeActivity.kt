package net.ayokemasjid.adminapp.ui.home

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.text.SpannableString
import android.text.style.TextAppearanceSpan
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_home.toolbar
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.PengaturanMasjid
import net.ayokemasjid.adminapp.databinding.ActivityHomeBinding
import net.ayokemasjid.adminapp.ui.berita.ListBeritaActivity
import net.ayokemasjid.adminapp.ui.berita.kategori.ListKategoriBeritaActivity
import net.ayokemasjid.adminapp.ui.gallery.ListGalleryActivity
import net.ayokemasjid.adminapp.ui.infaq.ListInfaqActivity
import net.ayokemasjid.adminapp.ui.jamaah.ListJamaahActivity
import net.ayokemasjid.adminapp.ui.kegiatan.ListKegiatanActivity
import net.ayokemasjid.adminapp.ui.mustahik.ListMustahikActivity
import net.ayokemasjid.adminapp.ui.pengaturan.PengaturanActivity
import net.ayokemasjid.adminapp.ui.pengurus.ListPengurusActivity
import net.ayokemasjid.adminapp.ui.user.ListUserActivity
import net.ayokemasjid.adminapp.ui.zakatfitrah.ListZakatFitrahActivity
import net.ayokemasjid.adminapp.ui.zakatmal.ListZakatMalActivity
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class HomeActivity : AppCompatActivity(), KodeinAware,
    NavigationView.OnNavigationItemSelectedListener {
    private lateinit var binding: ActivityHomeBinding
    lateinit var viewModel: HomeViewModel

    override val kodein by kodein()
    private val factory by instance<HomeViewModelFactory>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        binding.viewmodel = viewModel

        setSupportActionBar(toolbar)
        initNavigation()
    }

    private fun initNavigation() {
        /*val navHost = supportFragmentManager.findFragmentById(R.id.container_home) as NavHostFragment
        navController = navHost.navController
        NavigationUI.setupWithNavController(navigation_view,navController)
        NavigationUI.setupActionBarWithNavController(this,navController,drawer_layout)*/

        val drawer: DrawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView: NavigationView = findViewById(R.id.navigation_view)
        val menu = navigationView.menu
        // find MenuItem you want to change
        /*val manajemenFragment = menu.findItem(R.id.manajemenFragment)
        val s = SpannableString(manajemenFragment.title)
        s.setSpan(TextAppearanceSpan(this, R.style.drawerTitle), 0, s.length, 0)
        manajemenFragment.title = s*/

        val menuLogout = menu.findItem(R.id.menuLogout)
        val s = SpannableString(menuLogout.title)
        s.setSpan(TextAppearanceSpan(this, R.style.drawerTitle), 0, s.length, 0)
        menuLogout.title = s

        navigationView.setNavigationItemSelectedListener(this)
        supportFragmentManager.beginTransaction()
        .replace(R.id.container_home, ManajemenFragment())
        .commit()

        navigationView.setCheckedItem(R.id.manajemenFragment)

        if (ContextCompat.checkSelfPermission(this@HomeActivity,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this@HomeActivity,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this@HomeActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
            } else {
                ActivityCompat.requestPermissions(this@HomeActivity,
                    arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
            }
        }
    }

    /*override fun onCreateOptionsMenu(menu: Menu?): Boolean{
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main_top, menu)
        return super.onCreateOptionsMenu(menu)
    }*/

    /*override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navController, drawer_layout)
    }*/

    override fun onBackPressed() {
        val drawer: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId
        /*return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)*/
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.

        when (item.itemId) {
            R.id.menuLaporan -> {
                supportFragmentManager.beginTransaction()
                    .replace(R.id.container_home, ManajemenFragment())
                    .commit()
            }
            R.id.menuJamaah -> {
                if (ContextCompat.checkSelfPermission(this@HomeActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this@HomeActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        ActivityCompat.requestPermissions(this@HomeActivity,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
                    } else {
                        ActivityCompat.requestPermissions(this@HomeActivity,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
                    }
                } else{
                    val i = Intent(this@HomeActivity, ListJamaahActivity::class.java)
                    startActivity(i)
                }
            }
            R.id.menuFitrah -> {
                val i = Intent(this@HomeActivity, ListZakatFitrahActivity::class.java)
                startActivity(i)
            }
            R.id.menuMal -> {
                val i = Intent(this@HomeActivity, ListZakatMalActivity::class.java)
                startActivity(i)
            }
            R.id.menuInfaq -> {
                val i = Intent(this@HomeActivity, ListInfaqActivity::class.java)
                startActivity(i)
            }
            R.id.menuMustahik -> {
                val i = Intent(this@HomeActivity, ListMustahikActivity::class.java)
                startActivity(i)
            }
            R.id.menuBerita -> {
                val i = Intent(this@HomeActivity, ListBeritaActivity::class.java)
                startActivity(i)
            }
            R.id.menuKategori -> {
                val i = Intent(this@HomeActivity, ListKategoriBeritaActivity::class.java)
                startActivity(i)
            }
            R.id.menuKegiatan -> {
                val i = Intent(this@HomeActivity, ListKegiatanActivity::class.java)
                startActivity(i)
            }
            R.id.menuPengaturanWeb -> {
                if (ContextCompat.checkSelfPermission(this@HomeActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this@HomeActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                        ActivityCompat.requestPermissions(this@HomeActivity,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
                    } else {
                        ActivityCompat.requestPermissions(this@HomeActivity,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), 1)
                    }
                } else{
                    val i = Intent(this@HomeActivity, PengaturanActivity::class.java)
                    startActivity(i)
                }
            }
            R.id.menuPengurusMasjid -> {
                val i = Intent(this@HomeActivity, ListPengurusActivity::class.java)
                startActivity(i)
            }
            R.id.menuGallery -> {
                val i = Intent(this@HomeActivity, ListGalleryActivity::class.java)
                startActivity(i)
            }
            R.id.menuProfile -> {
                val i = Intent(this@HomeActivity, ListUserActivity::class.java)
                startActivity(i)
            }
            R.id.menuLogout -> {
                LogoutDialogFragment().show(supportFragmentManager, "LogoutDialogFragment")
            }
        }

        val drawer: DrawerLayout = findViewById(R.id.drawer_layout)
        drawer.closeDrawer(GravityCompat.START)
        return true
    }

    fun refresh(view: View) {
        val intent: Intent = intent
        finish()
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    override fun onRestart() {
        super.onRestart()
        refresh(rootView)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>,
                                            grantResults: IntArray) {
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED) {
                    if ((ContextCompat.checkSelfPermission(this@HomeActivity,
                            Manifest.permission.ACCESS_FINE_LOCATION) ==
                                PackageManager.PERMISSION_GRANTED)) {
                        toast("Permission Granted")
                    }
                } else {
                    toast("Permission Denied")
                }
                return
            }
        }
    }
}