package net.ayokemasjid.adminapp.ui.pengaturan

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_pengaturan.*
import kotlinx.android.synthetic.main.fragment_profile_web.*
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.preferences.PreferenceProvider
import net.ayokemasjid.adminapp.databinding.ActivityPengaturanBinding
import net.ayokemasjid.adminapp.util.Coroutines
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance


class PengaturanActivity : AppCompatActivity(), KodeinAware {
    private lateinit var binding: ActivityPengaturanBinding
    private lateinit var viewModel: PengaturanViewModel

    override val kodein by kodein()
    private val factory by instance<PengaturanViewModelFactory>()
    private val preference by instance<PreferenceProvider>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pengaturan)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_pengaturan)

        viewModel = ViewModelProvider(this, factory).get(PengaturanViewModel::class.java)
        binding.viewmodel = viewModel

        setSupportActionBar(binding.toolbar)
        bottom_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        if (savedInstanceState == null) bottom_navigation.selectedItemId = R.id.profileMasjidFragment

        setData()
    }

    private fun setData() = Coroutines.main {
        viewModel.masjidItems.await().observe(this, Observer {
            viewModel.dataMasjid = it
            tvNamaMasjid.text = it.name
            val logoUrl = it.logo

            val url = preference.getImageURL() ?: "http://staging.ayokemasjid.net/assets/uploads/"
            Picasso.get()
                .load(url+logoUrl)
                .placeholder(R.drawable.progress_animation)
                .error(R.drawable.logo_placeholder)
                .into(logo)
        })
    }

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.profileMasjidFragment -> {
                    replaceFragment(ProfileWebFragment())
                    return@OnNavigationItemSelectedListener true
                }
                R.id.settingMasjidFragment -> {
                    replaceFragment(SettingWebFragment())
                    return@OnNavigationItemSelectedListener true
                }
            }
            false
        }

    private fun replaceFragment(fragment: Fragment) {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.container_pengaturan, fragment, fragment.javaClass.simpleName)
            .commit()
    }

    //button back di topbar
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    fun refresh() {
        val intent: Intent = intent
        finish()
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }
}