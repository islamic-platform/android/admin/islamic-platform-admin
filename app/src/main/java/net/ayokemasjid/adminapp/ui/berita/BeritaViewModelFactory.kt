package net.ayokemasjid.adminapp.ui.berita

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.ayokemasjid.adminapp.data.repository.BeritaRepository

@Suppress("UNCHECKED_CAST")
class BeritaViewModelFactory(
    private val repository: BeritaRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return BeritaViewModel(repository) as T
    }
}