package net.ayokemasjid.adminapp.ui.berita.kategori

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_list_berita.*
import kotlinx.android.synthetic.main.fragment_delete_kategori_berita_dialog.*
import net.ayokemasjid.adminapp.databinding.FragmentDeleteKategoriBeritaDialogBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.ui.berita.BeritaViewModel
import net.ayokemasjid.adminapp.ui.berita.BeritaViewModelFactory
import net.ayokemasjid.adminapp.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance

class DeleteKategoriBeritaDialogFragment : DialogFragment(), KodeinAware, MainListener {

    override val kodein by kodein()
    private lateinit var viewModel: BeritaViewModel
    private val factory by instance<BeritaViewModelFactory>()

    private var _binding: FragmentDeleteKategoriBeritaDialogBinding? = null
    private val binding get() = _binding!!

    private var listActivity: ListKategoriBeritaActivity? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDeleteKategoriBeritaDialogBinding.inflate(inflater, container, false)

        viewModel = ViewModelProvider(this,factory).get(BeritaViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel.beritaListener = this

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        buttonBatal.setOnClickListener {
            dismiss()
        }

        buttonDelete.setOnClickListener {
            val id = arguments?.getSerializable("ID") as Int
            viewModel.onDeleteCategoryButton(id)
            listActivity!!.refresh(view)
            dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            val a: Activity
            if (context is Activity) {
                a = context
                listActivity = a as ListKategoriBeritaActivity
            }
        } catch (e: ClassCastException) {
            throw ClassCastException("$activity must implement FeedbackListener")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStarted() {
        if(listActivity!!.swipeRefreshLayout != null){
            listActivity!!.swipeRefreshLayout.isRefreshing = true
        }
    }

    override fun getMessage(message: String, status: Boolean?) {
        if(listActivity!!.swipeRefreshLayout != null){
            listActivity!!.swipeRefreshLayout.isRefreshing = false
        }
        listActivity!!.toast(message)
    }

}