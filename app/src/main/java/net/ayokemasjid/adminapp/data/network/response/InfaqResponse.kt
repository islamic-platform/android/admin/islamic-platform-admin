package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.Infaq

class InfaqResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<Infaq>?
)