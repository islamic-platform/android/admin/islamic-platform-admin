package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.model.Kegiatan
import net.ayokemasjid.adminapp.data.model.KegiatanData
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.KegiatanResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody

data class KegiatanRepository (
    private val api: MyApi
): SafeApiRequest() {

    private val kegiatanItems = MutableLiveData<List<Kegiatan>>()

    suspend fun addKegiatan(title: RequestBody, people: RequestBody, information: RequestBody, date: RequestBody, file: MultipartBody.Part): KegiatanResponse {
        return apiRequest { api.addKegiatan(title, people, information, date, file) }
    }

    suspend fun editKegiatan(kegiatanData: KegiatanData): KegiatanResponse {
        return apiRequest { api.editKegiatan(kegiatanData) }
    }

    suspend fun deleteKegiatan(idKegiatan : Int): KegiatanResponse {
        return apiRequest { api.deleteKegiatan(idKegiatan) }
    }

    suspend fun getAllKegiatan(): LiveData<List<Kegiatan>> {
        fetchAllKegiatan()
        return withContext(Dispatchers.IO) {
            kegiatanItems
        }
    }

    private suspend fun fetchAllKegiatan() {
        try {
            val response = apiRequest { api.getKegiatan() }
            kegiatanItems.postValue(response.data)
        } catch (e: Exception) {
            val empty: List<Kegiatan> = ArrayList()
            kegiatanItems.postValue(empty)
            e.printStackTrace()
        }
    }
}