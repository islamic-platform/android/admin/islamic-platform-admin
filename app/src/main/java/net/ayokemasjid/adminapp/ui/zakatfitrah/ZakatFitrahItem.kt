package net.ayokemasjid.adminapp.ui.zakatfitrah

import android.os.Bundle
import com.xwray.groupie.databinding.BindableItem
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.ZakatFitrah
import net.ayokemasjid.adminapp.databinding.ItemZakatFitrahBinding

class ZakatFitrahItem(
    val zakatFitrah: ZakatFitrah,
    private val viewModel: ZakatFitrahViewModel,
    val activity: ListZakatFitrahActivity
): BindableItem<ItemZakatFitrahBinding>() {

    override fun getLayout() = R.layout.item_zakat_fitrah

    override fun bind(viewBinding: ItemZakatFitrahBinding, position: Int) {
        viewBinding.zakatfitrah = zakatFitrah
        viewBinding.viewmodel = viewModel

        viewBinding.btnDeleteZakatFitrah.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("ID", zakatFitrah.id_zakat_fitrah!!)

            val fm = activity.supportFragmentManager
            val fragment = DeleteZakatFitrahDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Delete Dialog")
        }
    }
}