package net.ayokemasjid.adminapp.ui.gallery

import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.repository.GalleryRepository
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred
import okhttp3.MultipartBody
import okhttp3.RequestBody

class GalleryViewModel(
    private val repository: GalleryRepository
): ViewModel()   {

    var idGallery: Int? = null
    var caption: String? = null
    var filePart: RequestBody? = null
    var file_name: String? = null

    fun setImage(data: RequestBody?, name: String?){
        filePart = data
        file_name = name
    }

    var galleryListener: MainListener? = null

    val galleryItems by lazyDeferred{
        repository.getAllGallery()
    }

    fun onAddButtonClick(){
        galleryListener?.onStarted()
        Coroutines.main {
            try {
                val captionBody = RequestBody.create(MultipartBody.FORM, caption.toString())
                val file = MultipartBody.Part.createFormData("file_name", file_name, filePart!!)

                val galleryResponse = repository.addGallery(captionBody,file)
                galleryListener?.getMessage(galleryResponse.message!!, galleryResponse.status)
            } catch (e: ApiException){
                galleryListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                galleryListener?.getMessage(e.message!!, false)
            }
        }
    }

    fun onDeleteButton(idGallery: Int){
        galleryListener?.onStarted()
        Coroutines.main {
            try{
                val respon = repository.deleteGallery(idGallery)
                galleryListener?.getMessage(respon.message!!, false)
            } catch (e: ApiException){
                galleryListener?.getMessage(e.message!!, false)
            } catch (e: NoInternetException){
                galleryListener?.getMessage(e.message!!, false)
            }
        }
    }
}