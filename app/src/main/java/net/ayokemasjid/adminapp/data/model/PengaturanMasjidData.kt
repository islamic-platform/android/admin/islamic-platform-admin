package net.ayokemasjid.adminapp.data.model

import com.google.gson.annotations.SerializedName
//PUT Mosque
data class PengaturanMasjidData (
    @SerializedName("id_mosque")
    var id_mosque: Int,
    @SerializedName("email")
    var email: String,
    @SerializedName("phone")
    var phone: String,
    @SerializedName("address")
    var address: String,
    @SerializedName("city")
    var city: String,
    @SerializedName("province")
    var province: String,
    @SerializedName("country")
    var country: String,
    @SerializedName("postcode")
    var postcode: String,
    @SerializedName("lat")
    var lat: String,
    @SerializedName("lon")
    var lon: String
)