package net.ayokemasjid.adminapp.data.model

import java.io.Serializable

data class ZakatFitrah (
    var id_zakat_fitrah: Int? = null,
    var date: String? = null,
    var name: String? = null,
    var jiwa: String? = null,
    var category: String? = null,
    var total: String? = null,
    var info: String? = null
): Serializable