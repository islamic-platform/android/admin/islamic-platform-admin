package net.ayokemasjid.adminapp.ui.berita

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import jp.wasabeef.richeditor.RichEditor
import kotlinx.android.synthetic.main.activity_add_berita.*
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Berita
import net.ayokemasjid.adminapp.databinding.ActivityAddBeritaBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.ui.UploadRequestBody
import net.ayokemasjid.adminapp.util.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream


class AddBeritaActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, KodeinAware,
    MainListener, UrlBeritaDialogFragment.OnInputListener, UploadRequestBody.UploadCallback {

    override val kodein by kodein()
    private val factory by instance<BeritaViewModelFactory>()
    private lateinit var viewModel: BeritaViewModel

    private lateinit var binding: ActivityAddBeritaBinding

    private var isAdded = false

    private var status : String = "tambah"

    private var mEditor: RichEditor? = null
    private var uri: Uri? = null

    private val mutableList: MutableList<String> = ArrayList()

    companion object {
        const val REQUEST_IMAGE_SELECTOR = 110
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_berita)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_berita)
        viewModel = ViewModelProvider(this, factory).get(BeritaViewModel::class.java)
        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.beritaListener = this

        status = intent.getSerializableExtra("status")  as? String ?: "tambah"

        setListener(binding)
        setDataSpinner()
        setButtonListener()
        setEditor()

        //status edit
        setFormStatus(binding)
    }

    private fun setButtonListener(){
        if(status == "edit"){
            buttonAddBerita.setOnClickListener {
                viewModel.konten = mEditor!!.html
                if(isNoFormError(binding)){
                    viewModel.onEditButtonClick()
                } else{
                    root_layout.snackbar("Ada form yang kosong!")
                }
            }
            supportActionBar?.title = "Edit Berita"
            buttonAddBerita.text = "Edit"
        } else if(status == "tambah"){
            buttonAddBerita.setOnClickListener {
                viewModel.konten = mEditor!!.html
                if(isNoFormError(binding)){
                    viewModel.onAddButtonClick()
                } else{
                    root_layout.snackbar("Ada form yang kosong!")
                }
            }
            supportActionBar?.title = "Tambah Berita"
            buttonAddBerita.text = "Tambah"
        }

        buttonPilihGambar.setOnClickListener {
            openGallery()
        }

        buttonRemoveImage.setOnClickListener {
            layout_preview.visibility = View.GONE

            val attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "")
            viewModel.setImage(attachmentEmpty, "")
        }
    }

    //status edit
    private fun setFormStatus(binding: ActivityAddBeritaBinding){
        if(status == "edit"){
            val data = intent.getSerializableExtra("data") as Berita
            viewModel.konten = data.content
            mEditor!!.html = data.content
            viewModel.title = data.title
            for(x in 0 until mutableList.size){
                if(mutableList[x] == viewModel.category){
                    binding.spinnerJenisBerita.setSelection(x)
                }
            }
            viewModel.category = data.category
            viewModel.idBerita = data.id_news

            tvGambarUtama.visibility = View.GONE
            tvInfoGambar.visibility = View.GONE
        }
    }

    private fun setListener(binding: ActivityAddBeritaBinding){
        binding.etTitle.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (binding.etTitle.text.isNullOrEmpty()) {
                    binding.etTitle.error = "Isi judul berita dengan benar!"
                    return
                } else {
                    binding.etTitle.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun isNoFormError(binding: ActivityAddBeritaBinding): Boolean{
        var status = true
        if(binding.viewmodel!!.title.isNullOrEmpty()) {
            binding.etTitle.error = "Isi judul berita dengan benar!"
            if(status){
                status = false
            }
        }
        binding.viewmodel!!.category = spinnerJenisBerita.selectedItem.toString()
        if(binding.viewmodel!!.konten.isNullOrEmpty()) {
            binding.viewmodel!!.konten = ""
        }
        if(binding.viewmodel!!.image_name.isNullOrEmpty()){
            val attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "")
            viewModel.setImage(attachmentEmpty, "")
        }
        return status
    }

    private fun clearFormError(){
        isAdded = true
        etTitle.text.clear()
        etTitle.error = null
        spinnerJenisBerita.setSelection(0)
        layout_preview.visibility = View.GONE
        val attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "")
        viewModel.setImage(attachmentEmpty, "")
        mEditor!!.html = ""

    }

    private fun setDataSpinner() =  Coroutines.main {
        viewModel.categoryItems.await().observe(this, {
            for (data in it) {
                mutableList.add(data.name!!)
            }
            val spinner: Spinner = spinnerJenisBerita
            setSpinner(spinner, mutableList)
        })
    }

    private fun setSpinner(spinner: Spinner, array: MutableList<String>){
        ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            array

        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.setSelection(0, true)

            val tf = Typeface.createFromAsset(assets, "fonts/Roboto-Regular.ttf")
            val v = spinner.selectedView
            (v as TextView).setTextColor(Color.BLACK)
            v.typeface = tf
            v.textSize = 14f
        }
        spinner.onItemSelectedListener = this
    }

    //Untuk Spinner spinnerKategoriBerita
    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    //Untuk Spinner spinnerKategoriBerita
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val text: String = parent?.getItemAtPosition(position).toString()
        if (parent != null) {
            (parent.getChildAt(0) as TextView).textSize = 14f
            (parent.getChildAt(0) as TextView).typeface =  Typeface.createFromAsset(
                assets,
                "fonts/Roboto-Regular.ttf"
            )
            (parent.getChildAt(0) as TextView).error = null
            (parent.getChildAt(0) as TextView).setTextColor(Color.BLACK)
            binding.viewmodel!!.setJenis(text)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    /*override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }*/

    private fun hideSoftKeyboard(activity: Activity) {
        if(activity.currentFocus != null){
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }

    override fun onStarted() {
        hideSoftKeyboard(this)
        progress_bar.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        progress_bar.hide()
        if(status!!){
            if(this.status == "tambah"){
                root_layout.snackbar(message)
                clearFormError()
            } else{
                Handler(Looper.getMainLooper()).postDelayed({
                    toast(message)
                    finish()
                }, 1000)
            }
        } else{
            root_layout.snackbar(message)
        }
    }

    private fun setEditor(){
        mEditor = editor
        mEditor!!.setEditorHeight(200)
        mEditor!!.setEditorFontSize(14)
        mEditor!!.setEditorFontColor(R.color.dark)
        //mEditor!!.setEditorBackgroundColor(Color.BLUE)
        mEditor!!.setBackgroundColor(resources.getColor(R.color.gray_background, this.theme))
        //mEditor!!.setBackgroundResource(R.drawable.bg_round_bottom)
        mEditor!!.setPadding(10, 10, 10, 0)
        //mEditor!!.setBackground("https://raw.githubusercontent.com/wasabeef/art/master/chip.jpg")
        mEditor!!.setPlaceholder("Tulis isi berita disini")
        //mEditor!!.setInputEnabled(false)

        action_insert_image.setOnClickListener {
            mEditor!!.focusEditor()

            val bundle = Bundle()
            bundle.putSerializable("type", "image")

            val fm = this.supportFragmentManager
            val fragment = UrlBeritaDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Add URL Dialog")
        }

        action_undo.setOnClickListener {
            mEditor!!.focusEditor()

            mEditor!!.undo()
        }

        action_redo.setOnClickListener {
            mEditor!!.focusEditor()

            mEditor!!.redo()
        }

        action_bold.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, motionEvent: MotionEvent?): Boolean {
                // show interest in events resulting from ACTION_DOWN
                if (motionEvent!!.action == MotionEvent.ACTION_DOWN) {
                    return true
                }
                // don't handle event unless its ACTION_UP so "doSomething()" only runs once.
                if (motionEvent.action != MotionEvent.ACTION_UP) {
                    return false
                }

                mEditor!!.focusEditor()
                val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

                mEditor!!.setBold()
                action_bold.isPressed = !action_bold.isPressed

                return true
            }
        })

        action_italic.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, motionEvent: MotionEvent?): Boolean {
                // show interest in events resulting from ACTION_DOWN
                if (motionEvent!!.action == MotionEvent.ACTION_DOWN) {
                    return true
                }
                // don't handle event unless its ACTION_UP so "doSomething()" only runs once.
                if (motionEvent.action != MotionEvent.ACTION_UP) {
                    return false
                }

                mEditor!!.focusEditor()
                val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

                mEditor!!.setItalic()
                action_italic.isPressed = !action_italic.isPressed
                return true
            }
        })

        action_strikethrough.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, motionEvent: MotionEvent?): Boolean {
                // show interest in events resulting from ACTION_DOWN
                if (motionEvent!!.action == MotionEvent.ACTION_DOWN) {
                    return true
                }
                // don't handle event unless its ACTION_UP so "doSomething()" only runs once.
                if (motionEvent.action != MotionEvent.ACTION_UP) {
                    return false
                }

                mEditor!!.focusEditor()
                val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

                mEditor!!.setStrikeThrough()
                action_strikethrough.isPressed = !action_strikethrough.isPressed

                return true
            }
        })

        action_underline.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View, motionEvent: MotionEvent?): Boolean {
                // show interest in events resulting from ACTION_DOWN
                if (motionEvent!!.action == MotionEvent.ACTION_DOWN) {
                    return true
                }
                // don't handle event unless its ACTION_UP so "doSomething()" only runs once.
                if (motionEvent.action != MotionEvent.ACTION_UP) {
                    return false
                }

                mEditor!!.focusEditor()
                val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

                mEditor!!.setUnderline()
                action_underline.isPressed = !action_underline.isPressed

                return true
            }
        })

        action_heading1.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setHeading(
                1
            )
        }

        action_heading2.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setHeading(
                2
            )
        }

        action_heading3.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setHeading(
                3
            )
        }

        action_heading4.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setHeading(
                4
            )
        }

        action_heading5.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setHeading(
                5
            )
        }

        action_heading6.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setHeading(
                6
            )
        }

        action_txt_color.setOnClickListener(object : View.OnClickListener {
            private var isChanged = false
            override fun onClick(v: View) {
                mEditor!!.focusEditor()
                val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

                mEditor!!.setTextColor(if (isChanged) Color.BLACK else Color.RED)
                isChanged = !isChanged
            }
        })

        action_indent.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setIndent()
        }

        action_outdent.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setOutdent()
        }

        action_align_left.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setAlignLeft()
        }

        action_align_center.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setAlignCenter()
        }

        action_align_right.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setAlignRight()
        }

        action_blockquote.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setBlockquote()
        }

        action_insert_bullets.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setBullets()
        }

        action_insert_numbers.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.setNumbers()
        }

        action_insert_youtube.setOnClickListener {
            mEditor!!.focusEditor()

            val bundle = Bundle()
            bundle.putSerializable("type", "youtube")

            val fm = this.supportFragmentManager
            val fragment = UrlBeritaDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Add URL Dialog")
        }

        action_insert_audio.setOnClickListener {
            mEditor!!.focusEditor()

            val bundle = Bundle()
            bundle.putSerializable("type", "audio")

            val fm = this.supportFragmentManager
            val fragment = UrlBeritaDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Add URL Dialog")
        }

        action_insert_video.setOnClickListener {
            mEditor!!.focusEditor()

            val bundle = Bundle()
            bundle.putSerializable("type", "video")

            val fm = this.supportFragmentManager
            val fragment = UrlBeritaDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Add URL Dialog")
        }

        action_insert_link.setOnClickListener {
            mEditor!!.focusEditor()

            val bundle = Bundle()
            bundle.putSerializable("type", "link")

            val fm = this.supportFragmentManager
            val fragment = UrlBeritaDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Add URL Dialog")
        }
        action_insert_checkbox.setOnClickListener {
            mEditor!!.focusEditor()
            val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(editor, InputMethodManager.SHOW_IMPLICIT)

            mEditor!!.insertTodo()
        }
    }


    @Override
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_SELECTOR) {
            if (resultCode == RESULT_OK && data != null && data.data != null) {
                uri = data.data

                val parcelFileDescriptor =
                    contentResolver.openFileDescriptor(uri!!, "r", null) ?: return

                val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
                val file = File(cacheDir, contentResolver.getFileName(uri!!))
                val outputStream = FileOutputStream(file)
                inputStream.copyTo(outputStream)

                val body = UploadRequestBody(file, "main_image", this)

                preview_image.setImageURI(uri)
                layout_preview.visibility = View.VISIBLE
                viewModel.setImage(body, file.name)
            }
        }
    }

    @Override
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun openGallery() {
        try {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    REQUEST_IMAGE_SELECTOR
                )
            } else {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(intent, REQUEST_IMAGE_SELECTOR)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun sendInput(url: String, title: String, width: Int, height: Int, type: String) {
        when (type) {
            "image" -> {
                mEditor!!.insertImage(url, title, width, height)
            }
            "youtube" -> {
                mEditor!!.insertYoutubeVideo(url, width, height)
            }
            "video" -> {
                mEditor!!.insertVideo(url, width, height)
            }
            "audio" -> {
                mEditor!!.insertAudio(url)
            }
            "link" -> {
                mEditor!!.insertLink(url, title)
            }
        }
    }

    override fun onProgressUpdate(percentage: Int) {

    }
}