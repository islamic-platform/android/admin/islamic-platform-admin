package net.ayokemasjid.adminapp.ui.mustahik

import android.os.Bundle
import com.xwray.groupie.databinding.BindableItem
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Mustahik
import net.ayokemasjid.adminapp.databinding.ItemMustahikBinding

class MustahikItem(
    val mustahik: Mustahik,
    private val viewModel: MustahikViewModel,
    val activity: ListMustahikActivity
): BindableItem<ItemMustahikBinding>() {

    override fun getLayout() = R.layout.item_mustahik

    override fun bind(viewBinding: ItemMustahikBinding, position: Int) {
        viewBinding.mustahik = mustahik
        viewBinding.viewmodel = viewModel

        viewBinding.btnDeleteMustahik.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("ID", mustahik.id_mustahik!!)

            val fm = activity.supportFragmentManager
            val fragment = DeleteMustahikDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Delete Dialog")
        }
    }
}