package net.ayokemasjid.adminapp.ui.berita.kategori

import android.app.Activity
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_add_kategori_berita.*
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.KategoriBerita
import net.ayokemasjid.adminapp.databinding.ActivityAddKategoriBeritaBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.ui.berita.BeritaViewModel
import net.ayokemasjid.adminapp.ui.berita.BeritaViewModelFactory
import net.ayokemasjid.adminapp.util.hide
import net.ayokemasjid.adminapp.util.show
import net.ayokemasjid.adminapp.util.snackbar
import net.ayokemasjid.adminapp.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance

class AddKategoriBeritaActivity : AppCompatActivity(), KodeinAware,
    MainListener {

    override val kodein by kodein()
    private val factory by instance<BeritaViewModelFactory>()
    private lateinit var viewModel: BeritaViewModel

    private lateinit var binding: ActivityAddKategoriBeritaBinding
    private var status : String = "tambah"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_kategori_berita)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_kategori_berita)
        viewModel = ViewModelProvider(this, factory).get(BeritaViewModel::class.java)
        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.beritaListener = this

        status = intent.getSerializableExtra("status")  as? String ?: "tambah"

        setListener(binding)
        setButtonListener()

        //status edit
        setFormStatus()
    }

    private fun setButtonListener(){
        if(status == "edit"){
            buttonAddKategori.setOnClickListener {
                if(isNoFormError(binding)){
                    viewModel.onEditCategoryButtonClick()
                } else{
                    root_layout.snackbar("Ada form yang kosong!")
                }
            }
            supportActionBar?.title = "Edit Kategori Berita"
            buttonAddKategori.text = "Edit"
        } else if(status == "tambah"){
            buttonAddKategori.setOnClickListener {
                if(isNoFormError(binding)){
                    viewModel.onAddCategoryButtonClick()
                } else{
                    root_layout.snackbar("Ada form yang kosong!")
                }
            }
            supportActionBar?.title = "Tambah Kategori Berita"
            buttonAddKategori.text = "Tambah"
        }
    }

    //status edit
    private fun setFormStatus(){
        if(status == "edit"){
            val data = intent.getSerializableExtra("data") as KategoriBerita
            viewModel.namaKategori = data.name
            viewModel.idKategori = data.id_category
        }
    }

    private fun setListener(binding: ActivityAddKategoriBeritaBinding){
        binding.etNama.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (binding.etNama.text.isNullOrEmpty()) {
                    binding.etNama.error = "Isi nama dengan benar!"
                    return
                } else {
                    binding.etNama.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun isNoFormError(binding: ActivityAddKategoriBeritaBinding): Boolean{
        var status = true
        if(binding.viewmodel!!.namaKategori.isNullOrEmpty()) {
            binding.etNama.error = "Isi nama dengan benar!"
            if(status){
                status = false
            }
        }
        return status
    }

    private fun clearFormError(){
        etNama.text.clear()
        etNama.error = null
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if(activity.currentFocus != null){
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }

    override fun onStarted() {
        hideSoftKeyboard(this)
        progress_bar.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        progress_bar.hide()
        if(status!!){
            if(this.status == "tambah"){
                root_layout.snackbar(message)
                clearFormError()
            } else{
                Handler(Looper.getMainLooper()).postDelayed({
                    toast(message)
                    finish()
                }, 1000)
            }
        } else{
            root_layout.snackbar(message)
        }
    }
}