package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.Mustahik

data class MustahikResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<Mustahik>?
)