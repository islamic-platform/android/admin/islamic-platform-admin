package net.ayokemasjid.adminapp.data.model

import java.io.Serializable

data class Berita (
    var id_news: Int? = null,
    var title: String? = null,
    var category: String? = null,
    var content: String? = null,
    var date_insert: String? = null,
    var main_image: String? = null
): Serializable