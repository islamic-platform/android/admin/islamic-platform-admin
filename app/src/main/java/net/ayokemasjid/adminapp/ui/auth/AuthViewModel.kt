package net.ayokemasjid.adminapp.ui.auth

import android.view.View
import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.repository.MainRepository
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException

class AuthViewModel(
    private val repository: MainRepository
): ViewModel() {
    var email: String? = null
    var password: String? = null

    var authListener: AuthListener? = null

    fun getLoggedInUser() = repository.getUser()

    fun onLoginButtonClick(view : View) {
        authListener?.onStarted()
        if(email.isNullOrEmpty() || password.isNullOrEmpty()) {
            authListener?.onFailure("Invalid email or password")
            return
        }

        Coroutines.main {
            try {
                //val data = LoginData(email!!,password!!)
                val authResponse = repository.userLogin(email!!,password!!)
                authResponse.data?.let {
                    authListener?.onSuccess(it[0])
                    repository.saveUser(it[0])
                    return@main
                }
                authListener?.onFailure(authResponse.message!!)
            } catch (e: ApiException){
                authListener?.onFailure(e.message!!)
            } catch (e: NoInternetException){
                authListener?.onFailure(e.message!!)
            }
        }
    }

    fun clearUser(){
        Coroutines.main {
            try{
                repository.logout()
            } catch (e: ApiException){
                authListener?.onFailure(e.message!!)
            } catch (e: NoInternetException){
                authListener?.onFailure(e.message!!)
            }
        }
    }

    //private fun get_SHA_512_SecurePassword(passwordToHash: String?, salt: String?): String? {
    //    var generatedPassword: String? = null
    //    try {
    //        val md: MessageDigest = MessageDigest.getInstance("SHA-512")
    //        md.update(salt!!.toByteArray(StandardCharsets.UTF_8))
    //        val bytes: ByteArray = md.digest(passwordToHash!!.toByteArray(StandardCharsets.UTF_8))
    //        val sb = StringBuilder()
    //        for (i in bytes.indices) {
    //            sb.append(
    //                Integer.toString((bytes[i] and 0xff.toByte()) + 0x100, 16).substring(1)
    //            )
    //        }
    //        generatedPassword = sb.toString()
    //    } catch (e: NoSuchAlgorithmException) {
    //        e.printStackTrace()
    //    }
    //    return generatedPassword
    //}
}