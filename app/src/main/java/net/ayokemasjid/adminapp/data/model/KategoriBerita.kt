package net.ayokemasjid.adminapp.data.model

import java.io.Serializable

data class KategoriBerita (
    var id_category: Int? = null,
    var name: String? = null
): Serializable