package net.ayokemasjid.adminapp.data.model

import java.io.Serializable

data class Gallery (
    var id_gallery: Int? = null,
    var file_name: String? = null,
    var caption: String? = null
): Serializable