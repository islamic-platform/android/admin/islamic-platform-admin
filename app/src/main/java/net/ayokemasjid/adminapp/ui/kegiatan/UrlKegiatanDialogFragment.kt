package net.ayokemasjid.adminapp.ui.kegiatan

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.fragment_url_dialog.*
import net.ayokemasjid.adminapp.databinding.FragmentUrlDialogBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein

class UrlKegiatanDialogFragment : DialogFragment(), KodeinAware, MainListener {

    override val kodein by kodein()

    private var _binding: FragmentUrlDialogBinding? = null
    private val binding get() = _binding!!

    private var listActivity: AddKegiatanActivity? = null

    private var type: String? = "image"

    interface OnInputListener{
        fun sendInput(url: String, title: String, width: Int, height: Int, type: String)
    }

    private var mOnInputListener: OnInputListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentUrlDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)

        setListener()
        //get bundle
        type = arguments?.getSerializable("type") as String
        when (type) {
            "image" -> {
                judulDialog.text = "Add Image URL"
                etTitle.hint = "Isi nama image"
            }
            "youtube" -> {
                judulDialog.text = "Add YouTube URL"
                etTitle.visibility = View.GONE
                layoutUkuran.visibility = View.GONE
            }
            "video" -> {
                judulDialog.text = "Add .MP4 URL"
                etTitle.visibility = View.GONE
            }
            "audio" -> {
                judulDialog.text = "Add .MP3 URL"
                etTitle.visibility = View.GONE
                layoutUkuran.visibility = View.GONE
            }
            "link" -> {
                judulDialog.text = "Add Link URL"
                layoutUkuran.visibility = View.GONE
            }
        }

        buttonBatal.setOnClickListener {
            dismiss()
        }

        buttonAdd.setOnClickListener {
            val url = etUrl.text.toString()
            val typeUrl = type
            var title = ""
            if(!etTitle.text.isNullOrEmpty() && etTitle.visibility == View.VISIBLE){
                title = etTitle.text.toString()
            }
            var width = 360
            if(!etWidth.text.isNullOrEmpty() && layoutUkuran.visibility == View.VISIBLE){
                width = etWidth.text.toString().toInt()
            }
            var height = 240
            if(!etHeight.text.isNullOrEmpty() && layoutUkuran.visibility == View.VISIBLE){
                height = etHeight.text.toString().toInt()
            }
            if(isNoEmptyForm()){
                mOnInputListener!!.sendInput(url
                    ,title,width,height,typeUrl!!)
                dismiss()
            }
        }
    }

    private fun isNoEmptyForm(): Boolean{
        var bool = true
        if(etUrl.text.isNullOrEmpty()){
            etUrl.error = "Isi URL dengan benar!"
            bool = false
        }
        if(etTitle.visibility == View.VISIBLE && etTitle.text.isNullOrEmpty()){
            etTitle.error = "Isi text dengan benar!"
            bool = false
        }
        return bool
    }

    private fun setListener(){
        etUrl.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (etUrl.text.isNullOrEmpty()) {
                    etUrl.error = "Isi URL dengan benar!"
                    return
                } else {
                    etUrl.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        etTitle.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (etTitle.visibility == View.VISIBLE && etTitle.text.isNullOrEmpty()) {
                    etUrl.error = "Isi text dengan benar!"
                    return
                } else {
                    etTitle.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            val a: Activity
            if (context is Activity) {
                a = context
                listActivity = a as AddKegiatanActivity
                mOnInputListener = activity as OnInputListener
            }
        } catch (e: ClassCastException) {
            throw ClassCastException("$activity must implement FeedbackListener")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStarted() {
    }

    override fun getMessage(message: String, status: Boolean?) {
    }

}