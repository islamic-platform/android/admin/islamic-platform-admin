package net.ayokemasjid.adminapp.ui.infaq

import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.ViewHolder
import kotlinx.android.synthetic.main.activity_list_infaq.*
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Infaq
import net.ayokemasjid.adminapp.databinding.ActivityListInfaqBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.snackbar
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.io.FileOutputStream
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*


class ListInfaqActivity : AppCompatActivity(), MainListener, KodeinAware {

    override val kodein by kodein()
    private val factory by instance<InfaqViewModelFactory>()

    private lateinit var binding: ActivityListInfaqBinding
    private lateinit var viewModel: InfaqViewModel

    private var isLayoutSaveVisible = false

    private var dataExcel: StringBuilder? = null

    private var isDateFormatted = false
    private var isRupiahFormatted = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_list_infaq)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_infaq)
        viewModel = ViewModelProvider(this, factory).get(InfaqViewModel::class.java)

        swipeRefreshLayout.isRefreshing = true
        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.infaqListener = this

        //recycle view
        bindUI()
        setListener()
    }

    private fun changeFormat(list: List<Infaq>){
        for(infaq in list){
            infaq.date = getDate(infaq.date!!)
            /*if(isDateFormatted){
                infaq.total = getRupiah(infaq.total!!)
            }*/
        }
    }

    private fun getDate(string: String): String{
        return try{
            val localeID = Locale("in", "ID")
            val date: Date? = SimpleDateFormat("yyyy-MM-dd", localeID).parse(string)
            val formatter = SimpleDateFormat("EEEE, dd MMMM yyyy", localeID)
            isDateFormatted = true
            formatter.format(date)
        } catch (e: Exception){
            isDateFormatted = false
            string
        }
    }

    private fun getRupiah(string: String): String{
        return try{
            val localeID = Locale("in", "ID")
            val otherSymbols = DecimalFormatSymbols(localeID)
            otherSymbols.decimalSeparator = ','
            otherSymbols.groupingSeparator = '.'
            val format = NumberFormat.getInstance() as DecimalFormat
            val symbol = Currency.getInstance(localeID).getSymbol(localeID)
            format.isGroupingUsed = true
            format.positivePrefix = "$symbol. "
            format.negativePrefix = "-$symbol. "
            format.minimumFractionDigits = 2
            format.maximumFractionDigits = 2
            format.decimalFormatSymbols = otherSymbols

            return format.format(string.toDouble())
        } catch (e: Exception){
            string
        }
    }

    //recycle view
    private fun bindUI() = Coroutines.main {
        //recycle view
        viewModel.infaqItems.await().observe(this, {
            var mutableList : MutableList<Infaq> = ArrayList()
            mutableList = it.toMutableList()
            //recycle view
            initRecyclerView(it.toInfaqItem())

            //add data to excel
            val currentDate = getCurrentDate()
            dataExcel = StringBuilder()
            dataExcel!!.append("Data Infaq $currentDate")
            dataExcel!!.append("No,Date,Name,Jumlah Infaq,Information")
            if (!mutableList.isNullOrEmpty()) {
                changeFormat(mutableList)
                for ((x, data) in mutableList.withIndex()) {
                    val number = x + 1
                    val date = data.date!!
                    val category = data.category!!
                    val total = data.total!!
                    val information = data.info!!

                    dataExcel!!.append(
                        "\n\"$number\",\"$date\",\"$category\",\"$total\",\"$information\""
                    )
                }
            }
        })

        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun exportExcel(data: StringBuilder?){
        try {
            val currentDate = getCurrentDate()
            //saving the file into device
            val out: FileOutputStream = openFileOutput(
                "dataInfaq[$currentDate].csv",
                Context.MODE_PRIVATE
            )
            out.write(data.toString().toByteArray())
            out.close()

            //exporting
            val context: Context = applicationContext
            val fileLocation = File(filesDir, "dataInfaq[$currentDate].csv")
            val path: Uri = FileProvider.getUriForFile(
                context,
                "net.ayokemasjid.adminapp.fileprovider",
                fileLocation
            )
            val fileIntent = Intent(Intent.ACTION_SEND)
            val resInfoList =
                packageManager.queryIntentActivities(fileIntent, PackageManager.MATCH_DEFAULT_ONLY)
            for (resolveInfo in resInfoList) {
                val packageName = resolveInfo.activityInfo.packageName
                applicationContext.grantUriPermission(
                    packageName,
                    path,
                    Intent.FLAG_GRANT_READ_URI_PERMISSION
                )
            }
            fileIntent.type = "text/csv"
            fileIntent.putExtra(Intent.EXTRA_SUBJECT, "dataInfaq[$currentDate]")
            fileIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            fileIntent.putExtra(Intent.EXTRA_STREAM, path)
            startActivity(Intent.createChooser(fileIntent, "Send via"))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun getCurrentDate(): String{
        val current = Calendar.getInstance()
        val dayOfMonth = current.get(Calendar.DAY_OF_MONTH)
        val bulan = current.get(Calendar.MONTH)
        val year = current.get(Calendar.YEAR)

        return "$year-$bulan-$dayOfMonth"
    }

    private fun setListener(){
        btnAdd.setOnClickListener {
            startActivity(Intent(this, AddInfaqActivity::class.java))
        }

        btnSaveAs.setOnClickListener {
            if(isLayoutSaveVisible){
                layout_btnSave.visibility = View.GONE
                isLayoutSaveVisible = false
            } else{
                layout_btnSave.visibility = View.VISIBLE
                isLayoutSaveVisible = true
            }
        }

        btnExcel.setOnClickListener {
            exportExcel(dataExcel)
        }

        swipeRefreshLayout.setOnRefreshListener {
            refresh(root_layout)
        }

    }

    //recycle view
    private fun List<Infaq>.toInfaqItem() : List<InfaqItem>{
        return this.map {
            InfaqItem(it, viewModel, this@ListInfaqActivity)
        }
    }

    //recycle view
    private fun initRecyclerView(jamaahItem: List<InfaqItem>){
        val mAdapter = GroupAdapter<ViewHolder>().apply {
            addAll(jamaahItem)
        }

        recyclerview.apply {
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            adapter = mAdapter
        }
    }

    //button back di topbar
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onStarted() {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = true
        }
    }

    override fun getMessage(message: String, status: Boolean?) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
        root_layout.snackbar(message)
    }

    fun refresh(view: View) {
        val intent: Intent = intent
        finish()
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }

    override fun onRestart() {
        super.onRestart()
        refresh(root_layout)
    }
}