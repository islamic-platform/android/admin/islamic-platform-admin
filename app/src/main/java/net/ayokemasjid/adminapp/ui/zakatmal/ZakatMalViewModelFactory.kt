package net.ayokemasjid.adminapp.ui.zakatmal

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.ayokemasjid.adminapp.data.repository.ZakatMalRepository

@Suppress("UNCHECKED_CAST")
class ZakatMalViewModelFactory(
    private val repository: ZakatMalRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ZakatMalViewModel(repository) as T
    }
}