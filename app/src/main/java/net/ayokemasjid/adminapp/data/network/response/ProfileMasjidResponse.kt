package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.ProfileMasjid

class ProfileMasjidResponse (
    val status : Boolean?,
    val message : String?,
    val data: ProfileMasjid?
)