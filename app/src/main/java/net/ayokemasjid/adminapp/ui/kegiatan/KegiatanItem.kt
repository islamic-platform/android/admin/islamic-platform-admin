package net.ayokemasjid.adminapp.ui.kegiatan

import android.content.Intent
import android.os.Bundle
import com.xwray.groupie.databinding.BindableItem
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Kegiatan
import net.ayokemasjid.adminapp.databinding.ItemKegiatanBinding

class KegiatanItem (
    val kegiatan: Kegiatan,
    private val viewModel: KegiatanViewModel,
    val activity: ListKegiatanActivity
): BindableItem<ItemKegiatanBinding>() {

    override fun getLayout() = R.layout.item_kegiatan

    override fun bind(viewBinding: ItemKegiatanBinding, position: Int) {
        viewBinding.kegiatan = kegiatan
        viewBinding.viewmodel = viewModel

        viewBinding.btnDetailKegiatan.setOnClickListener {
            val intent = Intent(activity.baseContext, DetailKegiatanActivity::class.java)
            intent.putExtra("data", kegiatan)
            activity.startActivity(intent)
        }
        viewBinding.btnEditKegiatan.setOnClickListener {
            val intent = Intent(activity.baseContext, AddKegiatanActivity::class.java)
            intent.putExtra("data", kegiatan)
            intent.putExtra("status","edit")
            activity.startActivity(intent)
        }
        viewBinding.btnDeleteKegiatan.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("ID", kegiatan.id_task!!)

            val fm = activity.supportFragmentManager
            val fragment = DeleteKegiatanDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Delete Dialog")
        }
    }
}