package net.ayokemasjid.adminapp.data.model

import com.google.gson.annotations.SerializedName

data class PengurusData (
    @SerializedName("name")
    var name: String,
    @SerializedName("position")
    var position: String
)