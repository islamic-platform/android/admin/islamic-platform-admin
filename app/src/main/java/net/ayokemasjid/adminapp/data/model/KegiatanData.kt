package net.ayokemasjid.adminapp.data.model

import com.google.gson.annotations.SerializedName

data class KegiatanData (
    @SerializedName("id_task")
    var id_task: Int,
    @SerializedName("title")
    var title: String,
    @SerializedName("people")
    var people: String,
    @SerializedName("information")
    var information: String,
    @SerializedName("date")
    var date: String
)