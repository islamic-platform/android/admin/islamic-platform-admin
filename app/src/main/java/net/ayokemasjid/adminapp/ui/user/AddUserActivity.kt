package net.ayokemasjid.adminapp.ui.user

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.DatePicker
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_add_user.*
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.User
import net.ayokemasjid.adminapp.databinding.ActivityAddUserBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.ui.user.UserViewModel
import net.ayokemasjid.adminapp.ui.user.UserViewModelFactory
import net.ayokemasjid.adminapp.util.hide
import net.ayokemasjid.adminapp.util.show
import net.ayokemasjid.adminapp.util.snackbar
import net.ayokemasjid.adminapp.util.toast
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class AddUserActivity : AppCompatActivity(), KodeinAware,
    UserListener {

    override val kodein by kodein()
    private val factory by instance<UserViewModelFactory>()
    private lateinit var viewModel: UserViewModel

    private lateinit var binding: ActivityAddUserBinding

    private var pesan: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_user)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_user)
        viewModel = ViewModelProvider(this, factory).get(UserViewModel::class.java)
        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.userListener = this

        setListener(binding)
        setButtonListener()
        setData()
    }

    private fun setData(){
        val data = intent.getSerializableExtra("data") as User
        viewModel.user = data
    }

    private fun setButtonListener(){
        buttonEditUser.setOnClickListener {
            if(isNoFormError(binding,"info")){
                viewModel.onEditButtonClick()
            } else{
                root_layout.snackbar("Ada form yang kosong!")
            }
        }
        /*buttonEditPassword.setOnClickListener {
            if(isNoFormError(binding,"password")){
                viewModel.onChangePassword()
            } else{
                root_layout.snackbar(pesan)
            }
        }
        checkboxPasswordLama.setOnCheckedChangeListener { compoundButton, isChecked ->
            if (isChecked) {
                // show password
                etPasswordLama.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                // hide password
                etPasswordLama.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }
        checkboxPasswordBaru.setOnCheckedChangeListener { compoundButton, isChecked ->
            if (isChecked) {
                // show password
                etPasswordBaru.transformationMethod = HideReturnsTransformationMethod.getInstance()
            } else {
                // hide password
                etPasswordBaru.transformationMethod = PasswordTransformationMethod.getInstance()
            }
        }*/
    }

    private fun setListener(binding: ActivityAddUserBinding){
        binding.etNamaAwal.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etNamaAwal.text.isNullOrEmpty()){
                    binding.etNamaAwal.error = "Isi nama awal dengan benar!"
                    return
                } else{
                    binding.etNamaAwal.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etNamaAkhir.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etNamaAkhir.text.isNullOrEmpty()){
                    binding.etNamaAkhir.error = "Isi nama akhir dengan benar!"
                    return
                } else{
                    binding.etNamaAkhir.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etUsername.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etUsername.text.isNullOrEmpty()){
                    binding.etUsername.error = "Isi username dengan benar!"
                    return
                } else{
                    binding.etUsername.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etEmail.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etEmail.text.isNullOrEmpty()){
                    binding.etEmail.error = "Isi email dengan benar!"
                    return
                } else{
                    binding.etEmail.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etTelepon.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etTelepon.text.isNullOrEmpty()){
                    binding.etTelepon.error = "Isi nomor telepon dengan benar!"
                    return
                } else{
                    binding.etTelepon.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etAlamat.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etAlamat.text.isNullOrEmpty()){
                    binding.etAlamat.error = "Isi alamat dengan benar!"
                    return
                } else{
                    binding.etAlamat.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etKota.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etKota.text.isNullOrEmpty()){
                    binding.etKota.error = "Isi nama kota dengan benar!"
                    return
                } else{
                    binding.etKota.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etProvinsi.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etProvinsi.text.isNullOrEmpty()){
                    binding.etProvinsi.error = "Isi nama provinsi dengan benar!"
                    return
                } else{
                    binding.etProvinsi.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etNegara.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etNegara.text.isNullOrEmpty()){
                    binding.etNegara.error = "Isi nama negara dengan benar!"
                    return
                } else{
                    binding.etNegara.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etPostcode.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etPostcode.text.isNullOrEmpty()){
                    binding.etPostcode.error = "Isi kode pos dengan benar!"
                    return
                } else{
                    binding.etPostcode.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        /*binding.etPasswordLama.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etPasswordLama.text.isNullOrEmpty()){
                    binding.etPasswordLama.error = "Isi password lama dengan benar!"
                    return
                } else{
                    binding.etPasswordLama.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etPasswordBaru.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etPasswordBaru.text.isNullOrEmpty()){
                    binding.etPasswordBaru.error = "Isi password baru dengan benar!"
                    return
                } else{
                    binding.etPasswordBaru.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etKonfirmasiPassword.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etKonfirmasiPassword.text.isNullOrEmpty()){
                    binding.etKonfirmasiPassword.error = "Isi password baru dengan benar!"
                    return
                } else{
                    binding.etKonfirmasiPassword.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })*/
    }

    private fun isNoFormError(binding: ActivityAddUserBinding, string: String): Boolean{
        var status = true
        if(string == "info"){
            if(binding.viewmodel!!.user!!.first_name.isNullOrEmpty()) {
                binding.etNamaAwal.error = "Isi nama awal dengan benar!"
                if(status){
                    status = false
                }
            }
            if(binding.viewmodel!!.user!!.last_name.isNullOrEmpty()) {
                binding.etNamaAkhir.error = "Isi nama akhir dengan benar!"
                if(status){
                    status = false
                }
            }
            if(binding.viewmodel!!.user!!.username.isNullOrEmpty()) {
                binding.etUsername.error = "Isi username dengan benar!"
                if(status){
                    status = false
                }
            }
            if(binding.viewmodel!!.user!!.email.isNullOrEmpty()) {
                binding.etEmail.error = "Isi email dengan benar!"
                if(status){
                    status = false
                }
            }
            if(binding.viewmodel!!.user!!.phone.isNullOrEmpty()) {
                binding.etTelepon.error = "Isi nomor telepon dengan benar!"
                if(status){
                    status = false
                }
            }
            if(binding.viewmodel!!.user!!.address.isNullOrEmpty()) {
                binding.etAlamat.error = "Isi alamat dengan benar!"
                if(status){
                    status = false
                }
            }
            if(binding.viewmodel!!.user!!.city.isNullOrEmpty()) {
                binding.etKota.error = "Isi nama kota dengan benar!"
                if(status){
                    status = false
                }
            }
            if(binding.viewmodel!!.user!!.province.isNullOrEmpty()) {
                binding.etProvinsi.error = "Isi nama provinsi dengan benar!"
                if(status){
                    status = false
                }
            }
            if(binding.viewmodel!!.user!!.country.isNullOrEmpty()) {
                binding.etNegara.error = "Isi nama negara dengan benar!"
                if(status){
                    status = false
                }
            }
            if(binding.viewmodel!!.user!!.postcode.isNullOrEmpty()) {
                binding.etPostcode.error = "Isi kode pos dengan benar!"
                if(status){
                    status = false
                }
            }
        }/*else if(string == "password"){
            if(binding.viewmodel!!.oldPassword.isNullOrEmpty()) {
                binding.etPasswordLama.error = "Isi password lama dengan benar!"
                pesan = "Ada form yang kosong!"
                if(status){
                    status = false
                }
            }
            if(binding.viewmodel!!.newPassword.isNullOrEmpty()) {
                binding.etPasswordBaru.error = "Isi password baru dengan benar!"
                pesan = "Ada form yang kosong!"
                if(status){
                    status = false
                }
            }
            if(binding.viewmodel!!.confirmPassword.isNullOrEmpty()) {
                binding.etKonfirmasiPassword.error = "Isi password baru dengan benar!"
                pesan = "Ada form yang kosong!"
                if(status){
                    status = false
                }
            }
            if(!binding.viewmodel!!.confirmPassword.equals(binding.viewmodel!!.newPassword)){
                pesan = "Konfirmasi Password Tidak Sesuai!"
                if(status){
                    status = false
                }
            }
        }*/
        return status
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if(activity.currentFocus != null){
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }

    override fun onStarted() {
        hideSoftKeyboard(this)
        progress_bar.show()
    }

    override fun onSuccess(message: String, user: User?) {
        progress_bar.hide()
        toast(message)
        viewModel.user = user
        refresh(root_layout)
    }

    override fun getMessage(message: String, status: Boolean?) {
        progress_bar.hide()
        root_layout.snackbar(message)
    }

    fun refresh(view: View) {
        val intent: Intent = intent
        finish()
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION)
        overridePendingTransition(0, 0)
        startActivity(intent)
        overridePendingTransition(0, 0)
    }
}