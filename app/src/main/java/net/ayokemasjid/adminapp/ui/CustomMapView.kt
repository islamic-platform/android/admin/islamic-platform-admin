package net.ayokemasjid.adminapp.ui.home

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.ViewParent
import com.mapbox.mapboxsdk.maps.MapView
import org.jetbrains.annotations.Nullable

class Location(var lat: Double, var long: Double, var nama: String, var telp: String)

class CustomMapView : MapView {
    private var mViewParent: ViewParent? = null

    constructor(context: Context?) : super(context!!) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context!!, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(
        context!!,
        attrs,
        defStyle
    ) {
    }

    fun setViewParent(@Nullable viewParent: ViewParent?) { //any ViewGroup
        mViewParent = viewParent
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> if (null == mViewParent) {
                parent.requestDisallowInterceptTouchEvent(true)
            } else {
                mViewParent!!.requestDisallowInterceptTouchEvent(true)
            }
            MotionEvent.ACTION_UP -> if (null == mViewParent) {
                parent.requestDisallowInterceptTouchEvent(false)
            } else {
                mViewParent!!.requestDisallowInterceptTouchEvent(false)
            }
            else -> {
            }
        }
        return super.onInterceptTouchEvent(event)
    }
}