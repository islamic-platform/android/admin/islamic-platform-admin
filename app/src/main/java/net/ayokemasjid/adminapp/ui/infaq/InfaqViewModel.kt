package net.ayokemasjid.adminapp.ui.infaq

import androidx.lifecycle.ViewModel
import net.ayokemasjid.adminapp.data.model.Infaq
import net.ayokemasjid.adminapp.data.model.InfaqData
import net.ayokemasjid.adminapp.data.repository.InfaqRepository
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.ApiException
import net.ayokemasjid.adminapp.util.Coroutines
import net.ayokemasjid.adminapp.util.NoInternetException
import net.ayokemasjid.adminapp.util.lazyDeferred

class InfaqViewModel(
    private val repository: InfaqRepository
): ViewModel()   {

    var idInfaq: Int? = null
    var date: String? = null
    var category: String? = null
    var total: String? = null
    var info: String? = null

    var infaqListener: MainListener? = null

    val infaqItems by lazyDeferred{
        repository.getAllInfaq()
    }

    fun setJenis(data: String){
        category = data
    }

    fun onAddButtonClick(){
        infaqListener?.onStarted()
        Coroutines.main {
            try {
                val data = InfaqData(category!!,total!!,info!!,date!!)
                val infaqResponse = repository.addInfaq(data)
                infaqListener?.getMessage(infaqResponse.message!!,infaqResponse.status)
            } catch (e: ApiException){
                infaqListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                infaqListener?.getMessage(e.message!!,false)
            }
        }
    }

    fun onEditButtonClick(){
        infaqListener?.onStarted()
        Coroutines.main{
            try {
                val data = Infaq(idInfaq!!,category!!,total!!,info!!,date!!)
                val infaqResponse = repository.editInfaq(data)
                infaqListener?.getMessage(infaqResponse.message!!,infaqResponse.status)
            } catch (e: ApiException){
                infaqListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                infaqListener?.getMessage(e.message!!,false)
            }
        }
    }

    fun onDeleteButton(idInfaq : Int){
        infaqListener?.onStarted()
        Coroutines.main {
            try{
                val respon = repository.deleteInfaq(idInfaq)
                infaqListener?.getMessage(respon.message!!,false)
            } catch (e: ApiException){
                infaqListener?.getMessage(e.message!!,false)
            } catch (e: NoInternetException){
                infaqListener?.getMessage(e.message!!,false)
            }
        }
    }
}