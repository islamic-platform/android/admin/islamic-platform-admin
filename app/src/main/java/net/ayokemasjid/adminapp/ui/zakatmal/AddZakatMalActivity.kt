package net.ayokemasjid.adminapp.ui.zakatmal

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_add_zakat_mal.*
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.databinding.ActivityAddZakatMalBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.hide
import net.ayokemasjid.adminapp.util.show
import net.ayokemasjid.adminapp.util.snackbar
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class AddZakatMalActivity : AppCompatActivity(), KodeinAware,
    MainListener, DatePickerDialog.OnDateSetListener {

    override val kodein by kodein()
    private val factory by instance<ZakatMalViewModelFactory>()
    private lateinit var viewModel: ZakatMalViewModel

    private lateinit var binding: ActivityAddZakatMalBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_zakat_mal)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_zakat_mal)
        viewModel = ViewModelProvider(this, factory).get(ZakatMalViewModel::class.java)
        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.zakatMalListener = this

        setListener(binding)
        setDate(binding)
        setButtonListener()
    }

    private fun setDate(binding: ActivityAddZakatMalBinding){
        val current = Calendar.getInstance()
        val dayOfMonth = current.get(Calendar.DAY_OF_MONTH)
        val bulan = current.get(Calendar.MONTH)
        val year = current.get(Calendar.YEAR)

        val date = "$year-$bulan-$dayOfMonth"
        binding.viewmodel!!.date = date
        binding.etTanggal.text = date
    }

    private fun setButtonListener(){
        etTanggal.setOnClickListener { showDatePickerDialog() }

        buttonAddZakatMal.setOnClickListener {
            if(isNoFormError(binding)){
                viewModel.onAddButtonClick()
            } else{
                root_layout.snackbar("Ada form yang kosong!")
            }
        }
    }

    private fun showDatePickerDialog() {
        val datePickerDialog = DatePickerDialog(
            this,
            this,
            Calendar.getInstance().get(Calendar.YEAR),
            Calendar.getInstance().get(Calendar.MONTH),
            Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val bulan = month + 1
        val date = "$year-$bulan-$dayOfMonth"
        etTanggal.text = date
    }

    private fun setListener(binding: ActivityAddZakatMalBinding){
        binding.etNama.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etNama.text.isNullOrEmpty()){
                    binding.etNama.error = "Isi nama jamaah dengan benar!"
                    return
                } else{
                    binding.etNama.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etJumlahZakat.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etJumlahZakat.text.isNullOrEmpty()){
                    binding.etJumlahZakat.error = "Isi jumlah zakat dengan benar!"
                    return
                } else{
                    binding.etJumlahZakat.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun isNoFormError(binding: ActivityAddZakatMalBinding): Boolean{
        var status = true
        if(binding.viewmodel!!.name.isNullOrEmpty()) {
            binding.etNama.error = "Isi nama jamaah dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.total.isNullOrEmpty()) {
            binding.etJumlahZakat.error = "Isi jumlah zakat dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.info.isNullOrEmpty()) {
            binding.viewmodel!!.info = "-"
        }
        return status
    }

    private fun clearFormError(){
        etNama.text.clear()
        etNama.error = null
        etJumlahZakat.text.clear()
        etJumlahZakat.error = null
        etKeterangan.text.clear()
        setDate(binding)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if(activity.currentFocus != null){
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }

    override fun onStarted() {
        hideSoftKeyboard(this)
        progress_bar.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        progress_bar.hide()
        if(status!!){
            clearFormError()
        }
        root_layout.snackbar(message)
    }
}