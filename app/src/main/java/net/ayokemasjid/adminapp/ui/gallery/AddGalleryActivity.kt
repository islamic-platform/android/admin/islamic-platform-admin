package net.ayokemasjid.adminapp.ui.gallery

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_add_gallery.*
import kotlinx.android.synthetic.main.activity_add_gallery.progress_bar
import kotlinx.android.synthetic.main.activity_add_gallery.root_layout
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.databinding.ActivityAddGalleryBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.ui.UploadRequestBody
import net.ayokemasjid.adminapp.util.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class AddGalleryActivity : AppCompatActivity(), KodeinAware,
    MainListener, UploadRequestBody.UploadCallback {

    override val kodein by kodein()
    private val factory by instance<GalleryViewModelFactory>()
    private lateinit var viewModel: GalleryViewModel

    private lateinit var binding: ActivityAddGalleryBinding

    private var isAdded = false
    private var uri: Uri? = null

    companion object {
        const val REQUEST_IMAGE_SELECTOR = 110
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_gallery)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_gallery)
        viewModel = ViewModelProvider(this, factory).get(GalleryViewModel::class.java)
        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.galleryListener = this

        setListener(binding)
        setButtonListener()
    }

    private fun setButtonListener(){
        buttonAddGallery.setOnClickListener {
            if(isNoFormError(binding)){
                viewModel.onAddButtonClick()
            } else{
                root_layout.snackbar("Ada form yang kosong!")
            }
        }

        buttonPilihGambar.setOnClickListener {
            openGallery()
        }

        buttonRemoveImage.setOnClickListener {
            layout_preview.visibility = View.GONE

            val attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "")
            viewModel.setImage(attachmentEmpty, "")
        }
    }

    private fun setListener(binding: ActivityAddGalleryBinding){
        binding.etTitle.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (binding.etTitle.text.isNullOrEmpty()) {
                    binding.etTitle.error = "Isi judul gallery dengan benar!"
                    return
                } else {
                    binding.etTitle.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun isNoFormError(binding: ActivityAddGalleryBinding): Boolean{
        var status = true
        if(binding.viewmodel!!.caption.isNullOrEmpty()) {
            binding.etTitle.error = "Isi judul gallery dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.file_name.isNullOrEmpty()){
            val attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "")
            viewModel.setImage(attachmentEmpty, "")
        }
        return status
    }

    private fun clearFormError(){
        isAdded = true
        etTitle.text.clear()
        etTitle.error = null
        layout_preview.visibility = View.GONE
        val attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "")
        viewModel.setImage(attachmentEmpty, "")
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if(activity.currentFocus != null){
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }

    override fun onStarted() {
        hideSoftKeyboard(this)
        progress_bar.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        progress_bar.hide()
        root_layout.snackbar(message)
        clearFormError()
    }
    
    @Override
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_IMAGE_SELECTOR) {
            if (resultCode == RESULT_OK && data != null && data.data != null) {
                uri = data.data

                val parcelFileDescriptor =
                    contentResolver.openFileDescriptor(uri!!, "r", null) ?: return

                val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
                val file = File(cacheDir, contentResolver.getFileName(uri!!))
                val outputStream = FileOutputStream(file)
                inputStream.copyTo(outputStream)

                val body = UploadRequestBody(file, "main_image", this)

                preview_image.setImageURI(uri)
                layout_preview.visibility = View.VISIBLE
                viewModel.setImage(body, file.name)
            }
        }
    }

    @Override
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun openGallery() {
        try {
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    REQUEST_IMAGE_SELECTOR
                )
            } else {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(intent, REQUEST_IMAGE_SELECTOR)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onProgressUpdate(percentage: Int) {

    }
}