package net.ayokemasjid.adminapp.ui.pengaturan

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_pengaturan.*
import kotlinx.android.synthetic.main.fragment_manajemen.*
import kotlinx.android.synthetic.main.fragment_profile_web.*
import kotlinx.android.synthetic.main.fragment_profile_web.swipeRefreshLayout
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.preferences.PreferenceProvider
import net.ayokemasjid.adminapp.databinding.FragmentProfileWebBinding
import net.ayokemasjid.adminapp.ui.UploadRequestBody
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.*
import okhttp3.MediaType
import okhttp3.RequestBody
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.kodein
import org.kodein.di.generic.instance
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream

class ProfileWebFragment : DialogFragment(), KodeinAware, MainListener, UploadRequestBody.UploadCallback {

    override val kodein by kodein()
    private lateinit var viewModel: PengaturanViewModel
    private val factory by instance<PengaturanViewModelFactory>()
    private val preference by instance<PreferenceProvider>()

    private var _binding: FragmentProfileWebBinding? = null
    private val binding get() = _binding!!

    companion object {
        const val REQUEST_IMAGE_SLIDER = 110
        const val REQUEST_IMAGE_SAMBUTAN = 120
        const val REQUEST_IMAGE_LOGO = 130
    }
    private var uri: Uri? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentProfileWebBinding.inflate(inflater, container, false)
        viewModel = ViewModelProvider(this, factory).get(PengaturanViewModel::class.java)
        binding.viewmodel = viewModel
        viewModel.pengaturanListener = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        setData()
        setButtonListener()
        setListener()
    }

    private fun setData() = Coroutines.main {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = true
        }
        viewModel.masjidItems.await().observe(this, {
            viewModel.dataMasjid = it
            etNamaMasjid.setText(it.name)
            etMottoMasjid.setText(it.motto)
            val slide_pic = it.slider_pic
            etLuasMasjid.setText(it.width)
            etKapasitasMasjid.setText(it.volume)
            etShortMasjid.setText(it.about_short)
            etLongMasjid.setText(it.about_long)
            etSambutanTakmir.setText(it.greeting)
            val greeting_pic = it.greeting_pic
            val logo = it.logo

            if (!slide_pic.isNullOrEmpty()) {
                layout_slider.visibility = View.VISIBLE
                val url =
                    preference.getImageURL() ?: "http://staging.ayokemasjid.net/assets/uploads/"
                Picasso.get()
                    .load(url + slide_pic)
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.logo_placeholder)
                    .into(preview_slider)

                /*val file = File(url+slider_pic)
                val requestFile: RequestBody = RequestBody.create(
                    MediaType.parse
                        ("multipart/form-data"), file)*/
                val requestFile = RequestBody.create(
                    MediaType.parse("text/plain"),
                    slide_pic
                )
                viewModel.setSlider(requestFile, slide_pic)
            }
            if (!greeting_pic.isNullOrEmpty()) {
                layout_sambutan.visibility = View.VISIBLE
                val url =
                    preference.getImageURL() ?: "http://staging.ayokemasjid.net/assets/uploads/"
                Picasso.get()
                    .load(url + greeting_pic)
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.logo_placeholder)
                    .into(preview_sambutan)

                /*val file = File(url+viewModel.dataMasjid!!.greeting_pic)
                val requestFile: RequestBody = RequestBody.create(
                    MediaType.parse
                        ("multipart/form-data"), file)*/
                val requestFile = RequestBody.create(
                    MediaType.parse("text/plain"),
                    greeting_pic
                )
                viewModel.setSambutan(requestFile, greeting_pic)
            }
            if (!logo.isNullOrEmpty()) {
                layout_logo.visibility = View.VISIBLE
                val url =
                    preference.getImageURL() ?: "http://staging.ayokemasjid.net/assets/uploads/"
                Picasso.get()
                    .load(url + logo)
                    .placeholder(R.drawable.progress_animation)
                    .error(R.drawable.logo_placeholder)
                    .into(preview_logo)

                /*val file = File(url+logo)
                val requestFile: RequestBody = RequestBody.create(
                    MediaType.parse
                        ("multipart/form-data"), file)*/
                val requestFile = RequestBody.create(
                    MediaType.parse("text/plain"),
                    logo
                )
                viewModel.setLogo(requestFile, logo)
            }

        })
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
    }

    private fun setButtonListener(){
        swipeRefreshLayout.setOnRefreshListener {
            refresh()
        }

        buttonPilihSlider.setOnClickListener {
            openGallery("slider")
        }
        buttonRemoveSlider.setOnClickListener {
            tvPilihSlider.error = "Pilih gambar dengan benar!"
            layout_slider.visibility = View.GONE
            val attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "slider_pic")
            viewModel.setSlider(attachmentEmpty, "")
        }

        buttonPilihSambutan.setOnClickListener {
            openGallery("sambutan")
        }
        buttonRemoveSambutan.setOnClickListener {
            tvPilihSambutan.error = "Pilih gambar dengan benar!"
            layout_sambutan.visibility = View.GONE
            val attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "greeting_pic")
            viewModel.setSambutan(attachmentEmpty, "")
        }

        buttonPilihLogo.setOnClickListener {
            openGallery("logo")
        }
        buttonRemoveLogo.setOnClickListener {
            tvPilihLogo.error = "Pilih gambar dengan benar!"
            layout_logo.visibility = View.GONE
            val attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "logo")
            viewModel.setLogo(attachmentEmpty, "")
        }

        buttonProfileMasjid.setOnClickListener {
            onStarted()
            viewModel.dataMasjid?.id_mosque = 1
            if(isNoFormError()){
                viewModel.onProfileButtonClick()
                refresh()
            } else{
                (activity as PengaturanActivity?)?.toast("Ada form yang kosong!")
            }
            (activity as PengaturanActivity?)?.progress_bar?.hide()
        }
    }

    private fun openGallery(string: String) {
        try {
            if (ActivityCompat.checkSelfPermission(
                    requireContext(),
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    requireActivity(),
                    arrayOf(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ),
                    when (string) {
                        "slider" -> {
                            REQUEST_IMAGE_SLIDER
                        }
                        "sambutan" -> {
                            REQUEST_IMAGE_SAMBUTAN
                        }
                        else -> {
                            REQUEST_IMAGE_LOGO
                        }
                    }
                )
            } else {
                val intent = Intent()
                intent.type = "image/*"
                intent.action = Intent.ACTION_GET_CONTENT
                startActivityForResult(
                    intent,
                    when (string) {
                        "slider" -> {
                            REQUEST_IMAGE_SLIDER
                        }
                        "sambutan" -> {
                            REQUEST_IMAGE_SAMBUTAN
                        }
                        else -> {
                            REQUEST_IMAGE_LOGO
                        }
                    }
                )
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    @Override
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == AppCompatActivity.RESULT_OK && data != null && data.data != null) {
            uri = data.data

            val parcelFileDescriptor =
                activity?.contentResolver?.openFileDescriptor(uri!!, "r", null) ?: return

            val inputStream = FileInputStream(parcelFileDescriptor.fileDescriptor)
            val file = File(activity?.cacheDir, activity?.contentResolver?.getFileName(uri!!)!!)
            val outputStream = FileOutputStream(file)
            inputStream.copyTo(outputStream)

            when (requestCode) {
                REQUEST_IMAGE_SLIDER -> {
                    tvPilihSlider.error = null
                    val body = UploadRequestBody(file, "slider_pic", this)
                    preview_slider.setImageURI(uri)
                    layout_slider.visibility = View.VISIBLE
                    viewModel.setSlider(body, file.name)
                }
                REQUEST_IMAGE_SAMBUTAN -> {
                    tvPilihSambutan.error = null
                    val body = UploadRequestBody(file, "greeting_pic", this)
                    preview_sambutan.setImageURI(uri)
                    layout_sambutan.visibility = View.VISIBLE
                    viewModel.setSambutan(body, file.name)
                }
                REQUEST_IMAGE_LOGO -> {
                    tvPilihLogo.error = null
                    val body = UploadRequestBody(file, "logo", this)
                    preview_logo.setImageURI(uri)
                    layout_logo.visibility = View.VISIBLE
                    viewModel.setLogo(body, file.name)
                }
            }
        }
    }

    @Override
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    private fun setListener(){
        etNamaMasjid.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (etNamaMasjid.text.isNullOrEmpty()) {
                    etNamaMasjid.error = "Isi nama masjid dengan benar!"
                    return
                } else {
                    etNamaMasjid.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        etMottoMasjid.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (etMottoMasjid.text.isNullOrEmpty()) {
                    etMottoMasjid.error = "Isi motto masjid dengan benar!"
                    return
                } else {
                    etMottoMasjid.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        etLuasMasjid.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (etLuasMasjid.text.isNullOrEmpty()) {
                    etLuasMasjid.error = "Isi luas masjid dengan benar!"
                    return
                } else {
                    etLuasMasjid.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        etKapasitasMasjid.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (etKapasitasMasjid.text.isNullOrEmpty()) {
                    etKapasitasMasjid.error = "Isi kapasitas jama'ah dengan benar!"
                    return
                } else {
                    etKapasitasMasjid.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
        etSambutanTakmir.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if (etSambutanTakmir.text.isNullOrEmpty()) {
                    etSambutanTakmir.error = "Isi sambutan takmir dengan benar!"
                    return
                } else {
                    etSambutanTakmir.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun isNoFormError(): Boolean{
        var status = true
        if(viewModel.dataMasjid?.slider_pic.isNullOrEmpty()){
            tvPilihSlider.error = "Pilih gambar dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.greeting_pic.isNullOrEmpty()){
            tvPilihSambutan.error = "Pilih gambar dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.logo.isNullOrEmpty()){
            tvPilihLogo.error = "Pilih logo dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.name.isNullOrEmpty()){
            etNamaMasjid.error = "Isi nama masjid dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.motto.isNullOrEmpty()){
            etMottoMasjid.error = "Isi motto masjid dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.width.isNullOrEmpty()){
            etLuasMasjid.error = "Isi luas masjid dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.volume.isNullOrEmpty()){
            etKapasitasMasjid.error = "Isi kapasitas jama'ah dengan benar!"
            if(status){
                status = false
            }
        }
        if(viewModel.dataMasjid?.greeting.isNullOrEmpty()){
            etSambutanTakmir.error = "Isi sambutan takmir dengan benar!"
            if(status){
                status = false
            }
        }
        return status
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onStarted() {
        hideSoftKeyboard(requireActivity())
        (activity as PengaturanActivity?)?.progress_bar?.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        if (swipeRefreshLayout != null) {
            swipeRefreshLayout.isRefreshing = false
        }
        (activity as PengaturanActivity?)?.progress_bar?.hide()
        (activity as PengaturanActivity?)?.toast(message)
    }

    fun refresh(){
        (activity as PengaturanActivity?)?.refresh()
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if(activity.currentFocus != null){
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }

    override fun onProgressUpdate(percentage: Int) {

    }

    override fun onActivityCreated(@Nullable savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        hideSoftKeyboard(requireActivity())
    }
}