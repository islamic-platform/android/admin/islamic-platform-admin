package net.ayokemasjid.adminapp.ui.pengaturan

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.ayokemasjid.adminapp.data.repository.PengaturanRepository

@Suppress("UNCHECKED_CAST")
class PengaturanViewModelFactory(
    private val repository: PengaturanRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PengaturanViewModel(repository) as T
    }
}