package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.model.Mustahik
import net.ayokemasjid.adminapp.data.model.MustahikData
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.MustahikResponse

class MustahikRepository(
    private val api: MyApi
): SafeApiRequest() {

    private val mustahikItems = MutableLiveData<List<Mustahik>>()

    suspend fun addMustahik(mustahikData: MustahikData): MustahikResponse {
        return apiRequest { api.addMustahik(mustahikData) }
    }

    suspend fun deleteMustahik(idMustahik : Int): MustahikResponse {
        return apiRequest { api.deleteMustahik(idMustahik) }
    }

    suspend fun getAllMustahik(): LiveData<List<Mustahik>> {
        fetchAllMustahik()
        return withContext(Dispatchers.IO) {
            mustahikItems
        }
    }

    private suspend fun fetchAllMustahik() {
        try {
            val response = apiRequest { api.getMustahik() }
            mustahikItems.postValue(response.data)
        } catch (e: Exception) {
            val empty: List<Mustahik> = ArrayList()
            mustahikItems.postValue(empty)
            e.printStackTrace()
        }
    }
}