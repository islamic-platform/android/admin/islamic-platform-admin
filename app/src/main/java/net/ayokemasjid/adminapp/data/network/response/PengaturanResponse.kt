package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.PengaturanMasjid

class PengaturanResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<PengaturanMasjid>?
)