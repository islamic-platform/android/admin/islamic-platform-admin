package net.ayokemasjid.adminapp.data.network.response

import net.ayokemasjid.adminapp.data.model.User

data class AuthResponse (
    val status : Boolean?,
    val message : String?,
    val data: List<User>?
)