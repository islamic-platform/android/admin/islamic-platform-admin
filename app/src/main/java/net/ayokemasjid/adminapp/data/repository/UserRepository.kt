package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.model.User
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.AuthResponse

data class UserRepository(
    private val api: MyApi
): SafeApiRequest() {

    private val userItems = MutableLiveData<List<User>>()

    suspend fun userLogin(email: String, password: String): AuthResponse {
        return apiRequest { api.userLogin(email,password) }
    }

    suspend fun editUser(user: User): AuthResponse {
        return apiRequest { api.editUser(user) }
    }

    suspend fun getAllUser(): LiveData<List<User>> {
        fetchAllUser()
        return withContext(Dispatchers.IO) {
            userItems
        }
    }

    private suspend fun fetchAllUser() {
        try {
            val response = apiRequest { api.getUser() }
            userItems.postValue(response.data)
        } catch (e: Exception) {
            val empty: List<User> = ArrayList()
            userItems.postValue(empty)
            e.printStackTrace()
        }
    }
}