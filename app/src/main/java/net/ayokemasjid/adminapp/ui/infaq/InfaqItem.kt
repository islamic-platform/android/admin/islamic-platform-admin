package net.ayokemasjid.adminapp.ui.infaq

import android.content.Intent
import android.os.Bundle
import com.xwray.groupie.databinding.BindableItem
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.data.model.Infaq
import net.ayokemasjid.adminapp.databinding.ItemInfaqBinding

class InfaqItem(
    val infaq: Infaq,
    private val viewModel: InfaqViewModel,
    val activity: ListInfaqActivity
): BindableItem<ItemInfaqBinding>() {

    override fun getLayout() = R.layout.item_infaq

    override fun bind(viewBinding: ItemInfaqBinding, position: Int) {
        viewBinding.infaq = infaq
        viewBinding.viewmodel = viewModel

        viewBinding.btnEditInfaq.setOnClickListener {
            val intent = Intent(activity.baseContext, AddInfaqActivity::class.java)
            intent.putExtra("data", infaq)
            intent.putExtra("status","edit")
            activity.startActivity(intent)
        }
        viewBinding.btnDeleteInfaq.setOnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("ID", infaq.id_infaq!!)

            val fm = activity.supportFragmentManager
            val fragment = DeleteInfaqDialogFragment()
            fragment.arguments = bundle
            fragment.show(fm, "Delete Dialog")
        }
    }
}