package net.ayokemasjid.adminapp.ui.mustahik

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import kotlinx.android.synthetic.main.activity_add_mustahik.*
import net.ayokemasjid.adminapp.R
import net.ayokemasjid.adminapp.databinding.ActivityAddMustahikBinding
import net.ayokemasjid.adminapp.ui.etc.MainListener
import net.ayokemasjid.adminapp.util.hide
import net.ayokemasjid.adminapp.util.show
import net.ayokemasjid.adminapp.util.snackbar
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein
import org.kodein.di.generic.instance
import java.util.*

class AddMustahikActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener, KodeinAware,
    MainListener, DatePickerDialog.OnDateSetListener {

    override val kodein by kodein()
    private val factory by instance<MustahikViewModelFactory>()
    private lateinit var viewModel: MustahikViewModel

    private lateinit var binding: ActivityAddMustahikBinding

    private var isJenisSelected = false
    private var isAdded = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_mustahik)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_mustahik)
        viewModel = ViewModelProvider(this, factory).get(MustahikViewModel::class.java)
        setSupportActionBar(binding.toolbar)
        binding.viewmodel = viewModel
        viewModel.mustahikListener= this

        setListener(binding)
        setDate(binding)
        val spinner: Spinner = spinnerJenisZakat
        setSpinner(spinner)
        setButtonListener()

        disableSetJumlahZakat()
    }

    private fun setDate(binding: ActivityAddMustahikBinding){
        val current = Calendar.getInstance()
        val dayOfMonth = current.get(Calendar.DAY_OF_MONTH)
        val bulan = current.get(Calendar.MONTH)
        val year = current.get(Calendar.YEAR)

        val date = "$year-$bulan-$dayOfMonth"
        binding.viewmodel!!.date = date
        binding.etTanggal.text = date
    }

    private fun setButtonListener(){
        etTanggal.setOnClickListener { showDatePickerDialog() }

        buttonAddMustahik.setOnClickListener {
            if(isNoFormError(binding)){
                viewModel.onAddButtonClick()
            } else{
                root_layout.snackbar("Ada form yang kosong!")
            }
        }

        etJumlahZakat.setOnClickListener {
            if(!isJenisSelected){
                spinnerJenisZakat.performClick()
            }
        }
    }

    private fun enableSetJumlahZakat(){
        iconJenisZakat.visibility = View.VISIBLE
        isJenisSelected = true
        etJumlahZakat.isFocusable = true
        etJumlahZakat.isFocusableInTouchMode = true
        cardViewJumlah.alpha = 1F
    }

    private fun disableSetJumlahZakat(){
        iconJenisZakat.visibility = View.GONE
        isJenisSelected = false
        etJumlahZakat.isFocusable = false
        etJumlahZakat.text.clear()
        cardViewJumlah.alpha = 0.8F
    }

    private fun showDatePickerDialog() {
        val datePickerDialog = DatePickerDialog(
            this,
            this,
            Calendar.getInstance().get(Calendar.YEAR),
            Calendar.getInstance().get(Calendar.MONTH),
            Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        )
        datePickerDialog.show()
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        val bulan = month + 1
        val date = "$year-$bulan-$dayOfMonth"
        etTanggal.text = date
    }

    private fun setListener(binding: ActivityAddMustahikBinding){
        binding.etNama.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etNama.text.isNullOrEmpty()){
                    binding.etNama.error = "Isi nama jamaah dengan benar!"
                    return
                } else{
                    binding.etNama.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })

        binding.etJumlahZakat.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                if(binding.etJumlahZakat.text.isNullOrEmpty()){
                    binding.etJumlahZakat.error = "Isi jumlah zakat dengan benar!"
                    return
                } else{
                    binding.etJumlahZakat.error = null
                }
            }

            override fun afterTextChanged(s: Editable) {}
        })
    }

    private fun isNoFormError(binding: ActivityAddMustahikBinding): Boolean{
        var status = true
        if(binding.viewmodel!!.name.isNullOrEmpty()) {
            binding.etNama.error = "Isi nama penerima dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.total.isNullOrEmpty()) {
            binding.etJumlahZakat.error = "Isi jumlah zakat dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.category.isNullOrEmpty()) {
            val view: TextView = spinnerJenisZakat.selectedView as TextView
            view.error = "Pilih jenis zakat dengan benar!"
            if(status){
                status = false
            }
        }
        if(binding.viewmodel!!.info.isNullOrEmpty()) {
            binding.viewmodel!!.info = "-"
        }
        return status
    }

    private fun clearFormError(){
        isAdded = true
        etNama.text.clear()
        etNama.error = null
        etJumlahZakat.text.clear()
        etJumlahZakat.error = null
        spinnerJenisZakat.setSelection(0)
        etKeterangan.text.clear()
        setDate(binding)
    }

    private fun setSpinner(spinner: Spinner){
        ArrayAdapter.createFromResource(
            this,
            R.array.zakat_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            spinner.adapter = adapter
            spinner.setSelection(0, true)

            val tf = Typeface.createFromAsset(assets,"fonts/Roboto-Regular.ttf")
            val v = spinner.selectedView
            (v as TextView).setTextColor(Color.parseColor("#808080"))
            v.typeface = tf
            v.textSize = 14f
        }
        spinner.onItemSelectedListener = this
    }

    //Untuk Spinner spinnerJenisZakat
    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    //Untuk Spinner spinnerJenisZakat
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val text: String = parent?.getItemAtPosition(position).toString()
        if (parent != null) {
            (parent.getChildAt(0) as TextView).textSize = 14f
            (parent.getChildAt(0) as TextView).typeface =  Typeface.createFromAsset(assets,"fonts/Roboto-Regular.ttf")
            if(text == "Pilih jenis zakat"){
                (parent.getChildAt(0) as TextView).setTextColor(Color.parseColor("#808080"))
                if(isAdded){
                    (parent.getChildAt(0) as TextView).error = null
                    isAdded = false
                }else{
                    (parent.getChildAt(0) as TextView).error = "Pilih jenis zakat dengan benar!"
                }
                etJumlahZakat.hint = "Isi jumlah"
                disableSetJumlahZakat()
            }else{
                enableSetJumlahZakat()
                (parent.getChildAt(0) as TextView).error = null
                (parent.getChildAt(0) as TextView).setTextColor(Color.BLACK)
                if(text == "Beras"){
                    etJumlahZakat.hint = "Isi jumlah (Kg)"
                    iconJenisZakat.text = "Kg."
                } else if(text == "Uang"){
                    etJumlahZakat.hint = "Isi jumlah (Rp.)"
                    iconJenisZakat.text = "Rp."
                }
                binding.viewmodel!!.setJenis(text)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                super.onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            val imm: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun hideSoftKeyboard(activity: Activity) {
        if(activity.currentFocus != null){
            val inputMethodManager = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(activity.currentFocus!!.windowToken, 0)
        }
    }

    override fun onStarted() {
        hideSoftKeyboard(this)
        progress_bar.show()
    }

    override fun getMessage(message: String, status: Boolean?) {
        progress_bar.hide()
        if(status!!){
            clearFormError()
        }
        root_layout.snackbar(message)
    }
}