package net.ayokemasjid.adminapp.ui.infaq

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import net.ayokemasjid.adminapp.data.repository.InfaqRepository

@Suppress("UNCHECKED_CAST")
class InfaqViewModelFactory(
    private val repository: InfaqRepository
) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return InfaqViewModel(repository) as T
    }
}