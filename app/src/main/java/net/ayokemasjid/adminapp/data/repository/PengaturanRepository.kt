package net.ayokemasjid.adminapp.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import net.ayokemasjid.adminapp.data.model.*
import net.ayokemasjid.adminapp.data.network.MyApi
import net.ayokemasjid.adminapp.data.network.SafeApiRequest
import net.ayokemasjid.adminapp.data.network.response.PengaturanResponse
import net.ayokemasjid.adminapp.data.network.response.ProfileMasjidResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody

class PengaturanRepository(
    private val api: MyApi
): SafeApiRequest() {

    private val masjidItems = MutableLiveData<PengaturanMasjid>()

    suspend fun pengaturanMasjid(pengaturanMasjidData: PengaturanMasjidData): PengaturanResponse {
        return apiRequest { api.pengaturanMasjid(pengaturanMasjidData) }
    }

    suspend fun profileMasjid(
        id_mosque: RequestBody,
        name: RequestBody,
        motto: RequestBody,
        width: RequestBody,
        volume: RequestBody,
        about_short: RequestBody,
        about_long: RequestBody,
        greeting: RequestBody,
        slider_pic: MultipartBody.Part,
        greeting_pic: MultipartBody.Part,
        logo: MultipartBody.Part
    ): ProfileMasjidResponse {
        return apiRequest {
            api.profileMasjid(
                id_mosque,
                name,
                motto,
                width,
                volume,
                about_short,
                about_long,
                greeting,
                slider_pic,
                greeting_pic,
                logo
            )
        }
    }

    suspend fun getAllMasjid(): LiveData<PengaturanMasjid> {
        fetchAllMasjid()
        return withContext(Dispatchers.IO) {
            masjidItems
        }
    }

    private suspend fun fetchAllMasjid() {
        try {
            val response = apiRequest { api.getMasjid() }
            masjidItems.postValue(response.data?.get(0))
        } catch (e: Exception) {
            val empty: PengaturanMasjid = PengaturanMasjid()
            masjidItems.postValue(empty)
            e.printStackTrace()
        }
    }
}