package net.ayokemasjid.adminapp.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

const val CURRENT_USER_ID = 0

@Entity
data class User (
    var id_users: Int? = null,
    var username: String? = null,
    var email: String? = null,
    var first_name: String? = null,
    var last_name: String? = null,
    var password: String? = null,
    var salt: String? = null,
    var phone: String? = null,
    var address: String? = null,
    var city: String? = null,
    var province: String? = null,
    var country: String? = null,
    var postcode: String? = null,
    var date_joined: String? = null
): Serializable {
    @PrimaryKey(autoGenerate = false)
    var uid: Int = CURRENT_USER_ID
}