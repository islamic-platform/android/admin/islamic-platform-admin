package net.ayokemasjid.adminapp.data.model

import java.io.Serializable

data class ZakatMal (
    var id_zakat_mal: Int? = null,
    var date: String? = null,
    var name: String? = null,
    var total: String? = null,
    var info: String? = null
): Serializable